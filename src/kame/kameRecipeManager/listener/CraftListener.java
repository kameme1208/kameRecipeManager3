package kame.kameRecipeManager.listener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.inventory.PrepareItemCraftEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.server.PluginDisableEvent;
import org.bukkit.inventory.AnvilInventory;
import org.bukkit.inventory.CraftingInventory;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.util.Vector;

import com.google.common.collect.Sets;

import kame.kameRecipeManager.Main;
import kame.kameRecipeManager.recipe.KSmashRecipe;
import kame.kameRecipeManager.recipe.option.RecipeType;
import kame.kameRecipeManager.recipe.process.ItemProcessor;
import kame.kameRecipeManager.recipe.process.ItemSorts;
import kame.kameRecipeManager.recipe.process.ProcessingFusion;
import kame.kameRecipeManager.recipe.process.RecipeMatcher;
import kame.kameRecipeManager.recipe.process.RecipeOption;
import kame.kameRecipeManager.recipe.product.ProductRecipe;
import kame.kameRecipeManager.recipe.product.ProductReplace;
import kame.kameRecipeManager.utils.Display;
import kame.kameRecipeManager.utils.DummyStand;
import kame.kameRecipeManager.utils.Lang;
import kame.kameRecipeManager.utils.StandUtils;
import kame.kameRecipeManager.utils.Utils;
import kame.kameplayer.MainListener;

public class CraftListener implements Listener {

	private static final ItemStack DEFAULT_ITEM = new ItemStack(Material.COOKED_CHICKEN);
	private static HashMap<UUID, ArmorStand> stands = new HashMap<>();
	private static HashMap<UUID, Set<String>> includes = new HashMap<>();

	public CraftListener() {
		ItemMeta im = DEFAULT_ITEM.getItemMeta();
		im.setDisplayName("\1");
		im.setLore(Arrays.asList("@craft", "@inhand"));
		DEFAULT_ITEM.setItemMeta(im);
	}

	@EventHandler
	private void onDisable(PluginDisableEvent event) {
		if(event.getPlugin() instanceof Main) {
			Bukkit.getOnlinePlayers().stream().filter(CraftListener::isCrafting).forEach(Utils::reloaded);
		}
	}

	private static boolean isCrafting(Player player) {
		return stands.containsKey(player.getUniqueId());
	}

	@EventHandler
	private void onClick(InventoryClickEvent event) {
		Inventory inv = event.getClickedInventory();
		if(inv == null)return;
		switch(inv.getType()) {
		case ANVIL:
			ItemStack item = event.getClickedInventory().getContents()[0];
			if(event.getSlot() == 2 && item != null && item.getItemMeta().hasLore()) {
				event.getWhoClicked().sendMessage(Lang.changename());
				event.setCancelled(true);
			}
			break;
		case CRAFTING:
		case WORKBENCH:
			if(event.getSlot() != 0)
				RecipeOption.buffers.remove(event.getWhoClicked().getUniqueId());
			break;
		default:
			break;
		}
	}

	@EventHandler
	private void onClick(InventoryDragEvent event) {
		Inventory inv = event.getInventory();
		if(inv != null && inv instanceof CraftingInventory) {
			if(event.getInventorySlots().stream().anyMatch(x -> x <= 9))
				RecipeOption.buffers.remove(event.getWhoClicked().getUniqueId());
		}
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onInventoryInteract(PlayerInteractEvent event) {
		switch(event.getAction()) {
		case RIGHT_CLICK_BLOCK:
			if(onBlockClick(event))return;
		case RIGHT_CLICK_AIR:
			if(onAirClick(event))return;
		default:
			break;
		}
	}

	private boolean onAirClick(PlayerInteractEvent event) {
		ItemStack item = event.getItem();
		if(item == null || !item.getItemMeta().hasLore())return false;
		String lore = item.getItemMeta().getLore().toString().toLowerCase();
		if(lore.contains("@craft") && lore.contains("@inhand")) {
			event.setCancelled(true);
			if(event.getHand() == EquipmentSlot.OFF_HAND)return true;
			Player player = event.getPlayer();
			String itemname = item.getItemMeta().getDisplayName();
			ArmorStand entity = new DummyStand(itemname == null ? Lang.crafttable() : itemname, player.getLocation().getBlock());
			stands.put(player.getUniqueId(), entity);
			player.openWorkbench(player.getLocation(), true);
			Main.cast("§d[鯖たん] §e手持ち作業台にゃん♪(ฅ•ω•ฅ)");
			return true;
		}
		return false;
	}

	private boolean onBlockClick(PlayerInteractEvent event) {
		if(event.isCancelled())return false;
		Player player = event.getPlayer();
		Block block = event.getClickedBlock();
		ArmorStand entity = StandUtils.getCraftStand(block);
		if(event.hasItem() && player.isSneaking()) {
			return false;
		}
		switch(block.getType()) {
		case FURNACE:
		case BURNING_FURNACE:
			if(event.getHand() == EquipmentSlot.OFF_HAND)return true;
			stands.put(player.getUniqueId(), entity == null ? new DummyStand(Lang.furnace(), block) : entity);
			return true;
		case BREWING_STAND:
			if(entity == null)return false;
			event.setCancelled(true);
			if(event.getHand() == EquipmentSlot.OFF_HAND)return true;
			onBrew(player, block, entity);
			return true;
		case ANVIL:
			if(entity == null)return false;
			if(event.getHand() == EquipmentSlot.OFF_HAND)return true;
			if(stands.containsValue(entity)) {
				event.setCancelled(true);
				player.sendMessage(Lang.anvilusing());
				player.playSound(player.getEyeLocation(), Sound.BLOCK_CHEST_LOCKED, 1, 1);
				return true;
			}
			if(onAnvil(player, block, entity)) {
				stands.put(player.getUniqueId(), entity);
			}else {
				event.setCancelled(true);
			}
			return true;
		case WORKBENCH:
			if(event.getHand() == EquipmentSlot.OFF_HAND)return true;
			stands.put(player.getUniqueId(), entity == null ? new DummyStand(Lang.crafttable(), block) : entity);
			return true;
		default:
			if(entity == null)return false;
			if(event.getHand() == EquipmentSlot.OFF_HAND)return true;
			stands.put(player.getUniqueId(), entity);
			player.openWorkbench(block.getLocation(), true);
			event.setCancelled(true);
			return true;
		}
	}

	@EventHandler(priority = EventPriority.MONITOR)
	private void openInventory(InventoryOpenEvent event) {
		if(!(event.getPlayer() instanceof Player))return;
		Player player = (Player) event.getPlayer();
		if(!stands.containsKey(player.getUniqueId()))return;
		ArmorStand stand = stands.get(player.getUniqueId());
		Set<String> include = StandUtils.getIncludeName(stand);
		includes.put(player.getUniqueId(), include);
		switch(event.getInventory().getType()) {
		case ANVIL:
			Main.cast("§d[鯖たん] 金床開いたにゃん！(ฅ•ω•ฅ)");
			new Display(player, stand == null ? Lang.anvil() : stand.getCustomName(), include, RecipeType.Smash);
			smashView(player, stand, (AnvilInventory) event.getInventory());
			break;
		case FURNACE:
			Main.cast("§d[鯖たん] かまど開いたにゃん！(ฅ•ω•ฅ)");
			Display disp = new Display(player, stand == null ? Lang.furnace() : stand.getCustomName(), include, RecipeType.Furnace);
			disp.resisterFurnace(event.getInventory().getLocation().getBlock());
			break;
		case WORKBENCH:
			Main.cast("§d[鯖たん] 作業台開いたにゃん！(ฅ•ω•ฅ)");
			new Display(player, stand == null ? Lang.crafttable() : stand.getCustomName(), include, RecipeType.Shaped, RecipeType.Shapeless);
			break;
		case DROPPER:
		case DISPENSER:
			Main.cast("§d[鯖たん] 自動作業台開いたにゃん！(ฅ•ω•ฅ)");
			new Display(player, stand == null ? Lang.crafttable() : stand.getCustomName(), include, RecipeType.Shaped, RecipeType.Shapeless);
			break;
		default:
			Main.cast("§d[鯖たん] なんか開いたにゃん！(ฅ•ω•ฅ)");
			break;
		}
	}

	@EventHandler
	private void closeInventory(InventoryCloseEvent event) {
		if(stands.containsKey(event.getPlayer().getUniqueId())) {
			Main.cast("§d[鯖たん] インベントリ閉じたにゃん！(ฅ•ω•ฅ)");
			HumanEntity player = event.getPlayer();
			List<Item> items = smash.remove(player.getUniqueId());
			if(items != null) {
				items.forEach(x -> x.setPickupDelay(20));
			}
			ItemStack item = smashtool.remove(player.getUniqueId());
			if(item != null) {
				event.getInventory().setItem(0, null);
				player.getWorld().dropItem(player.getEyeLocation(), item).setPickupDelay(0);
				player.getWorld().playSound(player.getLocation(), Sound.ITEM_ARMOR_EQUIP_CHAIN, 1, 2);
			}
			Display.remove(player);
			RecipeOption.clear(player);
			special.remove(player.getUniqueId());
			smashing.remove(player.getUniqueId());
			includes.remove(player.getUniqueId());
			while(vannila.remove(player.getUniqueId()));
			ArmorStand stand = stands.remove(player.getUniqueId());
			if(stand == null || stand instanceof DummyStand)return;
			if(stand.getLocation().add(0, -Main.getOffsetY(), 0).getBlock().getType() == Material.AIR
					|| stand.getLeggings().equals(DEFAULT_ITEM))stand.remove();
		}
	}

	public static void setEventSkip(UUID uid, boolean flag) {
		if(flag)skip.add(uid);
		else skip.remove(uid);
	}

	private static Set<UUID> skip = new HashSet<>();
	public static List<UUID> vannila = new ArrayList<>();
	@EventHandler
	private void prepareCrafting(PrepareItemCraftEvent event) {
		Main.cast("§d[鯖たん] §e作業台にゃん♪(ฅ•ω•ฅ)");
		Player player = (Player) event.getView().getPlayer();
		if(vannila.contains(player.getUniqueId()))return;
		if(skip.contains(player.getUniqueId()))return;
		ArmorStand stand = stands.get(player.getUniqueId());
		if(stand == null) {
			if(event.getInventory().getType() == InventoryType.CRAFTING) {
				stand = new DummyStand(Lang.crafttable(), player.getLocation().getBlock());
				stands.put(player.getUniqueId(), stand);
			}else {
				player.sendMessage("§c[kame.デバッグ情報] 作業台エラーが発生しました。 エラー §e1");
				player.sendMessage("§c[kame.デバッグ情報] 再度検索開始します");
				Location debugloc = event.getInventory().getLocation();
				player.sendMessage("§c[block]§r World=[" + debugloc.getWorld() +
						", Location=[x:" + debugloc.getBlockX() + ", y:" + debugloc.getBlockY() + ", z:" + debugloc.getBlockZ() + "]");
				player.sendMessage("§c[block]§r BlockType=[§b" + debugloc.getBlock().getType() + "§r]");
				ItemStack[] item = event.getInventory().getMatrix();
				player.sendMessage("§c[block]§r Crafting=[§b" + Arrays.asList(item) + "§r]");
				stand = StandUtils.getCraftStand(debugloc.getBlock());
				if(stand == null)stand = new DummyStand(Lang.crafttable(), debugloc.getBlock());
				stands.put(player.getUniqueId(), stand);
				player.sendMessage("§c[block]§r BlockData=[§b" + stand + "§r]");
				player.sendMessage("§c[block]§r スクリーンショットで報告してくれると有り難いです");
			}
		}
		Location loc = stand == null ? player.getEyeLocation() : stand.getLocation().clone().add(0, -Main.getOffsetY(), 0);
		ItemStack item = null;
		special.remove(player.getUniqueId());
		Set<String> include = includes.containsKey(player.getUniqueId()) ? includes.get(player.getUniqueId()) : Sets.newHashSet(Lang.crafttable()) ;
		if((repair.contains(player.getUniqueId()) || event.getRecipe().getResult().getAmount() == 0) && include.contains(Lang.crafttable())) {
			repair.remove(player.getUniqueId());
			Main.cast("§d[鯖たん] §eスペシャルなレシピにゃん♪(ฅ•ω•ฅ)\n§d(item)§r" + event.getRecipe().getResult());
			List<ItemStack> bs = new ArrayList<>();
			List<ItemStack> it = new ArrayList<>();
			for(ItemStack items : event.getInventory().getMatrix()) {
				if(items.getAmount() == 0)continue;
				it.add(items);
				Map<String, Object> serial = items.serialize();
				serial.remove("meta");
				bs.add(ItemStack.deserialize(serial));
			}
			if(ItemProcessor.matchItem(bs, it, Main.getDefaultCheckMode(), player, loc)) {
				Main.cast("§d[鯖たん] §eスペシャルなレシピで確定にゃん♪(ฅ•ω•ฅ)");
				Main.cast(event.getRecipe().getResult().serialize());
				special.add(player.getUniqueId());
				return;
			}
		}
		if(event.isRepair() && player.hasPermission("kamerecipe.repair")) {
			Main.cast("§d[鯖たん] §eリペア候補にゃん♪(ฅ•ω•ฅ)");
			player.playSound(event.getInventory().getLocation().add(0.5, 0.5, 0.5), Sound.ITEM_ARMOR_EQUIP_IRON, 1, 2);
			repair.add(player.getUniqueId());
			return;
		}
		try {
			item = RecipeMatcher.prepareCraftingRecipe(event.getInventory(), event.getRecipe(), loc, player, include);
		}catch(Exception e) {
			e.printStackTrace();
			player.sendMessage(e.toString());
			player.sendMessage(Lang.unknownError());
			item = null;
		}
		event.getInventory().setItem(0, item);
		if(Main.debug && item != null)Main.cast("§d[鯖たん] §eこれあげるにゃん♪(ฅ•ω•ฅ)\n§d(item)§r" + item);
	}

	private static Set<UUID> special = new HashSet<>();
	private static Set<UUID> repair = new HashSet<>();
	public static HashMap<Player, BukkitTask> sync = new HashMap<>();
	@EventHandler(priority = EventPriority.LOWEST)
	private void onCrafting(CraftItemEvent event) {
		Player player = (Player) event.getView().getPlayer();
		if(vannila.remove(player.getUniqueId()))return;
		if(sync.containsKey(player)) {
			event.setCancelled(true);
			return;
		}
		if(!stands.containsKey(player.getUniqueId())) {
			event.setCancelled(true);
			player.sendMessage("§c[kame.デバッグ情報] 作業台エラーが発生しました。 エラー §e2");
			player.sendMessage("§c[kame.デバッグ情報] この処理はキャンセルされました。");
			Location debugloc = event.getInventory().getLocation();
			player.sendMessage("§c[block] World=[" + debugloc.getWorld() +
					", Location=[x:" + debugloc.getBlockX() + ", y:" + debugloc.getBlockY() + ", z:" + debugloc.getBlockZ() + "]");
			player.sendMessage("§c[block] BlockType=[" + debugloc.getBlock().getType() + "]");
			ItemStack[] item = event.getInventory().getMatrix();
			player.sendMessage("§c[block] Crafting=[" + Arrays.asList(item) + "]");
			player.sendMessage("§c[kame.] スクリーンショットで報告してくれると有り難いです。");
			Utils.reloaded(player);
			return;
		}
		BukkitTask run = Bukkit.getScheduler().runTaskLater(Main.getInstance(),  () -> {
			player.updateInventory();
			sync.remove(player);
		}, 2);
		sync.put(player, run);
		RecipeMatcher.resultCraftingRecipe(event, player);
	}

	private void onBrew(Player player, Block block, ArmorStand stand) {
		if(ProcessingFusion.isBrewing(block))return;
		List<ItemStack> items = new ArrayList<>();
		int i = 3;
		for(Entity e : stand.getWorld().getNearbyEntities(block.getLocation().add(0.5, 0.3, 0.5), 0.5, 0.25, 0.5))if(e instanceof Item){
			if(((Item) e).getPickupDelay() > 1000) {
				e.remove();
				continue;
			}
			ItemStack base = ((Item) e).getItemStack();
			while(i-- != 0) {
				ItemStack input = base.clone();
				base.setAmount(base.getAmount() - 1);
				input.setAmount(1);
				items.add(input);
				if(base.getAmount() == 0) {
					((Item) e).remove();
					break;
				}
			}
			e.getWorld().spawnParticle(Particle.FLAME, e.getLocation().add(0, 0.1, 0), 5, 0, 0, 0, 0.01);
			((Item) e).setItemStack(base);
		}
		World world = stand.getWorld();
		if(items.size() == 0) {
			world.playSound(stand.getEyeLocation(), Sound.BLOCK_REDSTONE_TORCH_BURNOUT, 1, 2f);
			return;
		}
		world.playSound(stand.getEyeLocation(), Sound.ENTITY_ENDERDRAGON_FLAP, 1, 0.2f);
		new ProcessingFusion(player, block, stand, items).runTaskTimer(Main.getInstance(), 20L, 20L);
	}

	private static HashMap<UUID, List<Item>> smash = new HashMap<>();
	private static HashMap<UUID, ItemStack> smashtool = new HashMap<>();
	private static HashMap<UUID, List<ItemStack>> smashing = new HashMap<>();
	private boolean onAnvil(Player player, Block block, ArmorStand stand) {
		ItemStack handitem = player.getEquipment().getItemInMainHand();
		Location loc = stand.getLocation().add(0, 0.5-Main.getOffsetY(), 0);
		switch(handitem.getType()) {
		case AIR:
			if(player.getGameMode() != GameMode.CREATIVE) player.damage(1);
			player.playSound(loc, Sound.ENTITY_PLAYER_BIG_FALL, 1, 1);
			return false;
		case WOOD_AXE:
		case STONE_AXE:
		case IRON_AXE:
		case GOLD_AXE:
		case DIAMOND_AXE:
			break;
		default:
			player.playSound(loc, Sound.ENTITY_ARMORSTAND_PLACE, 1, 1);
			return false;
		}
		Main.cast("[鯖たん] 金床レシピにゃん！(ฅ•ω•ฅ)");
		ArrayList<Item> items = new ArrayList<Item>();
		stand.getWorld().getNearbyEntities(loc, 0.55, 0.55, 0.55).stream().filter(x -> x instanceof Item).forEach(e -> {
			if(((Item) e).getPickupDelay() < 100) {
				items.add((Item) e);
				((Item) e).setPickupDelay(Integer.MAX_VALUE);
				e.teleport(stand);
				e.getWorld().spawnParticle(Particle.CRIT_MAGIC, e.getLocation().add(0, 0.5, 0), 5, 0.2, 0.2, 0.2, 0);
				loc.getWorld().playSound(loc, Sound.BLOCK_WOOD_BREAK, 1, 2);
			}
		});


		loc.getWorld().playSound(loc, Sound.ITEM_ARMOR_EQUIP_IRON, 1, 2);

		List<ItemStack> crafting = new ArrayList<>();
		smash.put(player.getUniqueId(), items);

		for(Item dropitem : items) {
			ItemStack craft = dropitem.getItemStack().clone();
			do{
				ItemStack buffer = craft.clone();
				buffer.setAmount(1);
				crafting.add(buffer);
				craft.setAmount(craft.getAmount() - 1);
			}while(craft.getAmount() != 0);
		}
		Collections.sort(crafting, ItemSorts.getInstance());
		smashing.put(player.getUniqueId(), crafting);
		if(Main.debug)Main.cast("§b[鯖たん] アイテムにゃん！(ฅ•ω•ฅ) §r" + crafting);
		return true;
	}

	private void smashView(Player player, ArmorStand stand, AnvilInventory inv) {
		int slot = player.getInventory().getHeldItemSlot();
		ItemStack axe = player.getInventory().getItem(slot);
		smashtool.put(player.getUniqueId(), axe.clone());
		player.getInventory().setItem(slot, null);
		ItemMeta im = axe.getItemMeta();
		im.setDisplayName(Lang.anvilSpel());
		axe.setItemMeta(im);
		inv.setItem(0, axe);
		Bukkit.getScheduler().runTaskLater(Main.getInstance(),  () -> {
			ItemStack axe2 = axe.clone();
			im.setDisplayName("");
			axe2.setItemMeta(im);
			inv.setItem(2, axe2);
		}, 2);
	}

	@EventHandler
	private void onAnvilInventoryClick(InventoryClickEvent event) {
		if(!(event.getClickedInventory() instanceof AnvilInventory) || !stands.containsKey(event.getWhoClicked().getUniqueId()))return;
		Main.cast("にゃっ");
		AnvilInventory inv = (AnvilInventory) event.getInventory();
		ItemStack item = inv.getItem(2);
		Player player = (Player) event.getWhoClicked();
		event.setCancelled(true);
		UUID uid = player.getUniqueId();
		Location loc = inv.getLocation().add(0.5, 0.5, 0.5);
		Set<String>include = includes.remove(uid);
		if(item == null) {
			player.closeInventory();
			return;
		}
		String spell = item.getItemMeta().hasDisplayName() ? item.getItemMeta().getDisplayName() : "";
		KSmashRecipe recipe = RecipeMatcher.getSmashRecipe(smashing.remove(uid), smashtool.get(uid), spell, loc, player, include);
		loc.add(0, 0.75, 0);
		if(recipe != null) {
			new ItemDrops(recipe, player, loc).runTaskTimer(Main.getInstance(), 0, 15);
			ItemStack tool = smashtool.remove(uid);
			int subdura = recipe.getConsumeDurability();
			short durability = tool.getDurability();
			if(tool.containsEnchantment(Enchantment.DURABILITY)) {
				int level = (int) (Math.random() * tool.getEnchantmentLevel(Enchantment.DURABILITY));
				subdura += subdura < 0 ? level : -level;
			}
			boolean bool = false;
			if(Bukkit.getPluginManager().isPluginEnabled("kameplayer") && MainListener.itemDamage(player, tool, null, subdura)) {
				bool = true;
			}else {
				durability += subdura;
				if(durability < 0)durability = 0;
				tool.setDurability(durability);
				switch(tool.getType()) {
				case WOOD_AXE:
					bool = tool.getDurability() > 59;
					break;
				case STONE_AXE:
					bool = tool.getDurability() > 131;
					break;
				case IRON_AXE:
					bool = tool.getDurability() > 250;
					break;
				case GOLD_AXE:
					bool = tool.getDurability() > 32;
					break;
				case DIAMOND_AXE:
					bool = tool.getDurability() > 1561;
					break;
				default:
				}
			}
			if(bool) {
				loc.getWorld().spawnParticle(Particle.ITEM_CRACK, loc,10, 0.2, 0.1, 0.2, 0.1, tool);
				Bukkit.getScheduler().runTaskLater(Main.getInstance(),  () -> loc.getWorld().playSound(loc, Sound.ENTITY_ITEM_BREAK, 1, 0.5f), 45);
			}else {
				player.getWorld().dropItem(player.getEyeLocation(), tool).setPickupDelay(0);
			}
		}else {
			loc.getWorld().playSound(loc, Sound.BLOCK_COMPARATOR_CLICK, 1, 0.5f);
		}
		inv.setItem(0, null);
		inv.setItem(1, null);
		inv.setItem(2, null);
		player.closeInventory();
	}

	private class ItemDrops extends BukkitRunnable {
		private KSmashRecipe recipe;
		private Player player;
		private Location loc;
		private List<Item> items;

		private ItemDrops(KSmashRecipe recipe, Player player, Location loc) {
			this.recipe = recipe;
			this.player = player;
			this.loc = loc;
			this.items = smash.remove(player.getUniqueId());
			loc.getWorld().playSound(loc, Sound.BLOCK_ANVIL_USE, 1, 0.5f);
		}

		private int i = 0;
		public void run() {
			switch(i++) {
			case 2:
				this.cancel();
				if(!recipe.getEffect().isEnpty())recipe.getEffect().playEffect(null, loc);
				else loc.getWorld().spawnParticle(Particle.CLOUD, loc,10, 0.2, 0.1, 0.2, 0.1);
				if(!recipe.getSound().isEnpty())recipe.getSound().playSound(null, loc);
				dropItems();
			default:
				loc.getWorld().spawnParticle(Particle.CRIT, loc,10, 0.2, 0.1, 0.2, 0);
				if(!recipe.getSound().isEnpty())recipe.getSound().playSound(null, loc);
				break;
			}
		}

		private void dropItems() {
			ItemStack result = recipe.getResult(player, loc.subtract(0, 0.25, 0));
			for(ProductRecipe product : recipe.getProducts()) {
				if(product instanceof ProductReplace) {
					result = product.buildItem();
				}else {
					ItemStack item = product.buildItem();
					if(item == null)continue;
					loc.getWorld().dropItem(loc, item).setVelocity(new Vector(0, 0, 0));
				}
			}
			if(result != null)loc.getWorld().dropItem(loc, result).setVelocity(new Vector(0, 0, 0));
			items.forEach(x -> x.remove());
		}
	}

}
