package kame.kameRecipeManager;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.server.PluginEnableEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import com.google.common.io.Files;

import kame.kameRecipeManager.command.RecipeCommand;
import kame.kameRecipeManager.listener.BlockListener;
import kame.kameRecipeManager.listener.CraftListener;
import kame.kameRecipeManager.listener.FurnaceListener;
import kame.kameRecipeManager.reader.FileReader;
import kame.kameRecipeManager.recipe.option.CheckMode;
import kame.kameRecipeManager.recipe.process.ActiveFurnace;

public class Main extends JavaPlugin implements Listener {

	private static Main instance;
	private static FileConfiguration config;
	private static float offset = 1;
	private static Set<CheckMode> mode = new HashSet<>();
	private static List<String> warnings = new ArrayList<String>();
	public static boolean debug = false;
	public static final char COLOR = ChatColor.COLOR_CHAR;
	public void onEnable() {
		instance = this;
		getServer().getPluginManager().registerEvents(this, this);
		getServer().getPluginManager().registerEvents(new CraftListener()	, this);
		getServer().getPluginManager().registerEvents(new BlockListener()	, this);
		getServer().getPluginManager().registerEvents(new FurnaceListener()	, this);
		RecipeCommand r = new RecipeCommand();
		getCommand("kamerecipe").setExecutor(r);
		getCommand("kamerecipe").setTabCompleter(r);
		loadConfig();
		FileReader.readRecipes();
		ActiveFurnace.getInstance();
	}

	public void onDisable() {
	}

	public static void addWarnig(String str) {
		warnings.add(str);
	}

	@EventHandler
	private void onEnable(PluginEnableEvent event) {
		if(event.getPlugin() instanceof Main) Bukkit.getOnlinePlayers().forEach(player -> sendWarnings(player));
	}

	@EventHandler
	private void onJoin(PlayerJoinEvent event) {
		sendWarnings(event.getPlayer());
	}

	private static Set<Player> queue = new HashSet<Player>();
	public static void sendWarnings(Player player) {
		if(warnings.size() == 0)return;
		if(player.hasPermission("kameRecipe.admin") && !queue.contains(player)) {
			queue.add(player);
			Bukkit.getScheduler().runTaskLaterAsynchronously(instance, () -> {
				player.sendMessage(ChatColor.RED + "[kameRecipeManager] WARNING!");
				player.sendMessage(warnings.toArray(new String[0]));
				queue.remove(player);
			}, 60);
		}
	}

	public static void loadConfig() {
		warnings.clear();
		instance.saveDefaultConfig();
		try {
			File file = new File(instance.getDataFolder(), "config.yml");
			config = YamlConfiguration.loadConfiguration(file);
			if(config.getDouble("configversion") != 3.2) {
				instance.getLogger().severe("不一致なバージョンのConfigを検知したので置き換えました");
				warnings.add(ChatColor.RED + "[kames.] 不一致なバージョンのConfigを検知したので置き換えました");
				Files.move(file, new File(instance.getDataFolder(), "config-old.yml"));
				instance.saveDefaultConfig();
				config = YamlConfiguration.loadConfiguration(file);
			}
			debug = config.getBoolean("debug");
			offset = Float.parseFloat(config.getString("offset-y"));
			for(String str : config.getStringList("debaultcheckmode")) {
				CheckMode m = CheckMode.fromName(str.toLowerCase());
				if(m != null)mode.add(m);
			}
		} catch (Exception e) {
			instance.getLogger().warning("Config error! Configを確認してください！");
			warnings.add(ChatColor.RED + "[kames.] Config error! Configを確認してください！");
			e.printStackTrace();
		}
	}

	public static void cast(Object o) {
		if(debug)Bukkit.broadcastMessage(String.valueOf(o));
	}

	public static Set<CheckMode> getDefaultCheckMode() {
		return mode;
	}

	public static float getOffsetY() {
		return offset;
	}

	public static Plugin getInstance() {
		return instance;
	}

	public static FileConfiguration getConf() {
		return config;
	}
}
