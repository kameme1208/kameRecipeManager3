package kame.kameRecipeManager.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.google.common.collect.Lists;

import kame.kameRecipeManager.reader.exception.IllegalItemException;
import kame.kameRecipeManager.reader.exception.IllegalItemException.ErrorType;

public class Utils {

	@SuppressWarnings("deprecation")
	public static Material parseMaterial(String name) {
		String[] item = name.split(":");
		if(item[0].equalsIgnoreCase("AIR") || name.indexOf("minecraft:air") == 0)return Material.AIR;
		if(item.length > 1)name = item[0].concat(":").concat(item[1]);
		Material material = Bukkit.getUnsafe().getMaterialFromInternalName(name.toLowerCase());
		if(material.equals(Material.AIR))material = Material.matchMaterial(item[0].toUpperCase());
		return material;
	}

	public static ItemStack parseItemStack(String line) throws IllegalItemException {
		while(line.charAt(0) == ' ')line = line.substring(1);
		String[] metas = line.split(" @");
		String[] item = metas[0].split(":");
		int i = metas[0].matches("[0-z_]+((:[0-9]+)?:[0-9]+)?") ? 0 : 1;
		Material material = Utils.parseMaterial(metas[0]);
		if(material == null)throw new IllegalItemException(ErrorType.MATERIAL, metas[0], "is inknown Material");
		short dat = item.length > ++i ? (short) Utils.toInt(item[i], -1) : -1;
		int amount = item.length > ++i ? amount = Utils.toInt(item[i], 1) : 1;
		ItemStack result = new ItemStack(material);
		if(dat == -1)result.getData().setData((byte) dat);
		else result.setDurability(dat);
		result.setAmount(amount);
		ItemMeta im = result.getItemMeta();
		List<String> lore = new ArrayList<String>();
		for(String meta : metas) {
			item = meta.split(":", 2);
			switch(item[0]) {
			case "name":
				im.setDisplayName(meta.substring(5));
				break;
			case "lore":
				lore.add(meta.substring(5));
				im.setLore(lore);
				break;
			case "ench":
				String[] enchs = meta.split(":");
				if(enchs.length == 3) {
					Enchantment e = enchs[1].matches("[0-9]+") ? e = Enchantment.getById(Utils.toInt(enchs[1], -1)) : Enchantment.getByName(enchs[1]);
					if(e == null)throw new IllegalItemException(ErrorType.ENCHANTMENTS, "", "", enchs[1] , " is unknown Enchant");
					im.addEnchant(e, Utils.toInt(enchs[2], -1), true);
				}else throw new IllegalItemException(ErrorType.ENCHANTMENTS, "Enchantment. too few arguments");
				break;
			case "flag":
				if(item.length != 2) {
					throw new IllegalItemException(ErrorType.AUGMENTS, "ItemFlag. too few arguments");
				}try {
					if(item[1].equalsIgnoreCase("ALL")) {
						im.addItemFlags(ItemFlag.values());
					}else {
						ItemFlag flag = ItemFlag.valueOf(item[1].toUpperCase());
						im.addItemFlags(flag);
					}
				}catch(Exception e) {
					throw new IllegalItemException(ErrorType.OTHER, "ItemFlag. " + item[1] + " is unknown flag");
				}
				break;
			case "tag":
				try {
					im = Bukkit.getUnsafe().modifyItemStack(result, meta.substring(4)).getItemMeta();
				}catch(Exception e) {
					new IllegalItemException(ErrorType.BADNBT, "NBTtag;" + meta + " is failed");
				}
				break;
			default:
			}
		}
		result.setItemMeta(im);
		return result;
	}

	public static int toInt(String input, int def) {
		try {
			return Integer.parseInt(input);
		}catch(NumberFormatException e) {
			return def;
		}
	}

	public static void reloaded(Player player) {
		player.sendMessage(Lang.reloaded());
		player.closeInventory();
		player.setScoreboard(Bukkit.getScoreboardManager().getMainScoreboard());
	}

	public static List<ItemStack> replaceMeta(Collection<ItemStack> item, Player player, Location loc) {
		item.forEach(x -> replaceMeta(x, player, loc));
		return Lists.newArrayList(item);
	}

	public static ItemStack replaceMeta(ItemStack item, Player player, Location loc) {
		if(item == null || !item.hasItemMeta())return item;
		ItemMeta im = item.getItemMeta();
		if(im.hasDisplayName())im.setDisplayName(engine(im.getDisplayName(), player, loc));
		if(im.hasLore()) {
			List<String> lore = im.getLore();
			List<String> output = new ArrayList<>();
			for(String line : lore) {
				output.add(engine(line, player, loc));
			}
			im.setLore(output);
		}
		item = item.clone();
		item.setItemMeta(im);
		return item;
	}

	public static String replace(String input, Player player, Location loc) {
		StringBuilder builder = new StringBuilder(input);
		if(player != null) {
			replaceString(builder, "<player>",		player.getName());
			replaceString(builder, "<name>",			player.getDisplayName());
			replaceString(builder, "<level>",			player.getLevel());
			replaceString(builder, "<UUID>",			player.getUniqueId());
			replaceString(builder, "<health>",		player.getHealth());
			replaceString(builder, "<foodlevel>",		player.getFoodLevel());
			replaceString(builder, "<saturation>",	player.getSaturation());
			replaceString(builder, "<playerX>",		player.getLocation().getBlockX());
			replaceString(builder, "<playerY>",		player.getLocation().getBlockY());
			replaceString(builder, "<playerZ>",		player.getLocation().getBlockZ());

			String a = "<score_";
			int first;
			while((first = builder.indexOf(a)) != -1) {
				int last = first += 7;
				while(last < builder.length()-1 && builder.charAt(last) != '>')if(builder.charAt(last++) == '\\')last++;
				String put = builder.substring(first, last).replace("\\>", ">");
				try {
					String b = Integer.toString(Bukkit.getScoreboardManager().getMainScoreboard().getObjective(put).getScore(player.getName()).getScore());
					builder.replace(first-7, last+1, b);
				}catch(NullPointerException e) {
					builder.replace(first-7, last+1, "0");
				}
			}
		}
		if(loc != null) {
			replaceString(builder, "<locationX>",		loc.getBlockX());
			replaceString(builder, "<locationY>",		loc.getBlockY());
			replaceString(builder, "<locationZ>",		loc.getBlockZ());
			replaceString(builder, "<time>",			loc.getWorld().getTime());
			replaceString(builder, "<fulltime>",		loc.getWorld().getFullTime());
			replaceString(builder, "<skylevel>",		loc.getBlock().getLightFromSky());
			replaceString(builder, "<blocklevel>",	loc.getBlock().getLightFromBlocks());
			replaceString(builder, "<lightlevel>",	loc.getBlock().getLightLevel());
			replaceString(builder, "<World>", 		loc.getWorld().getName());
			replaceString(builder, "<biome>", 		loc.getBlock().getBiome().toString().toLowerCase());
			replaceString(builder, "<BIOME>", 		loc.getBlock().getBiome().toString());
		}
		return builder.toString();
	}

	public static String replaceString(String input, String target, Object obj) {
		int index = input.indexOf(target);
		if(index == -1) {
			return input;
		}else {
			String replace = String.valueOf(obj);
			int length = target.length(), l = length == 0 ? replace.length()+1 : replace.length();
			StringBuilder builder = new StringBuilder(input.length()).append(input);
			do {
				builder.replace(index, index + length, replace);
				index = builder.indexOf(target, index + l);
				if(builder.length() == index) {
					builder.replace(index, index + length, replace);
					index = -1;
				}
			}while(index != -1);
			return builder.toString();
		}
	}

	private static void replaceString(StringBuilder builder, String target, Object obj) {
		int index = builder.indexOf(target);
		if(index == -1) {
			return;
		}else {
			String replace = String.valueOf(obj);
			int length = target.length(), l = length == 0 ? replace.length()+1 : replace.length();
			do {
				builder.replace(index, index + length, replace);
				index = builder.indexOf(target, index + l);
				if(builder.length() == index) {
					builder.replace(index, index + length, replace);
					index = -1;
				}
			}while(index != -1);
			return;
		}
	}

	public static String engine(String input, Player player, Location loc) {
		return engine(replace(input, player, loc), player);
	}

	public static String engine(String line, CommandSender sender) {
		String s = "==NUM(str)";
		String i = "==NUM(int)";
		String d = "==NUM(dec)";
		for(int first=line.indexOf(s)+10;first!=9;first=line.indexOf(s, first)+10) {
			int a = line.indexOf('[', first);
			if(a == -1)continue;
			String selecter = line.substring(first, a);
			int last = ++a;
			char c[] = line.toCharArray();
			while(last < c.length-1 && c[last] != ']')if(c[last++] == '\\')last++;
			String put = replaceString(line.substring(a, last), "\\]", "]");
			line = line.substring(0, first-10).concat(engine(selecter, put, 0, sender)).concat(line.substring(last+1));
		}
		for(int first=line.indexOf(i)+10;first!=9;first=line.indexOf(s, first)+10) {
			int a = line.indexOf('[', first);
			if(a == -1)continue;
			String selecter = line.substring(first, a);
			int last = ++a;
			char c[] = line.toCharArray();
			while(last < c.length-1 && c[last] != ']')if(c[last++] == '\\')last++;
			String put = replaceString(line.substring(a, last), "\\]", "]");
			line = line.substring(0, first-10).concat(engine(selecter, put, 1, sender)).concat(line.substring(last+1));
		}
		for(int first=line.indexOf(d)+10;first!=9;first=line.indexOf(s, first)+10) {
			int a = line.indexOf('[', first);
			if(a == -1)continue;
			String selecter = line.substring(first, a);
			int last = ++a;
			char c[] = line.toCharArray();
			while(last < c.length-1 && c[last] != ']')if(c[last++] == '\\')last++;
			String put = replaceString(line.substring(a, last), "\\]", "]");
			line = line.substring(0, first-10).concat(engine(selecter, put, 2, sender)).concat(line.substring(last+1));
		}
		return line;
	}

	private static Map<String, ScriptEngine> engines = new HashMap<>();
	private static String engine(String enginename, String str, int type, CommandSender sender) {
		Script engine = new Script();
		Thread t = new Thread(() -> {
			if(str.equals("new")) {
				engines.put(enginename, new ScriptEngineManager().getEngineByName("JavaScript"));
				engine.eval = "";
			}else {
				if(!engines.containsKey(enginename))engines.put(enginename, new ScriptEngineManager().getEngineByName("JavaScript"));
				engine.eval = engineThread(engines.get(enginename), str, type, sender);
			}
		});
		t.start();
		try {
			t.join(1000);
			if(engine.eval == null) {
				sender.sendMessage("§c[kames.] 演算に失敗ました" + str);
				sender.sendMessage("タイムアウトしました");
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return engine.eval == null ? "ERROR" : engine.eval;
	}

	private static class Script {
		private String eval;
	}

	public static String engineThread(ScriptEngine engine, String str, int type, CommandSender sender) {
		try {
			str = engine.eval(str).toString();
			switch(type) {
			case 1:
				str = Double.valueOf(str).intValue() + "";
				break;
			case 2:
				str = Double.valueOf(str).toString();
			default:
			}
		}catch(Exception e) {
			sender.sendMessage("§c[kames.] 演算に失敗ました。不正な式です " + str);
			sender.sendMessage(e.getMessage());
		}
		return str;
	}

	public static boolean equals(ItemStack base, ItemStack item) {
		if(base == item) return true;
		if(base == null || item == null)return false;
		base = base.clone();
		item = item.clone();
		base.setAmount(1);
		item.setAmount(1);
		return base.equals(item);
	}


    private static String[] split(String line, String split, int size) {
    	ArrayList<String> list = new ArrayList<>();
    	int i = 0, j = 0, length = split.length();
    	for(int k = 1;k++ != size && (j = line.indexOf(split, i)) != -1; i = j+length) {
    		list.add(line.substring(i, j));
    	}
    	list.add(line.substring(i, line.length()));
    	return list.toArray(new String[0]);
    }
}
