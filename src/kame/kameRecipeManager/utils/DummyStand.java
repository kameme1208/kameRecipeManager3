package kame.kameRecipeManager.utils;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.bukkit.EntityEffect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Server;
import org.bukkit.World;
import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeInstance;
import org.bukkit.block.Block;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Egg;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.Snowball;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionAttachment;
import org.bukkit.permissions.PermissionAttachmentInfo;
import org.bukkit.plugin.Plugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.EulerAngle;
import org.bukkit.util.Vector;

import kame.kameRecipeManager.Main;

public class DummyStand implements ArmorStand {
	private String customname;
	private Location loc;
	public DummyStand(String name, Block block) {
		customname = name;
		loc = block.getLocation().add(0.5, Main.getOffsetY(), 0.5);
	}

	@Override
	public int _INVALID_getLastDamage() {
		return 0;
	}

	@Override
	public void _INVALID_setLastDamage(int arg0) {
	}

	@Override
	public boolean addPotionEffect(PotionEffect arg0) {
		return false;
	}

	@Override
	public boolean addPotionEffect(PotionEffect arg0, boolean arg1) {
		return false;
	}

	@Override
	public boolean addPotionEffects(Collection<PotionEffect> arg0) {
		return false;
	}

	@Override
	public Collection<PotionEffect> getActivePotionEffects() {
		return null;
	}

	@Override
	public boolean getCanPickupItems() {
		return false;
	}

	@Override
	public EntityEquipment getEquipment() {
		return null;
	}

	@Override
	public double getEyeHeight() {
		return 0;
	}

	@Override
	public double getEyeHeight(boolean arg0) {
		return 0;
	}

	@Override
	public Location getEyeLocation() {
		return null;
	}

	@Override
	public Player getKiller() {
		return null;
	}

	@Override
	public double getLastDamage() {
		return 0;
	}

	@Override
	public List<Block> getLastTwoTargetBlocks(HashSet<Byte> arg0, int arg1) {
		return null;
	}

	@Override
	public List<Block> getLastTwoTargetBlocks(Set<Material> arg0, int arg1) {
		return null;
	}

	@Override
	public Entity getLeashHolder() throws IllegalStateException {
		return null;
	}

	@Override
	public List<Block> getLineOfSight(HashSet<Byte> arg0, int arg1) {
		return null;
	}

	@Override
	public List<Block> getLineOfSight(Set<Material> arg0, int arg1) {
		return null;
	}

	@Override
	public int getMaximumAir() {
		return 0;
	}

	@Override
	public int getMaximumNoDamageTicks() {
		return 0;
	}

	@Override
	public int getNoDamageTicks() {
		return 0;
	}

	@Override
	public int getRemainingAir() {
		return 0;
	}

	@Override
	public boolean getRemoveWhenFarAway() {
		return false;
	}

	@Override
	public Block getTargetBlock(HashSet<Byte> arg0, int arg1) {
		return null;
	}

	@Override
	public Block getTargetBlock(Set<Material> arg0, int arg1) {
		return null;
	}

	@Override
	public boolean hasAI() {
		return false;
	}

	@Override
	public boolean hasLineOfSight(Entity arg0) {
		return false;
	}

	@Override
	public boolean hasPotionEffect(PotionEffectType arg0) {
		return false;
	}

	@Override
	public boolean isCollidable() {
		return false;
	}

	@Override
	public boolean isGliding() {
		return false;
	}

	@Override
	public boolean isLeashed() {
		return false;
	}

	@Override
	public void removePotionEffect(PotionEffectType arg0) {}

	@Override
	public void setAI(boolean arg0) {}

	@Override
	public void setCanPickupItems(boolean arg0) {}

	@Override
	public void setCollidable(boolean arg0) {}

	@Override
	public void setGliding(boolean arg0) {}

	@Override
	public void setLastDamage(double arg0) {}

	@Override
	public boolean setLeashHolder(Entity arg0) {
		return false;
	}

	@Override
	public void setMaximumAir(int arg0) {}

	@Override
	public void setMaximumNoDamageTicks(int arg0) {}

	@Override
	public void setNoDamageTicks(int arg0) {}

	@Override
	public void setRemainingAir(int arg0) {}

	@Override
	public void setRemoveWhenFarAway(boolean arg0) {}

	@Override
	public AttributeInstance getAttribute(Attribute arg0) {
		return null;
	}

	@Override
	public boolean eject() {
		return false;
	}

	@Override
	public String getCustomName() {
		return customname;
	}

	@Override
	public int getEntityId() {
		return 0;
	}

	@Override
	public float getFallDistance() {
		return 0;
	}

	@Override
	public int getFireTicks() {
		return 0;
	}

	@Override
	public EntityDamageEvent getLastDamageCause() {
		return null;
	}

	@Override
	public Location getLocation() {
		return loc;
	}

	@Override
	public Location getLocation(Location arg0) {
		return loc;
	}

	@Override
	public int getMaxFireTicks() {
		return 0;
	}

	@Override
	public List<Entity> getNearbyEntities(double arg0, double arg1, double arg2) {
		return null;
	}

	@Override
	public Entity getPassenger() {
		return null;
	}

	@Override
	public Server getServer() {
		return null;
	}

	@Override
	public int getTicksLived() {
		return 0;
	}

	@Override
	public EntityType getType() {
		return null;
	}

	@Override
	public UUID getUniqueId() {
		return null;
	}

	@Override
	public Entity getVehicle() {
		return null;
	}

	@Override
	public Vector getVelocity() {
		return null;
	}

	@Override
	public World getWorld() {
		return null;
	}

	@Override
	public boolean isCustomNameVisible() {
		return false;
	}

	@Override
	public boolean isDead() {
		return false;
	}

	@Override
	public boolean isEmpty() {
		return false;
	}

	@Override
	public boolean isGlowing() {
		return false;
	}

	@Override
	public boolean isInsideVehicle() {
		return false;
	}

	@Override
	public boolean isInvulnerable() {
		return false;
	}

	@Override
	public boolean isOnGround() {
		return false;
	}

	public boolean isSilent() {
		return false;
	}

	@Override
	public boolean isValid() {
		return false;
	}

	@Override
	public boolean leaveVehicle() {
		return false;
	}

	@Override
	public void playEffect(EntityEffect arg0) {}

	@Override
	public void remove() {}

	@Override
	public void setCustomName(String arg0) {}

	@Override
	public void setCustomNameVisible(boolean arg0) {}

	@Override
	public void setFallDistance(float arg0) {}

	@Override
	public void setFireTicks(int arg0) {}

	@Override
	public void setGlowing(boolean arg0) {}

	@Override
	public void setInvulnerable(boolean arg0) {}

	@Override
	public void setLastDamageCause(EntityDamageEvent arg0) {}

	@Override
	public boolean setPassenger(Entity arg0) {
		return false;
	}

	public void setSilent(boolean arg0) {}

	@Override
	public void setTicksLived(int arg0) {}

	@Override
	public void setVelocity(Vector arg0) {}

	@Override
	public Spigot spigot() {
		return null;
	}

	@Override
	public boolean teleport(Location arg0) {
		return false;
	}

	@Override
	public boolean teleport(Entity arg0) {
		return false;
	}

	@Override
	public boolean teleport(Location arg0, TeleportCause arg1) {
		return false;
	}

	@Override
	public boolean teleport(Entity arg0, TeleportCause arg1) {
		return false;
	}

	@Override
	public List<MetadataValue> getMetadata(String arg0) {
		return null;
	}

	@Override
	public boolean hasMetadata(String arg0) {
		return false;
	}

	@Override
	public void removeMetadata(String arg0, Plugin arg1) {}

	@Override
	public void setMetadata(String arg0, MetadataValue arg1) {}

	@Override
	public String getName() {
		return null;
	}

	@Override
	public void sendMessage(String arg0) {}

	@Override
	public void sendMessage(String[] arg0) {}

	@Override
	public PermissionAttachment addAttachment(Plugin arg0) {
		return null;
	}

	@Override
	public PermissionAttachment addAttachment(Plugin arg0, int arg1) {
		return null;
	}

	@Override
	public PermissionAttachment addAttachment(Plugin arg0, String arg1, boolean arg2) {
		return null;
	}

	@Override
	public PermissionAttachment addAttachment(Plugin arg0, String arg1, boolean arg2, int arg3) {
		return null;
	}

	@Override
	public Set<PermissionAttachmentInfo> getEffectivePermissions() {
		return null;
	}

	@Override
	public boolean hasPermission(String arg0) {
		return false;
	}

	@Override
	public boolean hasPermission(Permission arg0) {
		return false;
	}

	@Override
	public boolean isPermissionSet(String arg0) {
		return false;
	}

	@Override
	public boolean isPermissionSet(Permission arg0) {
		return false;
	}

	@Override
	public void recalculatePermissions() {}

	@Override
	public void removeAttachment(PermissionAttachment arg0) {}

	@Override
	public boolean isOp() {
		return false;
	}

	@Override
	public void setOp(boolean arg0) {}

	@Override
	public void _INVALID_damage(int arg0) {}

	@Override
	public void _INVALID_damage(int arg0, Entity arg1) {}

	@Override
	public int _INVALID_getHealth() {
		return 0;
	}

	@Override
	public int _INVALID_getMaxHealth() {
		return 0;
	}

	@Override
	public void _INVALID_setHealth(int arg0) {}

	@Override
	public void _INVALID_setMaxHealth(int arg0) {}

	@Override
	public void damage(double arg0) {}

	@Override
	public void damage(double arg0, Entity arg1) {}

	@Override
	public double getHealth() {
		return 0;
	}

	@Override
	public double getMaxHealth() {
		return 0;
	}

	@Override
	public void resetMaxHealth() {}

	@Override
	public void setHealth(double arg0) {}

	@Override
	public void setMaxHealth(double arg0) {}

	@Override
	public <T extends Projectile> T launchProjectile(Class<? extends T> arg0) {
		return null;
	}

	@Override
	public <T extends Projectile> T launchProjectile(Class<? extends T> arg0, Vector arg1) {
		return null;
	}

	@Override
	public EulerAngle getBodyPose() {
		return null;
	}

	@Override
	public ItemStack getBoots() {
		return null;
	}

	@Override
	public ItemStack getChestplate() {
		return null;
	}

	@Override
	public EulerAngle getHeadPose() {
		return null;
	}

	@Override
	public ItemStack getHelmet() {
		return null;
	}

	@Override
	public ItemStack getItemInHand() {
		return null;
	}

	@Override
	public EulerAngle getLeftArmPose() {
		return null;
	}

	@Override
	public EulerAngle getLeftLegPose() {
		return null;
	}

	@Override
	public ItemStack getLeggings() {
		return null;
	}

	@Override
	public EulerAngle getRightArmPose() {
		return null;
	}

	@Override
	public EulerAngle getRightLegPose() {
		return null;
	}

	@Override
	public boolean hasArms() {
		return false;
	}

	@Override
	public boolean hasBasePlate() {
		return false;
	}

	@Override
	public boolean hasGravity() {
		return false;
	}

	@Override
	public boolean isMarker() {
		return false;
	}

	@Override
	public boolean isSmall() {
		return false;
	}

	@Override
	public boolean isVisible() {
		return false;
	}

	@Override
	public void setArms(boolean arg0) {}

	@Override
	public void setBasePlate(boolean arg0) {}

	@Override
	public void setBodyPose(EulerAngle arg0) {}

	@Override
	public void setBoots(ItemStack arg0) {}

	@Override
	public void setChestplate(ItemStack arg0) {}

	@Override
	public void setGravity(boolean arg0) {}

	@Override
	public void setHeadPose(EulerAngle arg0) {}

	@Override
	public void setHelmet(ItemStack arg0) {}

	@Override
	public void setItemInHand(ItemStack arg0) {}

	@Override
	public void setLeftArmPose(EulerAngle arg0) {}

	@Override
	public void setLeftLegPose(EulerAngle arg0) {}

	@Override
	public void setLeggings(ItemStack arg0) {}

	@Override
	public void setMarker(boolean arg0) {}

	@Override
	public void setRightArmPose(EulerAngle arg0) {}

	@Override
	public void setRightLegPose(EulerAngle arg0) {}

	@Override
	public void setSmall(boolean arg0) {}

	@Override
	public void setVisible(boolean arg0) {}

	//version-1.11
	public PotionEffect getPotionEffect(PotionEffectType arg0) {
		return null;
	}

	//version-1.11
	public boolean addScoreboardTag(String arg0) {
		return false;
	}

	//version-1.11
	public int getPortalCooldown() {
		return 0;
	}

	//version-1.11
	public Set<String> getScoreboardTags() {
		return null;
	}

	//version-1.11
	public boolean removeScoreboardTag(String arg0) {
		return false;
	}

	//version-1.11
	public void setPortalCooldown(int arg0) {}

	public Arrow shootArrow() {
		return null;
	}

	public Egg throwEgg() {
		return null;
	}

	public Snowball throwSnowball() {
		return null;
	}

	@Override
	public String toString() {
		return "DummyStand:[Name=" + customname + ", x=" + loc.getBlockX() + ", y=" + loc.getBlockY() + ", z=" + loc.getBlockZ() + "]";

	}

}
