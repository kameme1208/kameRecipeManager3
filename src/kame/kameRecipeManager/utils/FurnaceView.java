package kame.kameRecipeManager.utils;

import org.bukkit.block.Furnace;

public class FurnaceView {
	private Furnace block;

	public FurnaceView(Furnace block) {
		this.block = block;
	}

	public void getProcessing() {
		block.getCookTime();
	}

}
