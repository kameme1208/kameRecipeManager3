package kame.kameRecipeManager.utils;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.ArmorStand;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.google.common.collect.Sets;

import kame.kameRecipeManager.Main;

public class StandUtils {

	private static final ItemStack kames;
	static {
		ItemStack item = new ItemStack(Material.COOKED_CHICKEN);
		ItemMeta im = item.getItemMeta();
		im.setDisplayName("[kameRecipeManager]");
		item.setItemMeta(im);
		kames = item;
	}

	/**
	 * レシピプラグインのアーマースタンドを作成する、
	 * @param loc スポーン場所
	 * @param item 持たせるアイテム
	 * @return
	 */
	public static ArmorStand createStand(Location loc, ItemStack item) {
		ArmorStand entity = (ArmorStand) loc.getWorld().spawn(loc/*.clone().add(0, 300, 0)*/, ArmorStand.class);
		entity.setVisible(false);
		entity.setBoots(kames);
		entity.setLeggings(kames);
		entity.setSmall(true);
		entity.setCustomNameVisible(false);
		entity.setCustomName("\n");
		entity.setGravity(false);
		entity.setBasePlate(false);
		entity.setMarker(true);
		entity.setChestplate(item);
		return entity;
	}

	/**
	 * アーマースタンドをクラフトスタンドに設定する
	 * @param entity
	 * @param item
	 * @param visible
	 * @return
	 */
	public static ArmorStand setCraftStand(ArmorStand entity, ItemStack item, boolean visible) {
		entity.setCustomName(item.getItemMeta().getDisplayName());
		entity.setCustomNameVisible(visible);
		return entity;
	}

	public static boolean isKameStand(ArmorStand stand) {
		ItemStack item = stand.getBoots();
		return item != null && item.equals(kames);
	}

	public static boolean isCraftStand(ArmorStand stand) {
		return isKameStand(stand) && !stand.getCustomName().equals("\n");
	}

	public static ArmorStand getKameStand(Block block) {
		Location loc = block.getLocation().add(0.5D, Main.getOffsetY(), 0.5);
		return (ArmorStand) loc.getWorld().getNearbyEntities(loc, 0.1, 0.1, 0.1).stream()
			.filter(x -> x.getLocation().equals(loc) && x instanceof ArmorStand && isKameStand((ArmorStand) x)).findFirst().orElse(null);
	}

	public static ArmorStand getCraftStand(Block block) {
		Location loc = block.getLocation().add(0.5D, Main.getOffsetY(), 0.5);
		return (ArmorStand) loc.getWorld().getNearbyEntities(loc, 0.1, 0.1, 0.1).stream()
			.filter(x -> x.getLocation().equals(loc) && x instanceof ArmorStand && isCraftStand((ArmorStand) x)).findFirst().orElse(null);
	}

	public static Set<String> getIncludeName(ArmorStand stand) {
		if(stand == null || stand instanceof DummyStand) return new HashSet<>();
		Set<String> list = Sets.newHashSet(stand.getCustomName());
		for(String line : stand.getChestplate().getItemMeta().getLore()) {
			int index = line.indexOf("@include:");
			if(index == -1)continue;
			list.add(line.substring(index + 9));
		}
		return list;
	}

	public static float getBurnRate(Block block) {
		ArmorStand stand = getKameStand(block);
		if(stand == null)return 1;
		List<String> lore = stand.getChestplate().getItemMeta().getLore();
		for(String l : lore)if(l.toLowerCase().matches("@burnrate:[0-9]+(.[0-9]+)?"))
			return Float.parseFloat(l.split(":")[1]);
		return 1;
	}

	public static float getCookRate(Block block) {
		ArmorStand stand = getKameStand(block);
		if(stand == null)return 1;
		List<String> lore = stand.getChestplate().getItemMeta().getLore();
		for(String l : lore)if(l.toLowerCase().matches("@cookrate:[0-9]+(.[0-9]+)?"))
			return Float.parseFloat(l.split(":")[1]);
		return 1;
	}
}
