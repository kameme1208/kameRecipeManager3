package kame.kameRecipeManager.utils;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

import kame.kameRecipeManager.Main;
import kame.kameRecipeManager.command.RecipeCommand;
import kame.kameRecipeManager.recipe.KameRecipes;
import kame.kameRecipeManager.recipe.option.RecipeType;
import kame.kameRecipeManager.recipe.process.ActiveFurnace;
import kame.kameRecipeManager.recipe.process.ProcessingFurnace;

public class Display {
	private static HashMap<Player, Display> view = new HashMap<>();
	private static HashMap<Player, Integer> removes = new HashMap<>();
	private Scoreboard score;
	private String title;
	private int scores;
	private Block block;

	static {
		new BukkitRunnable() {
			public void run() {
				view.entrySet().stream().filter(x -> x.getValue().block != null)
				.forEach(x -> updateFurnace(x.getKey(), ActiveFurnace.getActive(x.getValue().block)));
				Set<Entry<Player, Integer>> entry = removes.entrySet();
				entry.forEach(x -> x.setValue(RecipeCommand.contains(x.getKey().getUniqueId()) ? 40 : x.getValue() - 1));
				entry.removeIf(x -> (x.getValue() == 0) && remove(x.getKey()));
			}
			private boolean remove(Player player) {
				view.remove(player);
				player.setScoreboard(Bukkit.getScoreboardManager().getMainScoreboard());
				return true;
			}
		}.runTaskTimer(Main.getInstance(), 0, 1);
	}

	public Display(Player player, String title, Set<String> include, RecipeType... types) {
		removes.remove(player);
		include.add(title);
		this.title = title;
		if(types[0] == RecipeType.Furnace) {
			if(title.length() > 8)title = title.substring(0, 8);
			title = title + " §c 0§r%";
		}else {
			if(title.length() > 16)title = title.substring(0, 16);
		}
		player.setScoreboard(Bukkit.getScoreboardManager().getNewScoreboard());
		score = player.getScoreboard();
		setDiplay(score.registerNewObjective(title, title));
		scores = KameRecipes.getRecipes(include, types).size();
		score.getObjective(title).getScore(Lang.list()).setScore(scores);
		view.put(player, this);
	}
	
	private void setDiplay(Objective obj) {
		if(Main.getInstance().getConfig().getBoolean("display.enable")) {
			obj.setDisplaySlot(DisplaySlot.SIDEBAR);
		}
	}
	
	public static void resetDiplay(HumanEntity player) {
		if(view.containsKey(player)) {
			Display disp = view.get(player);
			String display = disp.title;
			disp.score.getObjectives().forEach(x -> {
				disp.title = x.getDisplayName();
				x.unregister();
			});
			Objective obj = disp.score.registerNewObjective(disp.title, disp.title);
			disp.title = display;
			disp.setDiplay(obj);
			obj.getScore(Lang.list()).setScore(disp.scores);
			obj.getScore("§b§lexp§e 0.0").setScore(-1);
		}
	}

	public void resisterFurnace(Block block) {
		this.block = block;
		Float exp = ActiveFurnace.exp.get(block.getLocation());
		if(exp == null)exp = 0F;
		score.getObjectives().iterator().
		next().getScore("§b§lexp§e" + (exp < 10000 ? String.format(" %.1f", exp) : "+9999.9")).setScore(-1);
	}

	public static void updateFurnace(HumanEntity player, ProcessingFurnace furnace) {
		if(furnace == null)return;
		int per = furnace.getCookPer();
		if(view.containsKey(player)) {
			Display disp = view.get(player);
			String title = disp.title;
			if(title.length() > 8)title = title.substring(0, 8);
			title = title + " §c" + (per < 10 ? " " + per : per) + "§r%";
			disp.score.getObjectives().forEach(x -> x.unregister());
			Objective obj = disp.score.registerNewObjective(title, title);
			disp.setDiplay(obj);
			obj.getScore(Lang.list()).setScore(disp.scores);
			Float exp = ActiveFurnace.exp.get(furnace.getBlock().getLocation());
			if(exp == null)exp = 0F;
			obj.getScore("§b§lexp§e" + (exp < 10000 ? String.format(" %.1f", exp) : "+9999.9")).setScore(-1);
		}
	}

	public static void remove(HumanEntity player) {
		removes.put((Player) player, 40);
	}
}
