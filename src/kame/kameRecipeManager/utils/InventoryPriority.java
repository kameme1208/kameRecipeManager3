package kame.kameRecipeManager.utils;

import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

public class InventoryPriority {
	private enum Priority {
		/* ====== Inventory ID ======
		 * 35 34 33 32 31 30 29 28 27
		 * 26 25 24 23 22 21 20 19 18
		 * 17 16 15 14 13 12 11 10 09
		 * 08 07 06 05 04 03 02 01 00
		 */

		IO_00(8),	IO_01(7),	IO_02(6),	IO_03(5),	IO_04(4),	IO_05(3),	IO_06(2),	IO_07(1),	IO_08(0),
		IO_09(35),	IO_10(34),	IO_11(33),	IO_12(32),	IO_13(31),	IO_14(30),	IO_15(29),	IO_16(28),	IO_17(27),
		IO_18(26),	IO_19(25),	IO_20(24),	IO_21(23),	IO_22(22),	IO_23(21),	IO_24(20),	IO_25(19),	IO_26(18),
		IO_27(17),	IO_28(16),	IO_29(15),	IO_30(14),	IO_31(13),	IO_32(12),	IO_33(11),	IO_34(10),	IO_35(9),
		;
		private int invID;

		private Priority(int invid) {
			invID = invid;
		}
	}

	/**
	 * インベントリのアイテムが入る残容量を返す
	 * @param inv プレイヤーのインベントリ
	 * @param item 追加するアイテム/追加する分を
	 * @return
	 */
	public static int freeCapacity(PlayerInventory inv, ItemStack item) {
		int capacity = 0;
		int maxstacksize = item.getMaxStackSize();
		for(Priority pr : Priority.values()) {
			ItemStack invitem = inv.getItem(pr.invID);
			if(invitem == null)capacity += maxstacksize;
			else if(equals(item, invitem))
				capacity = capacity + maxstacksize - invitem.getAmount();
		}
		return capacity;
	}

	/**
	 * インベントリに自動で指定された分のアイテムを入れる
	 * Inventory::addとの違い=アイテムの入力順位がクライアント側のシフト操作に合わせている
	 * @param inv		:プレイヤーのインベントリ
	 * @param item		:追加するアイテム / 追加する合計分を64を超えていてもsetAmountで指定する事
	 * @return amount	:アイテムが入りきらなかった場合はその個数、すべて入った場合は0
	 */
	public static int addInventory(PlayerInventory inv, ItemStack item) {
		int addItems = item.getAmount();
		int maxstacksize = item.getMaxStackSize();
		for(Priority pr : Priority.values()) {
			if(addItems <= 0)break;
			ItemStack add = inv.getItem(pr.invID);
			if(add != null && add.getAmount() < maxstacksize && equals(item, add)) {
				int before = add.getAmount();
				int after = before + addItems < maxstacksize ? before + addItems : maxstacksize;
				addItems -= after - before;
				add.setAmount(after);
				inv.setItem(pr.invID, add);
			}
		}
		for(Priority pr : Priority.values()) {
			if(addItems <= 0)break;
			if(inv.getItem(pr.invID) == null) {
				int after = addItems < maxstacksize ? addItems : maxstacksize;
				ItemStack add = item.clone();
				add.setAmount(after);
				addItems -= after;
				inv.setItem(pr.invID, add);
			}
		}
		return addItems;
	}

	public static boolean equals(ItemStack base, ItemStack item) {
		base = base.clone();
		item = item.clone();
		base.setAmount(1);
		item.setAmount(1);
		return base.equals(item);
	}


}
