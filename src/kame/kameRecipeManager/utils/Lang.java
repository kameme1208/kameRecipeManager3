package kame.kameRecipeManager.utils;

import org.bukkit.ChatColor;

import kame.kameRecipeManager.Main;

public class Lang {
	private static String replace(String line) {
		
		return line != null ? ChatColor.translateAlternateColorCodes('&', line) : "";
	}
	public static String crafttable() {
		return replace(Main.getConf().getString("display.crafttable"));
	}

	public static String furnace() {
		return replace(Main.getConf().getString("display.furnace"));
	}


	public static String anvil() {
		return replace(Main.getConf().getString("display.anvil"));
	}

	public static String brewer() {
		return replace(Main.getConf().getString("display.brewstand"));
	}

	public static String list() {
		return replace(Main.getConf().getString("display.recipeList"));
	}

	public static String keepDisp(boolean bool) {
		return replace(bool ? Main.getConf().getString("message.DispKeepOn") : Main.getConf().getString("message.DispKeepOff"));
	}

	public static String anvilusing() {
		return replace(Main.getConf().getString("message.Anvil-using"));
	}

	public static String unknownError() {
		return replace(Main.getConf().getString("message.UnknownError"));
	}

	public static String reloaded() {
		return replace(Main.getConf().getString("message.Inventory-reloaded"));
	}

	public static String anvilSpel() {
		return replace(Main.getConf().getString("message.Anvil-Enterspell"));
	}

	public static String changename() {
		return replace(Main.getConf().getString("message.FailedChangeName"));
	}
}
