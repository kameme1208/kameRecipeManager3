package kame.kameRecipeManager.command;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.craftbukkit.v1_10_R1.entity.CraftPlayer;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import kame.kameRecipeManager.Main;
import kame.kameRecipeManager.reader.FileReader;
import kame.kameRecipeManager.recipe.KameRecipes;
import kame.kameRecipeManager.utils.Lang;

public class RecipeCommand implements CommandExecutor, TabCompleter {

	private static HashSet<UUID> keeps = new HashSet<UUID>();

	public static boolean contains(UUID uid) {
		return keeps.contains(uid);
	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command label, String cmd, String[] args) {
		if(sender.hasPermission("kamerecipe.user") && args.length == 1 && args[0].equals("keep") && (sender instanceof Player)) {
			if(keeps.contains(((Player) sender).getUniqueId())) {
				keeps.remove(((Player) sender).getUniqueId());
				sender.sendMessage(Lang.keepDisp(false));
			} else {
				keeps.add(((Player) sender).getUniqueId());
				sender.sendMessage(Lang.keepDisp(true));
			}
		}
		if(!sender.hasPermission("kamerecipe.admin"))return false;
		if(args.length == 1 && args[0].equals("list")) {
			for(String name : KameRecipes.getAllRecipes().keySet())
				sender.sendMessage("§a[kame.] " + "§r" + name + "§r" + " in " + KameRecipes.getRecipe(name).size() + " recipe[s]");
		}
		if(args.length == 1 && args[0].equals("reload")) {
			Main.loadConfig();
			FileReader.readRecipes();
			sender.sendMessage("§a[kame.] §dconfig&recipes Reloaded!");
			Bukkit.getOnlinePlayers().forEach(player -> Main.sendWarnings(player));
		}
		if(args.length > 0 && args[0].equals("debug")) {
			Main.debug = !Main.debug;
			sender.sendMessage("[kame.] DebugMode= " + Main.debug);
		}
		if(args.length > 1 && sender instanceof Player) {
			switch(args[0]) {
			case "sounds":
				try {
					Sound s = Sound.valueOf(args[1]);
					float volume = args.length > 2 && args[2].matches("[0-9](.[0-9]+)?") ? Float.parseFloat(args[2]) : 1;
					float pitch = args.length > 3 && args[3].matches("[0-9](.[0-9]+)?") ? Float.parseFloat(args[3]) : 1;
					if(s != null)((Player)sender).playSound(((Player)sender).getLocation(), s, volume, pitch);
					sender.sendMessage("[kame.] Sounds. ID= §e" + s.ordinal() + "§r Name= §e" + s.name());
				}catch(IllegalArgumentException e) {
					sender.sendMessage("[kame.] 選択されたものが見つかりませんでした");
					sender.sendMessage("[kame.] " + e.getMessage());
				}
				break;
			case "effects":
				try {
					Particle s = Particle.valueOf(args[1]);
					Object obj = null;
					if(s.equals(Particle.BLOCK_CRACK) || s.equals(Particle.BLOCK_DUST)) {
						obj = new ItemStack(Material.values()[args.length > 2 && args[2].matches("[0-9]+") ? Integer.parseInt(args[2]) : 1]).getData();
					}
					if(s.equals(Particle.ITEM_CRACK)) {
						obj = new ItemStack(Material.values()[args.length > 2 && args[2].matches("[0-9]+") ? Integer.parseInt(args[2]) : 1]);
					}
					if(s != null)((CraftPlayer)sender).spawnParticle(s, ((Player)sender).getEyeLocation().
							add(((Player)sender).getLocation().getDirection().multiply(3)), 16, 0, 0, 0, 0.1, obj);
					sender.sendMessage("[kame.] Effects. ID= §e" + s.ordinal() + "§r Name= §e" + s.name());
				}catch(IllegalArgumentException e) {
					sender.sendMessage("[kame.] 選択されたものが見つかりませんでした");
					sender.sendMessage("[kame.] " + e.getMessage());

				}
				break;
			case "items":
				break;
			case "enchs":
				if(args[0].equals("enchs")) {
					try {
						Enchantment ench = Enchantment.getByName(args[1]);
						sender.sendMessage("[kame.] Enchantment. ID= §e" + ench.getId() + "§r Name= §e" + ench.getName());
					}catch(IllegalArgumentException e) {
						sender.sendMessage("[kame.] 選択されたものが見つかりませんでした");
						sender.sendMessage("[kame.] " + e.getMessage());
					}
				}
				break;
			case "flags":
				break;
			default:
			}
		}
		return true;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command label, String cmd, String[] args) {
		List<String> list = new ArrayList<String>();
		if(args.length == 2 && sender.hasPermission("kamerecipe.admin")) {
			switch(args[0]) {
			case "sounds":
				for(Sound s : Sound.values())completion(list, s.toString(), args[1]);
				break;
			case "effects":
				for(Particle s : Particle.values())completion(list, s.toString(), args[1]);
				break;
			case "items":
				if(sender instanceof Player) {
					PlayerInventory inv = ((Player)sender).getInventory();
					ItemStack item = inv.getItem(inv.getHeldItemSlot());
					list.add(item != null ? item.getType().toString() : "AIR");
				}
				for(Material s : Material.values())completion(list, s.toString(), args[1]);
				return list;
			case "flags":
				for(ItemFlag s : ItemFlag.values())completion(list, s.toString(), args[1]);
				break;
			case "enchs":
				for(Enchantment s : Enchantment.values())completion(list, s.getName(), args[1]);
				break;
			}
		}
		if(sender.hasPermission("kamerecipe.admin") && args.length == 1) {
			String[] arg = {"reload", "list", "items", "sounds", "effects","enchs", "flags"};
			for(String str : arg)if(str.startsWith(args[0]))list.add(str);
		}
		if(args.length == 1 && sender.hasPermission("kamerecipe.user")) {
			String[] arg = {"keep"};
			for(String str : arg)if(str.startsWith(args[0]))list.add(str);
		}
		Collections.sort(list);
		return list;
	}

	private void completion(Collection<String> list, String base, String line) {
		if(base.toLowerCase().startsWith(line.toLowerCase()))list.add(base);
	}

}
