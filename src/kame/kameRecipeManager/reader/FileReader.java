package kame.kameRecipeManager.reader;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileFilter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URLDecoder;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.jar.JarFile;
import java.util.stream.Stream;

import org.bukkit.Bukkit;
import org.bukkit.inventory.FurnaceRecipe;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.ShapelessRecipe;

import com.google.common.collect.Lists;

import kame.kameRecipeManager.Main;
import kame.kameRecipeManager.recipe.KameRecipes;
import kame.kameRecipeManager.recipe.VanillaFurnaceRecipe;
import kame.kameRecipeManager.recipe.VanillaShapedRecipe;
import kame.kameRecipeManager.recipe.VanillaShapelessRecipe;
import kame.kameRecipeManager.utils.Lang;


public class FileReader {
	private static final File DIR = Main.getInstance().getDataFolder();
	private class Filter implements FileFilter {
		public boolean accept(File path) {
			return path.getName().endsWith(".txt") && path.getName().charAt(1) != '$';
		}
	}

	public static void readRecipes() {
		new FileReader();
	}

	private FileReader() {
		long time = System.currentTimeMillis();
		KameRecipes.initialize();
		List<Recipe> recipes = Lists.newArrayList(Bukkit.recipeIterator());
		try {
			loadDirectory();
		} catch (IOException e) {
			e.printStackTrace();
			Bukkit.getLogger().severe("<kameRecipeManager> ====================ERROR======================");
			Bukkit.getLogger().severe("<kameRecipeManager> 何らかの原因でファイルの読み込みに失敗しました!");
			Bukkit.getLogger().severe("<kameRecipeManager> " + e);
			for(StackTraceElement el : e.getStackTrace())Bukkit.getLogger().severe("\t".concat(el.toString()));
			Bukkit.getLogger().severe("<kameRecipeManager> ====================ERROR======================");
		}
		Bukkit.getLogger().warning("<kameRecipe> load recipe " + (float) (System.currentTimeMillis() - time) / 1000.0F + "[s]");
		loadDefault(recipes);
	}

	private void textcopy(String from, String folder, String filename, String... line) throws IOException {
		String path = URLDecoder.decode(getClass().getResource(from).getPath(), "UTF-8").substring(6);
		JarFile jarfile = new JarFile(new File(path.substring(0, path.lastIndexOf('!'))));
		Stream<String> reader = new BufferedReader(new InputStreamReader(jarfile.getInputStream(jarfile.getEntry(path.substring(path.lastIndexOf('!')+2))), "UTF-8")).lines();
		File file = new File(DIR, folder);
		if(!file.exists())file.mkdir();
		try(BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(new File(file, filename)), "UTF-8"))) {
			for(String w : line)writer.write(w);
			reader.forEach(x -> {try {writer.write(x.concat("\n"));} catch (IOException e) {}});
		}finally {
			jarfile.close();
		}
	}

	private void loadDirectory() throws IOException {
		File files = new File(DIR, "/recipes");
		if(!files.exists())files.mkdir();
		File filter[] = files.listFiles(new Filter());
		if(filter.length == 0) {
			textcopy("sample.txt", "/recipes", "sample.txt");
			textcopy("fuelsample.txt", "/recipes/fuels", Lang.furnace().concat(".txt"));
			Bukkit.getLogger().warning("<kameRecipeManager> clone sample recipe!");
			filter = files.listFiles(new Filter());
		}
		StringBuilder builder = new StringBuilder("[kameRecipeManager] detected recipe " + filter.length + " file[s] {");
		for(File f : filter)builder.append(" ").append(f.getName());
		Bukkit.getLogger().info(builder.append(" }").toString());
		FileParser parse = new FileParser();
		for(File file : filter) {
			parse.parseFile(file);
		}
		loadFuels(parse);
		parse.finalyze();
	}

	private void loadFuels(FileParser parse) throws IOException {
		File files = new File(DIR, "/recipes/fuels");
		if(!files.exists())files.mkdirs();
		textcopy("fuelsample.txt", "/recipes/fuels", "$sample[先頭に$を付けると読み込まれません].txt"
				,"このファイルは頻繁にアップデートされる可能性があるので起動時に毎回初期状態に書き換えられます\n"
				,"使用法 : ファイル名がかまどに対応する名前になっています\n"
				,"       : このファイル名の場合rewrite_every_startupというかまどに下の燃料を追加していきます\n"
				,"       : 他の所で使うFUELの宣言はここでは必要ありません\n"
				,"       : このフォルダ内にFUELレシピのファイルを追加することでも燃料を追加できます\n"
				,"       : デフォルトの燃料を使うならこのレシピを使用するかまどの名前でコピーペーストしてください\n");
		File filter[] = files.listFiles(new Filter());
		StringBuilder builder = new StringBuilder("[kameRecipeManager] detected fuel " + filter.length + " file[s] {");
		for(File fuel : filter)builder.append(" ").append(fuel.getName());
		Bukkit.getLogger().info(builder.append(" }").toString());
		for(File file : filter)parse.parseFuel(file);
	}

	public void loadDefault(List<Recipe> recipes) {
		for(Recipe r : recipes) {
			if((r instanceof ShapedRecipe)) {
				Collection<ItemStack> items = ((ShapedRecipe) r).getIngredientMap().values();
				if((items.size() == 1 || items.size() == 9) && new HashSet<>(items).size() == 1) {
					ShapelessRecipe re = new ShapelessRecipe(r.getResult());
					items.forEach(item -> re.addIngredient(item.getData()));
					KameRecipes.addDefaultRecipe("COMBINE:" + Lang.crafttable(), new VanillaShapelessRecipe(re));
				}else {
					KameRecipes.addDefaultRecipe("CRAFT:" + Lang.crafttable(), new VanillaShapedRecipe((ShapedRecipe) r));
				}
			}else if((r instanceof ShapelessRecipe)) {
				KameRecipes.addDefaultRecipe("COMBINE:" + Lang.crafttable(), new VanillaShapelessRecipe((ShapelessRecipe) r));
			}else if((r instanceof FurnaceRecipe)) {
				KameRecipes.addDefaultRecipe("SMELT:" + Lang.furnace(), new VanillaFurnaceRecipe((FurnaceRecipe) r));
			}else {
			}
		}
	}

}
