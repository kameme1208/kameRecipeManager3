package kame.kameRecipeManager.reader.recipe;

import java.util.ArrayList;

import org.bukkit.inventory.ItemStack;

import kame.kameRecipeManager.reader.exception.IllegalItemException;
import kame.kameRecipeManager.reader.exception.IllegalItemException.ErrorType;
import kame.kameRecipeManager.recipe.KSmashRecipe;
import kame.kameRecipeManager.recipe.KameRecipes;
import kame.kameRecipeManager.recipe.option.CraftingEffect;
import kame.kameRecipeManager.recipe.option.CraftingSound;
import kame.kameRecipeManager.recipe.option.RecipeType;

public class BufferSmashRecipe extends BufferedRecipe {

	private ArrayList<ItemStack> input = new ArrayList<>();
	private String spell;
	private Short consume;
	private ItemStack smashitem;
	private CraftingEffect effect = new CraftingEffect();
	private CraftingSound sound = new CraftingSound();

	public BufferSmashRecipe(String recipename) {
		super(recipename);
		type = RecipeType.Smash;
	}

	private boolean addSmash(ItemStack item) {
		return input.add(item);
	}

	private boolean setCost(short cost) {
		if(this.consume == null) {
			this.consume = cost;
			return true;
		}else {
			return false;
		}
	}

	private boolean setSmashItem(ItemStack item) {
		if(this.smashitem == null) {
			this.smashitem = item;
			return true;
		}else {
			return false;
		}
	}

	private boolean setSpell(String spell) {
		if(this.spell == null) {
			this.spell = spell;
			return true;
		}else {
			return false;
		}
	}

	@Override
	public void parseLine(String line) throws IllegalItemException {
		super.parseLine(line);
		if(line.matches("I[1-9 ]= .*")) {
			ItemStack item = parseItemStack(line.substring(4));
			for(int i = line.charAt(1) == ' ' ? 1 : line.charAt(1) - 48; i != 0; i--) {
				if(!addSmash(item))throw new IllegalItemException(ErrorType.OTHER, line, "InputItemError too few items! ");
			}
		}
		if(line.toLowerCase().matches("\\$spell:.*")) {
			if(!setSpell(line.substring(7)))
				throw new IllegalItemException(ErrorType.OTHER, line, "SmashSpellError ", " already set spell!");
		}
		if(line.toLowerCase().matches("\\$effect:.*")) {
			line = line.substring(8);
			String str[] = line.split(":", 2);
			effect = new CraftingEffect(str[0], str.length > 1 ? str[1].split(":") : new String[0]);
		}
		if(line.toLowerCase().matches("\\$sound:.*")) {
			line = line.substring(7);
			String str[] = line.split(":", 2);
			sound = new CraftingSound(str[0], str.length > 1 ? str[1].split(":") : new String[0]);
		}
		if(line.toLowerCase().matches("\\$needitem:.*")) {
			if(!setSmashItem(parseItemStack(line.substring(10))))
				throw new IllegalItemException(ErrorType.OTHER, line, "InputItemError already set items! ");
		}
		if(line.toLowerCase().matches("\\$cost:-?[0-9]+")) {
			if(!setCost(Short.parseShort(line.substring(6))))
				throw new IllegalItemException(ErrorType.OTHER, line, "InputItemError already set items! ");
		}
	}

	@Override
	public void createRecipe() {
		KameRecipes.addRecipe(type.getName() + ":" + recipename, new KSmashRecipe(null, result, smashitem, consume == null ? 10 : consume, input, spell == null ? "" : spell, effect, sound, options, products, checks));
	}

	@Override
	public String finaly() {
		super.finarize();
		if(input.size() == 0)return "Input item is empty! (I = Items)";
		if(result == null)return "Output item is not set! (=== Items)";
		if(type != RecipeType.Smash)return "recipe is dropped... please check recipe";
		return "";
	}
}
