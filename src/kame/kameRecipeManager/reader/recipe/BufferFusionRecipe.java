package kame.kameRecipeManager.reader.recipe;

import java.util.ArrayList;

import org.bukkit.inventory.ItemStack;

import kame.kameRecipeManager.reader.exception.IllegalItemException;
import kame.kameRecipeManager.reader.exception.IllegalItemException.ErrorType;
import kame.kameRecipeManager.recipe.KFusionRecipe;
import kame.kameRecipeManager.recipe.KameRecipes;
import kame.kameRecipeManager.recipe.option.CraftingEffect;
import kame.kameRecipeManager.recipe.option.CraftingSound;
import kame.kameRecipeManager.recipe.option.RecipeType;
import kame.kameRecipeManager.utils.Utils;

public class BufferFusionRecipe extends BufferedRecipe {

	private ArrayList<ItemStack> input = new ArrayList<>();
	private int time = -100;
	private CraftingEffect effect = new CraftingEffect();
	private CraftingSound sound = new CraftingSound();

	public BufferFusionRecipe(String recipename) {
		super(recipename);
		type = RecipeType.Fusion;
	}

	private boolean addFusion(ItemStack item) {
		return this.input.size() < 4 ? input.add(item) : false;
	}

	private boolean setTimes(int time) throws IllegalItemException {
		if(time <= 0)throw new IllegalItemException(ErrorType.OTHER, "", "FusionTimeError ", time , " is smaller! the number bigger than 0!");
		return this.time == -100 ? (this.time = time) != 0 : false;
	}

	@Override
	public void parseLine(String line) throws IllegalItemException {
		super.parseLine(line);
		if(line.matches("I[1-3 ]= .*")) {
			ItemStack item = parseItemStack(line.substring(4));
			for(int i = line.charAt(1) == ' ' ? 1 : line.charAt(1) - 48; i != 0; i--) {
				if(!addFusion(item))throw new IllegalItemException(ErrorType.OTHER, line, "InputItemError too few items! ");
			}
		}
		if(line.toLowerCase().matches("\\$times:[0-9]+")) {
			if(!setTimes(Utils.toInt(line.substring(7), 100)))
				throw new IllegalItemException(ErrorType.OTHER, line, "FusionTimeError ", " already set times!");
		}
		if(line.toLowerCase().matches("\\$effect:.*")) {
			line = line.substring(8);
			String str[] = line.split(":", 2);
			effect = new CraftingEffect(str[0], str.length > 1 ? str[1].split(":") : new String[0]);
		}
		if(line.toLowerCase().matches("\\$sound:.*")) {
			line = line.substring(7);
			String str[] = line.split(":", 2);
			sound = new CraftingSound(str[0], str.length > 1 ? str[1].split(":") : new String[0]);
		}
	}

	@Override
	public void createRecipe() {
		KameRecipes.addRecipe(type.getName() + ":" + recipename, new KFusionRecipe(null, result, input, time == -100 ? 100 : time, effect, sound, options, products, checks));
	}

	@Override
	public String finaly() {
		super.finarize();
		if(input.size() == 0)return "Input item is empty! (I = Items)";
		if(result == null)return "Output item is not set! (=== Items)";
		if(type != RecipeType.Fusion)return "recipe is dropped... please check recipe";
		return "";
	}
}
