package kame.kameRecipeManager.reader.recipe;

import org.bukkit.Material;
import org.bukkit.inventory.FurnaceRecipe;
import org.bukkit.inventory.ItemStack;

import kame.kameRecipeManager.reader.exception.IllegalItemException;
import kame.kameRecipeManager.reader.exception.IllegalItemException.ErrorType;
import kame.kameRecipeManager.recipe.KFurnaceRecipe;
import kame.kameRecipeManager.recipe.KameRecipes;
import kame.kameRecipeManager.recipe.option.CraftingEffect;
import kame.kameRecipeManager.recipe.option.CraftingSound;
import kame.kameRecipeManager.recipe.option.RecipeType;
import kame.kameRecipeManager.utils.Utils;

public class BufferFurnaceRecipe extends BufferedRecipe {

	private ItemStack input;
	private int time = -200;
	private float exp;
	private CraftingEffect effect = new CraftingEffect();
	private CraftingSound sound = new CraftingSound();

	public BufferFurnaceRecipe(String recipename) {
		super(recipename);
		type = RecipeType.Furnace;
	}

	private boolean setInput(ItemStack item) {
		return this.input == null ? (input = item) != null : false;
	}

	private boolean setTimes(int time) throws IllegalItemException {
		if(time <= 0)throw new IllegalItemException(ErrorType.OTHER, "", "FurnaceTimeError ", time , " is smaller! the number bigger than 0!");
		return this.time == -200 ? (this.time = time) != 0 : false;
	}

	private boolean setExp(float exp) throws IllegalItemException {
		if(exp <= 0)throw new IllegalItemException(ErrorType.OTHER, "", "FurnaceExpError ", exp , " is smaller! the number bigger than 0!");
		return this.exp == 0 ? (this.exp = exp) != 0 : false;
	}


	@Override
	public void parseLine(String line) throws IllegalItemException {
		super.parseLine(line);
		if(line.matches("I = .*")) {
			ItemStack item = parseItemStack(line.substring(4));
			if(!setInput(item))throw new IllegalItemException(ErrorType.OTHER, line, "InputItemError too few items! ");
		}
		if(line.toLowerCase().matches("\\$times:[0-9]+")) {
			if(!setTimes(Utils.toInt(line.substring(7), 200)))
				throw new IllegalItemException(ErrorType.OTHER, line, "FurnaceTimelError ", " already set times!");
		}
		if(line.toLowerCase().matches("\\$exp:[0-9]+(\\.[0-9]+)?")) {
			if(!setExp(Float.parseFloat(line.substring(5))))
				throw new IllegalItemException(ErrorType.OTHER, line, "FurnaceExpError ", " already set times!");
		}
		if(line.toLowerCase().matches("\\$effect:.*")) {
			line = line.substring(8);
			String str[] = line.split(":", 2);
			effect = new CraftingEffect(str[0], str.length > 1 ? str[1].split(":") : new String[0]);
		}
		if(line.toLowerCase().matches("\\$sound:.*")) {
			line = line.substring(7);
			String str[] = line.split(":", 2);
			sound = new CraftingSound(str[0], str.length > 1 ? str[1].split(":") : new String[0]);
		}
	}

	@Override
	public void createRecipe() {
		FurnaceRecipe recipe = new FurnaceRecipe(result, input.getData(), exp);
		if(result.getType() == Material.AIR) {
			KameRecipes.addRemoveRecipe(recipe);
		}else {
			KameRecipes.addRecipe(type.getName() + ":" + recipename, new KFurnaceRecipe(recipe, input, time == -200 ? 200 : time, exp, effect, sound, options, products, checks));
		}
	}

	@Override
	public String finaly() {
		super.finarize();
		if(input == null)return "Input item is empty! (I = Items)";
		if(result == null)return "Output item is not set! (=== Items)";
		if(type != RecipeType.Furnace)return "recipe is dropped... please check recipe";
		return "";
	}
}
