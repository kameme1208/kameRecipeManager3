package kame.kameRecipeManager.reader.recipe;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import kame.kameRecipeManager.reader.exception.IllegalItemException;
import kame.kameRecipeManager.reader.exception.IllegalItemException.ErrorType;
import kame.kameRecipeManager.recipe.option.CheckMode;
import kame.kameRecipeManager.recipe.option.RecipeType;
import kame.kameRecipeManager.recipe.product.ProductAdd;
import kame.kameRecipeManager.recipe.product.ProductRecipe;
import kame.kameRecipeManager.recipe.product.ProductReplace;
import kame.kameRecipeManager.utils.Utils;

public abstract class BufferedRecipe {

	protected final String recipename;
	protected ItemStack result;
	protected ArrayList<String> options = new ArrayList<String>();
	protected ArrayList<ProductRecipe> products = new ArrayList<>();
	protected Set<CheckMode> checks = new HashSet<>();
	protected RecipeType type = RecipeType.Droped;
	protected ProductReplace replace;
	protected List<ProductAdd> add = new ArrayList<>();

	protected BufferedRecipe(String recipename) {
		this.recipename = recipename;
	}

	public String getName() {
		return recipename;
	}

	public void drop() {
		type = RecipeType.Droped;
	}

	public boolean isDrop() {
		return type == RecipeType.Droped;
	}

	public RecipeType getType() {
		return type;
	}

	public boolean setResult(ItemStack item) {
		if(result == null) {
			result = item;
			return true;
		}else {
			return false;
		}
	}

	public void addOption(String option) {
		options.add(option);
	}

	public void addProduct(ProductRecipe recipe) {
		products.add(recipe);
	}

	public void addCheckMode(CheckMode check) {
		if(check != null)checks.add(check);
	}

	protected ItemStack parseItemStack(String line) throws IllegalItemException {
		return Utils.parseItemStack(line);
	}

	private void createReplaceProduct(String line) throws IllegalItemException {
		int per = (int) (Double.parseDouble(line.substring(1, line.indexOf('%'))) * 1000);
		ItemStack item = parseItemStack(line.substring(line.indexOf(" = ") + 2));
		if(replace == null)replace = new ProductReplace();
		replace.addPerItem(per, item);
	}

	private void createAddProduct(String line) throws IllegalItemException {
		int per = (int) (Double.parseDouble(line.substring(1, line.indexOf('%'))) * 1000);
		ItemStack item = parseItemStack(line.substring(line.indexOf(" = ") + 2));
		add.add(new ProductAdd(per, item));
	}

	public void parseLine(String line) throws IllegalItemException {
		if(line.startsWith("=== ")) {
			if(!setResult(parseItemStack(line.substring(4))))
				throw new IllegalItemException(ErrorType.OTHER, line, "ResultItemError ", " already set items!");
		}
		if(line.toLowerCase().startsWith("$option @")) {
			for(String opt : line.substring(9).split(" @"))addOption(opt);
		}
		if(line.toLowerCase().startsWith("$checkmode:")) {
			for(String ckm : line.substring(11).split(" "))addCheckMode(CheckMode.fromName(ckm));
		}
		if(line.toLowerCase().startsWith("$nearblock:")) {
			String position = line.substring(11, line.indexOf(':', 12));
			if(!position.matches("-?[0-9]+,-?[0-9]+,-?[0-9]+"))
				throw new IllegalItemException(ErrorType.OTHER, line, "IllegalPositionError ", " already set items!");
			parseItemStack(line.substring(line.indexOf(':', 11) + 1));
			addOption(line.substring(1));
		}
		if(line.matches("[+-=][0-9]+(\\.[0-9]+)?% = .*")) {
			if(line.charAt(0) == '+') {
				createAddProduct(line);
			}else {
				createReplaceProduct(line);
			}
		}
	}

	public abstract void createRecipe();

	public abstract String finaly();

	public ItemStack getResult() {
		return result;
	}

	protected void finarize() {
		if(replace != null) {
			products.add(replace);
			if(result == null) {
				result = new ItemStack(Material.WORKBENCH);
				ItemMeta meta = result.getItemMeta();
				meta.setDisplayName("§b§lChance Recipe §ks");
				meta.setLore(replace.getList());
				result.setItemMeta(meta);
			}
		}
		products.addAll(add);
	}
}
