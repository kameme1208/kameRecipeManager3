package kame.kameRecipeManager.reader.recipe;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.ShapelessRecipe;

import kame.kameRecipeManager.reader.exception.IllegalItemException;
import kame.kameRecipeManager.reader.exception.IllegalItemException.ErrorType;
import kame.kameRecipeManager.recipe.KShapedRecipe;
import kame.kameRecipeManager.recipe.KShapelessRecipe;
import kame.kameRecipeManager.recipe.KameRecipes;
import kame.kameRecipeManager.recipe.option.RecipeType;

public class BufferShapedRecipe extends BufferedRecipe{

	private ArrayList<String> shapes = new ArrayList<>();
	private HashMap<Integer, ItemStack> rawmap = new HashMap<>();
	private HashMap<Character, ItemStack> map = new HashMap<>();
	private int sizeX = 0;
	private int sizeY = 0;

	public BufferShapedRecipe(String recipename) {
		super(recipename);
		super.type = RecipeType.Shaped;
	}

	private boolean addShape(String shape) {
		if(shape.length() > 3)return false;
		sizeX = Math.max(sizeX, shape.length());
		if(sizeY < 3)sizeY++;
		return shapes.size() < 3 ? shapes.add(shape) : false;
	}

	private boolean setItem(int key, ItemStack item) {
		return rawmap.containsKey(key) ? false : rawmap.put(key, item) == null;
	}

	private int[] parseRecipe() {
		int[] k = new int[sizeX * sizeY];
		for(int i = 0; i < sizeY; i++) {
			char c[] = shapes.get(i).toCharArray();
			for(int j = 0; j < sizeX; j++) {
				k[i*sizeX + j] = j < c.length ? c[j]-'0' : 0;
			}
		}
		return k;
	}

	@Override
	public void parseLine(String line) throws IllegalItemException {
		super.parseLine(line);
		if(line.matches("[0-9]( *\\+ *[0-9])?( *\\+ *[0-9])? *")) {
			if(!this.addShape(line.replaceAll("[^0-9]", "")))
				throw new IllegalItemException(ErrorType.OTHER, line, "ShapeError ", " too many shapes! ");
		}
		if(line.matches("[1-9] = .*")) {
			if(!this.setItem(line.charAt(0)-48, parseItemStack(line.substring(4))))
				throw new IllegalItemException(ErrorType.OTHER, line, "InputItemError [" + line.charAt(0) + "] already set items! ");
		}
	}

	@Override
	public void createRecipe() {
		ShapedRecipe recipe = new ShapedRecipe(result);
		recipe.shape(new String[] { "abc", "def", "ghi" });
		int[] cara = parseRecipe();
		StringBuilder builder = new StringBuilder();
		for(char c = 'a'; c < 'a'+ sizeX * sizeY ; c++) {
			builder.append(c);
			ItemStack mapitem = rawmap.get(cara[c-'a']);
			if(mapitem != null) {
				map.put(c, mapitem);
				recipe.setIngredient(c, mapitem.getData());
			}
			if(c % sizeX == 0)builder.append(' ');
		}
		if((map.size() == 1 || map.size() == 9) && new HashSet<>(map.values()).size() == 1) {
			ShapelessRecipe shapeless = new ShapelessRecipe(result);
			List<ItemStack> items = new ArrayList<>(map.values());
			items.forEach(item -> shapeless.addIngredient(item.getData()));
			KameRecipes.addRecipe("COMBINE:".concat(recipename), new KShapelessRecipe(shapeless, items, options, products, checks));
			return;
		}
		recipe.shape(builder.toString().split(" "));
		KameRecipes.addRecipe(type.getName() + ":" + recipename, new KShapedRecipe(recipe, map, options, products, checks));
		

	}

	@Override
	public String finaly() {
		super.finarize();
		if(shapes.size() == 0)return "Shape is not set! (0~9 + 0~9 + 0~9)";
		if(rawmap.size() == 0)return "Input item is empty! ([1-9] = Items)";
		if(result == null)return "Output item is not set! (=== Items)";
		if(type != RecipeType.Shaped)return "recipe is dropped... please check recipe";
		return "";
	}
}
