package kame.kameRecipeManager.reader.recipe;

import kame.kameRecipeManager.reader.exception.IllegalItemException;
import kame.kameRecipeManager.reader.exception.IllegalItemException.ErrorType;
import kame.kameRecipeManager.recipe.KFuelRecipe;
import kame.kameRecipeManager.recipe.KameRecipes;
import kame.kameRecipeManager.recipe.option.CheckMode;
import kame.kameRecipeManager.recipe.option.RecipeType;

public class BufferFuelRecipe extends BufferedRecipe {

	private int burntime;
	private float boost = 1;

	public BufferFuelRecipe(String recipename) {
		super(recipename);
		type = RecipeType.Fuel;
	}

	private boolean addTime(int time) {
		return burntime == 0 ? (burntime = time) != 0 : false;
	}

	private boolean addBoost(float b) {
		return boost == 1 ? (boost = b) != 1 : false;
	}

	@Override
	public void parseLine(String line) throws IllegalItemException {
		if(line.matches("[0-9]+ = .*")) {
			String str[] = line.split(" = ", 2);
			if(!addTime(Integer.parseInt(str[0]) * 20) || !setResult(parseItemStack(str[1])))
				throw new IllegalItemException(ErrorType.OTHER, line, "InputItemError already set items! ");
		}
		if(line.toLowerCase().matches("\\$option @.*")) {
			for(String opt : line.substring(9).split(" @"))addOption(opt);
		}
		if(line.toLowerCase().matches("\\$boost:[0-9]+(\\.[0-9]+)?")) {
			if(!addBoost(Float.parseFloat(line.substring(7))))
				throw new IllegalItemException(ErrorType.OTHER, line, "InputItemError already set items! ");
		}
		if(line.toLowerCase().matches("\\$checkmode:.*")) {
			for(String ckm : line.substring(11).split("|"))addCheckMode(CheckMode.fromName(ckm));
		}
	}
	
	@Override
	public void createRecipe() {
		KameRecipes.addRecipe(type.getName() + ":" + recipename, new KFuelRecipe(null, result, burntime, boost, options, checks));
	}

	@Override
	public String finaly() {
		super.finarize();
		if(burntime <= 0)return "Time is not set! (? = Items)";
		if(result == null)return "Output item is not set! (? = Items)";
		if(type != RecipeType.Fuel)return "recipe is dropped... please check recipe";
		return "";
	}
}
