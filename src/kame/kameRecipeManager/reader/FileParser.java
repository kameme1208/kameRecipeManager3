package kame.kameRecipeManager.reader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Level;

import org.bukkit.Bukkit;

import kame.kameRecipeManager.Main;
import kame.kameRecipeManager.reader.exception.IllegalItemException;
import kame.kameRecipeManager.reader.recipe.BufferFuelRecipe;
import kame.kameRecipeManager.reader.recipe.BufferFurnaceRecipe;
import kame.kameRecipeManager.reader.recipe.BufferFusionRecipe;
import kame.kameRecipeManager.reader.recipe.BufferShapedRecipe;
import kame.kameRecipeManager.reader.recipe.BufferShaplessRecipe;
import kame.kameRecipeManager.reader.recipe.BufferSmashRecipe;
import kame.kameRecipeManager.reader.recipe.BufferedRecipe;
import kame.kameRecipeManager.utils.Lang;
import net.md_5.bungee.api.ChatColor;

public class FileParser {
	private long time = System.currentTimeMillis();
	private int LINE;
	private int READ;
	private int WARN;
	private int ADDED;
	private String filename;
	private static int FAIL, FINE;
	private BufferedRecipe buffer;
	FileParser() {
		LINE = FAIL = FINE = ADDED = 0;
		System.out.println("<kameRecipe> ===============================================");
	}
	public void finalyze() {
		Bukkit.getLogger().info("<kameRecipe> load recipe " + (System.currentTimeMillis() - time) / 1000.0f + "[s]");
		Bukkit.getLogger().info("<kameRecipe> Added the" + FINE + " recipe[s]");
		Bukkit.getLogger().log(FAIL > 0 ? Level.SEVERE : Level.INFO , "<kameRecipe> failed recipe is " + FAIL);
		Bukkit.getLogger().info("<kameRecipe> ===============================================");
		if(FAIL > 0)Main.addWarnig(ChatColor.RED + "[kames.] " + ChatColor.LIGHT_PURPLE + "追加に失敗したレシピが" + FAIL + "個あります");
	}

	public void parseFile(File file) throws IOException {
		LINE = WARN = ADDED = 0;
		filename = file.getName();
		String enc = getEncode(file);
		if(enc == null) throw new IOException("Unknown encode type!");
		try(BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), enc))) {
			Bukkit.getLogger().info("<kameRecipe> Loading to " + filename + "    EncodeType = " + enc);
			br.lines().forEach(line -> {
				if(LINE == 0 && line.length() > 0 && line.charAt(0) > 60000)line = line.substring(1);
				LINE++;
				if(line.matches("(CRAFT|COMBINE|SMELT|FUSION|SMASH|FUEL) *.*")) {
					buildLine();//initialize
					createLine(line);
				}else {
					parseLine(line);
				}
			});
			buildLine();
			if(WARN > 0)Main.addWarnig(ChatColor.RED + "[kames.] " + ChatColor.LIGHT_PURPLE + filename + " で一部正常でない部分が" + WARN + "個あります");
			Bukkit.getLogger().log(WARN > 0 ? Level.WARNING : Level.INFO , "<kameRecipe> Complete loaded " + filename + " " + ADDED + " Added " + WARN + " Warning");
		}
	}

	public void parseFuel(File file) throws IOException {
		LINE = WARN = ADDED = 0;
		filename = file.getName();
		String enc = getEncode(file);
		if(enc == null) throw new IOException("Unknown encode type!");
		try(BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), enc))) {
			Bukkit.getLogger().info("<kameRecipe> Loading to " + filename + "    EncodeType = " + enc);
			br.lines().forEach(line -> {
				if(LINE == 0 && line.length() > 0 && line.charAt(0) > 60000)line = line.substring(1);
				LINE++;
				if(line.matches("[0-9]+ = .*")) {
					buildLine();//initialize
					createLine("FUEL " + filename.subSequence(0, filename.length() - 4));
					parseLine(line);
				}else {
					parseLine(line);
				}
			});
			buildLine();
			if(WARN > 0)Main.addWarnig(ChatColor.RED + "[kames.] " + ChatColor.LIGHT_PURPLE + filename + " で一部正常でない部分が" + WARN + "個あります");
			Bukkit.getLogger().log(WARN > 0 ? Level.WARNING : Level.INFO , "<kameRecipe> Complete loaded " + filename + " " + ADDED + " Added " + WARN + " Warning");
		}
	}

	private String getEncode(File file) {
		Path path = Paths.get(file.getPath());
		for (String c : new String[]{"Shift_JIS", "UTF-8", "MS932", "EUC-JP"}) {
			try {
				Files.readAllLines(path, Charset.forName(c));
			}catch(IOException e) {
				continue;
			}
			return c;
		}
		return null;
	}

	private void createLine(String line) {
		String str[] = line.split(" ", 2);
		switch(str[0]) {
		case "CRAFT":
			buffer = new BufferShapedRecipe(	str.length > 1 ? str[1] : Lang.crafttable());
			break;
		case "COMBINE":
			buffer = new BufferShaplessRecipe(	str.length > 1 ? str[1] : Lang.crafttable());
			break;
		case "SMELT":
			buffer = new BufferFurnaceRecipe(	str.length > 1 ? str[1] : Lang.furnace());
			break;
		case "FUSION":
			buffer = new BufferFusionRecipe(	str.length > 1 ? str[1] : Lang.brewer());
			break;
		case "SMASH":
			buffer = new BufferSmashRecipe(		str.length > 1 ? str[1] : Lang.anvil());
			break;
		case "FUEL":
			buffer = new BufferFuelRecipe(		str.length > 1 ? str[1] : Lang.furnace());
			break;
		default:
		}
		READ = LINE;
	}

	private void parseLine(String line) {
		try {
			if(buffer != null && !buffer.isDrop()) {
				buffer.parseLine(line);
			}
		} catch (IllegalItemException e) {
			WARN++;
			switch(e.getType()) {
			case AUGMENTS:
				Bukkit.getLogger().severe("<kameRecipe> ERROR!! Line " + LINE + " AugmentError " + e.getError() + " too few arguments!! (" + filename + ")");
				break;
			case BADNBT:
				Bukkit.getLogger().severe("<kameRecipe> ERROR!! Line " + LINE + " NBTError " + e.getError() + " is BAD NBT tag!! (" + filename + ")");
				break;
			case ENCHANTMENTS:
				Bukkit.getLogger().severe("<kameRecipe> ERROR!! Line " + LINE + " EnchantmentNameError " + e.getError() + e.getMessages() +  " (" + filename + ")");
				break;
			case MATERIAL:
				Bukkit.getLogger().severe("<kameRecipe> ERROR!! Line " + LINE + " MaterialNameError " + e.getError() + " is unknown Material!! (" + filename + ")");
				break;
			case NUMBER:
				Bukkit.getLogger().severe("<kameRecipe> ERROR!! Line " + LINE + " NumberFormatError " + e.getError() + " is no Number!! (" + filename + ")");
				break;
			case WARN:
				Bukkit.getLogger().severe("<kameRecipe> WARNING!! Line " + LINE + " " + e.getMessages() +  " (" + filename + ")");
				return;
			case OTHER:
				Bukkit.getLogger().severe("<kameRecipe> ERROR!! Line " + LINE + " " + e.getMessages() +  " (" + filename + ")");
				break;
			default:
				break;
			}
			buffer.drop();
		}catch(Exception e) {
			Bukkit.getLogger().severe("<kameRecipe> ERROR!! Line " + LINE + " " + e.getMessage() +  " (" + filename + ")");
		}
	}

	private void buildLine() {
		if(buffer == null)return;
		String warn = buffer.finaly();
		if(warn.equals("")) {
			buffer.createRecipe();
			FINE++;
			ADDED++;
		}else {
			Bukkit.getLogger().severe("<kameRecipe> Line " + READ + " " + buffer.getName() + ":" + warn);
			FAIL++;
		}
		buffer = null;
	}
}
