package kame.kameRecipeManager.recipe;

import java.util.List;
import java.util.Set;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;

import kame.kameRecipeManager.recipe.option.CheckMode;
import kame.kameRecipeManager.recipe.option.CraftingEffect;
import kame.kameRecipeManager.recipe.option.CraftingSound;
import kame.kameRecipeManager.recipe.process.RecipeSifter;
import kame.kameRecipeManager.recipe.product.ProductRecipe;
import kame.kameRecipeManager.utils.Utils;

public class KFusionRecipe implements KRecipe {
	private final List<ItemStack> input;
	private final List<Material> materials;
	private final ItemStack result;
	private final List<String> option;
	private final int time;
	private final CraftingEffect effect;
	private final CraftingSound sound;
	private final Set<CheckMode> check;
	private final List<ProductRecipe> products;

	public KFusionRecipe(Recipe recipe, ItemStack result, List<ItemStack> items, int time, CraftingEffect effect, CraftingSound sound, List<String> option, List<ProductRecipe> products, Set<CheckMode> check) {
		this.materials = RecipeSifter.itemForMaterial(items);
		this.input = items;
		this.time = time;
		this.result = result;
		this.effect = effect;
		this.sound = sound;
		this.option = option;
		this.products = products;
		this.check = check;
	}

	public List<ItemStack> getInput() {
		return this.input;
	}

	public List<Material> getInputMaterial() {
		return this.materials;
	}

	public int getFusionTime() {
		return this.time;
	}

	public Recipe getBukkitRecipe() {
		return null;
	}

	public ItemStack getResult() {
		return this.result;
	}

	public ItemStack getResult(Player player, Location loc) {
		return Utils.replaceMeta(result, player, loc);
	}

	public CraftingEffect getEffect() {
		return this.effect;
	}

	public CraftingSound getSound() {
		return this.sound;
	}

	public List<String> getOption() {
		return this.option;
	}

	public Set<CheckMode> getCheckMode() {
		return this.check;
	}

	public List<ProductRecipe> getProducts() {
		return this.products;
	}
	
	public String toString() {
		return "KFusionRecipe: input=" + input + "\ntime=" + time + "\nresult=" + result;
	}

}
