package kame.kameRecipeManager.recipe;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.FurnaceRecipe;

import kame.kameRecipeManager.recipe.option.RecipeType;
import kame.kameRecipeManager.recipe.process.ItemProcessor;

public class KameRecipes {
	private static Map<String, ArrayList<KRecipe>> recipes = new HashMap<>();
	private static Set<FurnaceRecipe> removes = new HashSet<>();
	private static final ArrayList<KRecipe> empty = new ArrayList<KRecipe>();
	/*
	 * CRAFT:Table_name
	 * COMBINE:Table_name
	 * SMELT:Table_name
	 * FUSION:Table_name
	 * SMASH:Table_name
	 */

	@SuppressWarnings("unchecked")
	public static <T> T getRecipes(Collection<String> includes, RecipeType type) {
		ArrayList<KRecipe> list = new ArrayList<>();
		includes.forEach(recipe -> list.addAll(getRecipe(type.getName() + ":" + recipe)));
		return (T) list;
	}

	public static List<KRecipe> getRecipes(Collection<String> includes, RecipeType... types) {
		ArrayList<KRecipe> list = new ArrayList<>();
		for(RecipeType craft : types)includes.forEach(recipe -> list.addAll(getRecipe(craft.getName() + ":" + recipe)));
		return list;
	}

	public static List<KRecipe> getRecipe(String recipename) {
		return recipes.containsKey(recipename) ? recipes.get(recipename) : empty;
	}

	public static Map<String, ArrayList<KRecipe>> getAllRecipes() {
		return new TreeMap<String, ArrayList<KRecipe>>(recipes);
	}

	public static void addRemoveRecipe(FurnaceRecipe recipe) {
		removes.add(recipe);
	}

	public static void addRecipe(String recipename, KRecipe recipe) {
		if(recipe.getBukkitRecipe() != null && !recipe.getResult().getType().equals(Material.AIR))
			Bukkit.addRecipe(recipe.getBukkitRecipe());
		ArrayList<KRecipe> list = recipes.get(recipename);
		if (list == null)list = new ArrayList<KRecipe>();
		list.add(recipe);
		recipes.put(recipename, list);
	}

	public static void addDefaultRecipe(String recipename, KRecipe recipe) {
		if(recipe.getBukkitRecipe() instanceof FurnaceRecipe && isDelete((FurnaceRecipe) recipe.getBukkitRecipe())) {
			Logger.getLogger("kameRecipeManager").info(recipename + " " + recipe.getResult() + " を除外しました");
			return;
		}
		ArrayList<KRecipe> list = recipes.get(recipename);
		if (list == null)list = new ArrayList<KRecipe>();
		list.add(recipe);
		recipes.put(recipename, list);
	}

	private static boolean isDelete(FurnaceRecipe add) {
		return removes.stream().anyMatch(x -> x.getInput().getType() == add.getInput().getType() &&
				ItemProcessor.matchItem(x.getInput(), add.getInput(), null, null, null));
	}

	public static void initialize() {
		Bukkit.resetRecipes();
		recipes = new HashMap<>();
		removes = new HashSet<>();
	}
}
