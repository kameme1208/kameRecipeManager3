package kame.kameRecipeManager.recipe.process;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import kame.kameRecipeManager.Main;
import kame.kameRecipeManager.recipe.option.CheckMode;
import kame.kameRecipeManager.utils.Utils;

public class ItemProcessor {
	private ItemProcessor(){};

	public static boolean matchMaterialMap(Map<Character, Material> base, Map<Character, Material> input, String[] shape) {
		if(base.equals(input))return true;
		for(String buf : shape)sortMaterial(input, buf.charAt(0), buf.charAt(buf.length()-1));
		return base.equals(input);
	}

	public static boolean matchItem(Map<Character, ItemStack> base, Map<Character, ItemStack> input, String[] shape, Set<CheckMode> mode, Player player, Location loc) {
		if(base.equals(input))return true;
		if(base.size() != input.size())return false;
		boolean bool = true;
		for(char c = 'a'; c < 'j';c++) {
			ItemStack a = base.get(c);
			ItemStack b = input.get(c);
			if(a == null && b == null)continue;
			if(!matchItem(a, b, mode, player, loc))bool = false;
		}
		if(bool)return true;
		for(String buf : shape)sortItem(input, buf.charAt(0), buf.charAt(buf.length()-1));
		for(char c = 'a'; c < 'j';c++) {
			ItemStack a = base.get(c);
			ItemStack b = input.get(c);
			if(a == null && b == null)continue;
			if(!matchItem(a, b, mode, player, loc))return false;
		}
		return true;
	}

	private static void sortMaterial(Map<Character, Material> map, char l, char r) {
		Material a = map.remove(l);
		Material b = map.remove(r);
		if(b != null)map.put(l, b);
		if(a != null)map.put(r, a);
	}

	private static void sortItem(Map<Character, ItemStack> map, char l, char r) {
		ItemStack a = map.remove(l);
		ItemStack b = map.remove(r);
		if(b != null)map.put(l, b);
		if(a != null)map.put(r, a);
	}

	public static boolean matchItem(Collection<ItemStack> base, Collection<ItemStack> input, Set<CheckMode> mode, Player player, Location loc) {
		if(base.equals(input))return true;
		if(base.size() != input.size())return false;
		base = Utils.replaceMeta(base, player, loc);
		input = Utils.replaceMeta(input, player, loc);
		Collections.sort((ArrayList<ItemStack>)base, ItemSorts.getInstance());
		Collections.sort((ArrayList<ItemStack>)input, ItemSorts.getInstance());
		Iterator<ItemStack> ba = base.iterator();
		Iterator<ItemStack> in = input.iterator();
		while(ba.hasNext()) {
			if(!itemCheck(ba.next(), in.next(), mode, player, loc))return false;
		}
		return true;
	}

	public static boolean matchItem(ItemStack base, ItemStack input, Set<CheckMode> mode, Player player, Location loc) {
		return itemCheck(Utils.replaceMeta(base, player, loc), Utils.replaceMeta(input, player, loc), mode, player, loc);
	}

	@SuppressWarnings("deprecation")
	private static boolean itemCheck(ItemStack base, ItemStack input, Set<CheckMode> mode, Player player, Location loc) {
		if(base == null || input == null)return false;
		if(base.getDurability() != 32767 && base.getData().getData() != -1) {
			if(base.getDurability() != input.getDurability())return false;
		}
		if(Main.debug)Main.cast("input = " + input.getData() + " base = " + base.getData());
		ItemMeta imbase = base.getItemMeta();
		ItemMeta imitem = input.getItemMeta();
		if(mode == null)mode = new HashSet<>();
		if(mode.contains(CheckMode.uncheckname)) {
			imbase.setDisplayName(" ");
			imitem.setDisplayName(" ");
		}
		if(mode.contains(CheckMode.patternname)) {
			if(!imbase.hasDisplayName() || !imitem.hasDisplayName())return false;
			if(!imitem.getDisplayName().matches(imbase.getDisplayName()))return false;
		}
		if(mode.contains(CheckMode.unchecklore)) {
			imbase.setLore(new ArrayList<>(0));
			imitem.setLore(new ArrayList<>(0));
		}
		if(mode.contains(CheckMode.patternlore)) {
			int siz1 = imbase.hasLore() ? imbase.getLore().size() : 0;
			int siz2 = imitem.hasLore() ? imitem.getLore().size() : 0;
			List<String> lorebase = new ArrayList<>(Math.max(siz1, siz2));
			List<String> loreitem = new ArrayList<>(Math.max(siz1, siz2));
			lorebase.addAll(siz1 > 0 ? imbase.getLore() : new ArrayList<>(0));
			loreitem.addAll(siz2 > 0 ? imitem.getLore() : new ArrayList<>(0));
			while(lorebase.size() != Math.max(siz1, siz2))lorebase.add("");
			while(loreitem.size() != Math.max(siz1, siz2))loreitem.add("");
			Iterator<String> ba = lorebase.iterator();
			Iterator<String> in = loreitem.iterator();
			while(ba.hasNext())if(!in.next().matches(ba.next()))return false;
		}
		if(mode.contains(CheckMode.uncheckitemflags)) {
			imbase.removeItemFlags(ItemFlag.values());
			imitem.removeItemFlags(ItemFlag.values());
		}
		if(mode.contains(CheckMode.uncheckenchant)) {
			for(Enchantment ench : imbase.getEnchants().keySet())imbase.removeEnchant(ench);
			for(Enchantment ench : imitem.getEnchants().keySet())imitem.removeEnchant(ench);
		}
		if(mode.contains(CheckMode.uncheckother)) {
			String name = imitem.getDisplayName();
			Set<ItemFlag> flags = imitem.getItemFlags();
			List<String> lore = imitem.getLore();
			Map<Enchantment, Integer> enchs = imitem.getEnchants();
			imitem = imbase.clone();
			imitem.setDisplayName(name);
			imitem.setLore(lore);
			imitem.addItemFlags(flags.toArray(new ItemFlag[0]));
			for(Entry<Enchantment, Integer> ench : enchs.entrySet())imitem.addEnchant(ench.getKey(), ench.getValue(), true);
		}
		return imbase.serialize().equals(imitem.serialize());
	}
}
