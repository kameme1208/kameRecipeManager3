package kame.kameRecipeManager.recipe.process;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import kame.kameRecipeManager.Main;

public class RecipeSifter {

	public static Map<Character, ItemStack> startSift(ItemStack[] craft, int... i) {
		if(Main.debug)caster(new StringBuilder("-IN--\n"), craft, i);
		try {
			if(craft.length == 4)return startSiftMini(craft, i);
			if(craft.length == 9)return startSiftLong(craft, i);
		}catch(Exception e) {
			e.printStackTrace();
		}
		System.err.println("[kames.]RecipeError [" + craft.length + "]");
		return null;
	}

	private static Map<Character, ItemStack> startSiftMini(ItemStack[] craft, int... i) {
		if(i[1] < 2 && isItemLine(craft, 2, 3))craft = siftUpMini(craft);
		if(i[0] < 2 && isItemLine(craft, 1, 3))craft = siftLeftMini(craft);
		return sortSift(craft, i);
	}

	private static Map<Character, ItemStack> startSiftLong(ItemStack[] craft, int... i) {
		if(i[1] < 3 && isItemLine(craft, 6, 7, 8))craft = siftUp(craft);
		if(i[1] < 2 && isItemLine(craft, 3, 4, 5))craft = siftUp(craft);
		if(i[0] < 3 && isItemLine(craft, 2, 5, 8))craft = siftLeft(craft);
		if(i[0] < 2 && isItemLine(craft, 1, 4, 7))craft = siftLeft(craft);
		return sortSift(craft, i);
	}

	private static ItemStack[] siftUp(ItemStack[] craft){
		ItemStack a = craft[0];
		for(int i = 0; i < 6; craft[i] = craft[i+++3]);
		craft[8] = craft[7] = craft[6] = a;
		return craft;
	}

	private static boolean isItemLine(ItemStack[] craft, int a, int b) {
		return craft[a].getAmount() + craft[b].getAmount() != 0;
	}

	private static boolean isItemLine(ItemStack[] craft, int a, int b, int c) {
		return craft[a].getAmount() + craft[b].getAmount() + craft[c].getAmount() != 0;
	}

	private static ItemStack[] siftLeft(ItemStack[] craft) {
		ItemStack a = craft[0];
		for(int i = 0; i < 8;i=i%3 == 0 ? i++ :i,craft[i] = craft[++i]);
		craft[2] = craft[5] = craft[8] = a;
		return craft;
	 }

	private static ItemStack[] siftUpMini(ItemStack[] craft) {
		ItemStack a = craft[0];
		craft[0] = craft[2];
		craft[1] = craft[3];
		craft[3] = craft[2] = a;
		return craft;
	}

	private static ItemStack[] siftLeftMini(ItemStack[] craft) {
		ItemStack a = craft[0];
		craft[0] = craft[1];
		craft[2] = craft[3];
		craft[3] = craft[1] = a;
		return craft;
	}

	public static List<ItemStack> sortSift(ItemStack[] item) {
		List<ItemStack> items = new ArrayList<>();
		for(ItemStack sort : item) {
			if (sort != null && sort.getAmount() != 0) {
				sort = sort.clone();
				sort.setAmount(1);
				items.add(sort);
			}
		}
		
		Collections.sort(items, ItemSorts.getInstance());
		Main.cast("List§b"+items);
		return items;
	}

	private static Map<Character, ItemStack> sortSift(ItemStack[] craft, int[] i) {
		if(Main.debug)caster(new StringBuilder("-OUT-\n"), craft, i);
		Map<Character, ItemStack> map = new HashMap<>(craft.length);
		char ch = 'a';
		for (int c = 0; c < craft.length; c++)
			if(c%Math.sqrt(craft.length) < i[0] && c/Math.sqrt(craft.length) < i[1]) {
			craft[c] = craft[c].clone();
			if(craft[c].getAmount() > 0) {
				craft[c].setAmount(1);
				map.put(ch, craft[c]);
			}
			ch++;
		}
		return map;
	}

	public static Map<Character, Material> itemForMaterial(Map<Character, ItemStack> craft) {
		craft.entrySet().removeIf(x -> x.getValue() == null);
		Map<Character, Material> map = new HashMap<>(craft.size());

		for(Entry<Character, ItemStack> entry : craft.entrySet())
			if(entry.getValue().getAmount() > 0)map.put(entry.getKey(), entry.getValue().getType());
		return map;
	}

	public static List<Material> itemForMaterial(List<ItemStack> list) {
		Collections.sort(list, ItemSorts.getInstance());
		List<Material> mates = new ArrayList<>();
		for(ItemStack item : list)mates.add(item.getType());
		return mates;
	}

	private static void caster(StringBuilder b, ItemStack[] craft, int[] i) {
		int g = 0;
		for(ItemStack s : craft) {
			if(s.getAmount() == 0)b.append("§cx §r");
			else b.append("§ao §r");
			if(++g % Math.sqrt(craft.length) == 0)b.append("\n");
		}
		Bukkit.broadcastMessage(b.append("\n§bsize :").append(i[0]).append(" ").append(i[1]).toString());
	}
}
