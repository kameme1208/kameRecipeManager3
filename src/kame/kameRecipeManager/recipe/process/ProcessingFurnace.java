package kame.kameRecipeManager.recipe.process;

import java.lang.reflect.Method;
import java.util.Set;

import org.bukkit.block.Furnace;
import org.bukkit.inventory.FurnaceInventory;
import org.bukkit.inventory.ItemStack;

import kame.kameRecipeManager.recipe.KFuelRecipe;
import kame.kameRecipeManager.recipe.KFurnaceRecipe;
import kame.kameRecipeManager.recipe.product.ProductReplace;
import kame.kameRecipeManager.utils.StandUtils;
import kame.kameRecipeManager.utils.Utils;

public class ProcessingFurnace {
	private KFurnaceRecipe recipe;
	private Furnace block;
	private ItemStack old;
	private Set<String> include;
	private float cooking = 0;
	private float cookrate = 1;

	public ProcessingFurnace(Furnace block, KFurnaceRecipe recipe, KFuelRecipe fuel, Set<String> include) {
		this.block = block;
		this.old = block.getInventory().getSmelting();
		this.recipe = recipe;
		this.include = include;
		this.cookrate = StandUtils.getCookRate(block.getBlock()) * (fuel != null ? fuel.getCookRate() : 1);
	}

	public int nextCook() {
		old = block.getInventory().getSmelting();
		cooking += cookrate;
		short cook = (short)(cooking / recipe.getCookTime() * 200);
		block.setCookTime((short) (cook == 199 ? 198 : cook));
		return cook;
	}

	public ItemStack getSmelting() {
		return block.getInventory().getSmelting();
	}

	public ItemStack getInput() {
		return recipe.getInput();
	}

	public ItemStack getOutput() {
		return block.getInventory().getResult();
	}

	public ItemStack getResult() {
		return recipe.getResult();
	}

	public ItemStack getOldSmelting() {
		return old;
	}

	public Furnace getBlock() {
		return block;
	}

	public FurnaceInventory getInventory() {
		return block.getInventory();
	}

	public void updateRecipe(KFurnaceRecipe recipe) {
		old = block.getInventory().getSmelting();
		this.recipe = recipe;
	}

	public boolean isBurning() {
		return block.getBurnTime() > 0;
	}

	public void setBurnTime(int burntime) {
		block.setBurnTime((short) burntime);
		setDisplayTick(burntime);
	}

	public void setCookTime(int i) {
		block.setCookTime((short) (i == 199 ? 198 : i));
		cooking = i;
	}

	public void setCookRate(KFuelRecipe recipe) {
		this.cookrate = StandUtils.getCookRate(block.getBlock()) * recipe.getCookRate();
	}

	public Set<String> getInclude() {
		return include;
	}

	public KFurnaceRecipe getRecipe() {
		return recipe;
	}

	public int getcook() {
		return recipe.getCookTime();
	}

	public int getCookPer() {
		return recipe != null ? (int) (cooking / recipe.getCookTime() * 100f) : 0;
	}

	public boolean isFull() {
		return getOutput() != null && getOutput().getAmount() + getResult().getAmount() > getResult().getMaxStackSize();
	}

	public boolean isProductReady() {
		return !recipe.getProducts().stream().noneMatch(x -> x instanceof ProductReplace) && getOutput() != null;
	}

	public boolean isOutReady() {
		return getOutput() == null || Utils.equals(getOutput(), getResult());
	}

	public boolean hasProductReplace() {
		return recipe != null ? !recipe.getProducts().stream().noneMatch(x -> x instanceof ProductReplace) : false;
	}

	private void setDisplayTick(int ticks) {
		if(ticks == 0)return;
		try {
			Method getTileEntity = block.getClass().getMethod("getTileEntity");
			Object tileEntityFurnace = getTileEntity.invoke(block);
			Method setPropaty = tileEntityFurnace.getClass().getMethod("setProperty", int.class, int.class);
			setPropaty.invoke(tileEntityFurnace, 1, ticks);
		} catch (ReflectiveOperationException e) {
			e.printStackTrace();
		}
	}

	public String toString() {
		return "ActiveFurnace:" + block.getLocation() + recipe + cooking + " " + block.getBurnTime();
	}
}
