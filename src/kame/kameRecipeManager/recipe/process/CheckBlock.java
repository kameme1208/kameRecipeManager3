package kame.kameRecipeManager.recipe.process;

import java.util.HashSet;
import java.util.Set;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;

public class CheckBlock {
	private Set<Material> blockmaterial = new HashSet<>();
	private Set<Material> Blockmaterial = new HashSet<>();
	private int x;
	private int y;
	private int z;

	public CheckBlock(int x, int y, int z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public CheckBlock(BlockFace face) {
		this.x = face.getModX();
		this.y = face.getModY();
		this.z = face.getModZ();
	}

	public void addBudMaterial(Material material) {
		Blockmaterial.add(material);
	}

	public void addMaterial(Material material) {
		blockmaterial.add(material);
	}

	public boolean containtsMaterial(Block block, BlockFace face) {
		Material type = null;
		switch(face) {
		case NORTH:
			type = block.getRelative(x, y, z).getType();
			break;
		case EAST:
			type = block.getRelative(z, y, x).getType();
			break;
		case SOUTH:
			type = block.getRelative(-x, y, -z).getType();
			break;
		case WEST:
			type = block.getRelative(-z, y, -x).getType();
			break;
		default:
			return false;
		}
		return (blockmaterial.isEmpty() || blockmaterial.contains(type)) && !Blockmaterial.contains(type);
	}

}
