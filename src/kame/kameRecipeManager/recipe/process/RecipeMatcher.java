package kame.kameRecipeManager.recipe.process;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.inventory.CraftingInventory;
import org.bukkit.inventory.FurnaceInventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.Recipe;
import org.bukkit.inventory.ShapedRecipe;

import kame.kameRecipeManager.Main;
import kame.kameRecipeManager.listener.CraftListener;
import kame.kameRecipeManager.recipe.KFuelRecipe;
import kame.kameRecipeManager.recipe.KFurnaceRecipe;
import kame.kameRecipeManager.recipe.KFusionRecipe;
import kame.kameRecipeManager.recipe.KShapedRecipe;
import kame.kameRecipeManager.recipe.KShapelessRecipe;
import kame.kameRecipeManager.recipe.KSmashRecipe;
import kame.kameRecipeManager.recipe.KameRecipes;
import kame.kameRecipeManager.recipe.VanillaRecipe;
import kame.kameRecipeManager.recipe.option.RecipeType;
import kame.kameRecipeManager.recipe.product.ProductRecipe;
import kame.kameRecipeManager.recipe.product.ProductReplace;
import kame.kameRecipeManager.utils.InventoryPriority;
import kame.kameRecipeManager.utils.Utils;


public abstract class RecipeMatcher {

	private RecipeMatcher() {}

	public static ItemStack prepareCraftingRecipe(CraftingInventory inv, Recipe recipe, Location loc, Player player, Set<String> includes) {
		RecipeOption.clear(player);
		if(recipe instanceof ShapedRecipe) {
			Main.cast("§d[鯖たん] §e定型レシピにゃん♪(ฅ•ω•ฅ)");
			ItemStack item = prepareShapedRecipe(inv, ((ShapedRecipe) recipe).getShape(), loc, player, includes);
			if(item != null) {
				return Utils.replaceMeta(item, player, loc);
			}else {
				Main.cast("§d[鯖たん] §eんー…不定形も見てみるにゃん♪(ฅ•ω•ฅ)");
				item = prepareShapelessRecipe(inv, loc, player, includes);
				if(item == null)Main.cast("§d[鯖たん] §eレシピが一致しなかったにゃん…♪(ฅ;ω;ฅ)");
				return Utils.replaceMeta(item, player, loc);
			}
		}else {
			Main.cast("§d[鯖たん] §e不定型レシピにゃん♪(ฅ•ω•ฅ)");
			ItemStack item = prepareShapelessRecipe(inv, loc, player, includes);
			if(item == null)Main.cast("§d[鯖たん] §eレシピが一致しなかったにゃん…♪(ฅ;ω;ฅ)");
			return Utils.replaceMeta(item, player, loc);
		}
	}

	private static ItemStack prepareShapedRecipe(CraftingInventory inv, String[] shape, Location loc, Player player, Set<String> includes) {
		Map<Character, ItemStack> map = RecipeSifter.startSift(inv.getMatrix(), shape[0].length(), shape.length);
		Map<Character, Material> mates = RecipeSifter.itemForMaterial(map);
		Main.cast("§b"+mates);
		List<KShapedRecipe> recipes = KameRecipes.getRecipes(includes, RecipeType.Shaped);
		recipes.removeIf(x -> !Arrays.equals(x.getShape(), shape) || !ItemProcessor.matchMaterialMap(x.getMaterialMap(), mates, shape));
		if(recipes.size() > 0)Main.cast("§d[鯖たん] §eにゅっ！一致の予感にゃん！！(ฅoωoฅ)");
		for(KShapedRecipe recipe : recipes) {
			if(ItemProcessor.matchItem(recipe.getIngredientMap(), map, shape, recipe.getCheckMode(), player, loc)) {
				if(Main.debug)Main.cast("§d[鯖たん] §eレシピが一致したにゃん♪(ฅ•ω•ฅ)\n" + recipe.getResult());
				RecipeOption.buildCraft(player, recipe, loc);
			}else {
				Main.cast("§d[鯖たん] §e違うかったにゃん♪(ฅ•ω•ฅ)");
			}
		}
		return RecipeOption.prepareCraft(player);

	}

	private static ItemStack prepareShapelessRecipe(CraftingInventory inv, Location loc, Player player, Set<String> includes) {
		List<ItemStack> list = RecipeSifter.sortSift(inv.getMatrix());
		List<Material> mates = RecipeSifter.itemForMaterial(list);
		List<KShapelessRecipe> recipes = KameRecipes.getRecipes(includes, RecipeType.Shapeless);
		recipes.removeIf(x -> !x.getMaterialList().equals(mates));
		if(recipes.size() > 0)Main.cast("§d[鯖たん] §eにゅっ！一致の予感にゃん！！(ฅoωoฅ)");
		for(KShapelessRecipe recipe : recipes) {
			if(ItemProcessor.matchItem(recipe.getIngredientList(), list, recipe.getCheckMode(), player, loc)) {
				if(Main.debug)Main.cast("§d[鯖たん] §eレシピが一致したにゃん♪(ฅ•ω•ฅ)\n" + recipe.getResult());
				RecipeOption.buildCraft(player, recipe, loc);
			}else {
				Main.cast("§d[鯖たん] §e違うかったにゃん♪(ฅ•ω•ฅ)");
			}
		}
		return RecipeOption.prepareCraft(player);
	}

	public static void resultCraftingRecipe(CraftItemEvent event, Player player) {
		RecipeOption recipe = RecipeOption.crafting(player);
		if(Main.debug)Main.cast(event.getClick());
		if(recipe == null) {
			event.setCancelled(true);
			return;
		}
		if(Main.debug)Main.cast(recipe.getRecipe());
		if(recipe.getRecipe() instanceof VanillaRecipe) {
			Main.cast("§d[鯖たん] §eバニラレシピにゃん♪(ฅ•ω•ฅ)");
			if(event.getClick() == ClickType.RIGHT) {
				ItemStack recipeitem = recipe.getRecipe().getResult();

				int min = 0;
				if(!Utils.equals(recipeitem, player.getItemOnCursor())) {
					min = 64;
				}else {
					min = (recipeitem.getMaxStackSize() - player.getItemOnCursor().getAmount()) / recipeitem.getAmount();
					for(ItemStack item : event.getInventory().getMatrix()) {
						if(item == null || item.getAmount() == 0)continue;
						min = Math.min(min, item.getAmount());
					}
				}
				for(int i = 1; i < min; i++)CraftListener.vannila.add(player.getUniqueId());
			}
			return;
		}
		switch(event.getClick()) {
		case DROP:
			onDrop(event, player, recipe);
			event.setCancelled(true);
			return;
		case LEFT:
			onLeftClick(event, player, recipe);
			event.setCancelled(true);
			return;
		case NUMBER_KEY:
			onNumberClick(event, player, recipe);
			event.setCancelled(true);
			return;
		case RIGHT:
			onRightClick(event, player, recipe);
			event.setCancelled(true);
			return;
		case SHIFT_LEFT:
			onShiftClick(event, player, recipe);
			event.setCancelled(true);
			return;
		case MIDDLE:
			if(player.getGameMode() != GameMode.CREATIVE)return;
			ItemStack result = recipe.getRecipe().getResult();
			result.setAmount(64);
			player.setItemOnCursor(result);
			return;
		default:
			return;
		}
	}

	private static boolean onProduct(CraftItemEvent event, Player player, RecipeOption recipe, boolean droped, int loop) {
		CraftingInventory inv = event.getInventory();
		List<ProductRecipe> products = recipe.getRecipe().getProducts();
		boolean bool = false;
		for(ProductRecipe product : products) {
			if(product instanceof ProductReplace) {
				if(!droped && player.getItemOnCursor().getAmount() != 0)return true;
				bool = true;
				subItems(inv, player, loop);
				for(int i = loop; i != 0;i--) {
					ItemStack item = Utils.replaceMeta(product.buildItem(), player, recipe.getLocation());
					if(item == null)continue;
					if(droped) {
						Item drop = player.getWorld().dropItem(player.getEyeLocation(), item);
						drop.setPickupDelay(0);
					}else {
						player.setItemOnCursor(item);
					}
				}
			}else {
				for(int i = loop; i != 0;i--) {
					ItemStack item = Utils.replaceMeta(product.buildItem(), player, recipe.getLocation());
					if(item == null)continue;
					Item drop = player.getWorld().dropItem(player.getEyeLocation(), item);
					drop.setPickupDelay(0);
				}
			}
		}
		return bool;
	}

	private static void onLeftClick(CraftItemEvent event, Player player, RecipeOption recipe) {
		CraftingInventory inv = event.getInventory();
		ItemStack cursor = player.getItemOnCursor();
		ItemStack result = Utils.replaceMeta(recipe.getRecipe().getResult(), player, recipe.getLocation());
		if(cursor.getAmount() != 0 && !Utils.equals(cursor, result))return;
		if(result.getAmount() + cursor.getAmount() > result.getMaxStackSize())return;
		RecipeOption.resultCraft(player, recipe);
		if(!onProduct(event, player, recipe, false, 1)) {
			subItems(inv, player, 1);
			result.setAmount(cursor.getAmount() + result.getAmount());
			player.setItemOnCursor(result);
		}
	}

	private static void onDrop(CraftItemEvent event, Player player, RecipeOption recipe) {
		ItemStack result = Utils.replaceMeta(recipe.getRecipe().getResult(), player, recipe.getLocation());
		RecipeOption.resultCraft(player, recipe);
		if(!onProduct(event, player, recipe, true, 1)) {
			subItems(event.getInventory(), player, 1);
			player.getWorld().dropItem(player.getEyeLocation(), result).setVelocity(player.getLocation().getDirection());
		}
	}

	private static void onNumberClick(CraftItemEvent event, Player player, RecipeOption recipe) {
		int slot = event.getHotbarButton();
		PlayerInventory inv = player.getInventory();
		ItemStack hotbar =inv.getItem(slot);
		ItemStack result = Utils.replaceMeta(recipe.getRecipe().getResult(), player, recipe.getLocation());
		if(hotbar != null && !Utils.equals(hotbar, result))return;
		int amount = hotbar == null ? 0 : hotbar.getAmount();
		if(result.getAmount() + amount > result.getMaxStackSize())return;
		RecipeOption.resultCraft(player, recipe);
		if(!onProduct(event, player, recipe, true, 1)) {
			subItems(event.getInventory(), player, 1);
			result.setAmount(amount + result.getAmount());
			inv.setItem(slot, result);
		}
	}

	private static void onRightClick(CraftItemEvent event, Player player, RecipeOption recipe) {
		CraftingInventory inv = event.getInventory();
		ItemStack cursor = player.getItemOnCursor();
		ItemStack result = Utils.replaceMeta(recipe.getRecipe().getResult(), player, recipe.getLocation());
		ItemStack matrix[] = inv.getMatrix();
		if(cursor.getAmount() != 0 && !Utils.equals(cursor, result))return;
		int min = result.getMaxStackSize() - cursor.getAmount();
		min = min / result.getAmount();
		if(min > 1 && RecipeOption.isOneByOne(player))min = 1;
		if(min > 0)RecipeOption.resultCraft(player, recipe);
		for(ItemStack item : matrix)if(item.getAmount() > 0)min = Math.min(min, item.getAmount());
		if(!onProduct(event, player, recipe, true, min)) {
			subItems(inv, player, min);
			result.setAmount(cursor.getAmount() + result.getAmount() * min);
			player.setItemOnCursor(result);
		}
	}

	private static void onShiftClick(CraftItemEvent event, Player player, RecipeOption recipe) {
		CraftingInventory inv = event.getInventory();
		ItemStack result = Utils.replaceMeta(recipe.getRecipe().getResult(), player, recipe.getLocation());
		ItemStack matrix[] = inv.getMatrix();
		int min = InventoryPriority.freeCapacity(player.getInventory(), result) / result.getAmount();
		if(min > 1 && RecipeOption.isOneByOne(player))min = 1;
		if(min > 0)RecipeOption.resultCraft(player, recipe);
		for(ItemStack item : matrix)if(item.getAmount() > 0)min = Math.min(min, item.getAmount());
		if(!onProduct(event, player, recipe, true, min)) {
			subItems(inv, player, min);
			result.setAmount(result.getAmount() * min);
			if(Main.debug)Main.cast("アイテムを " + min +" 回クラフトし " + result.getAmount() + " 個の§b" + result.getType() + "§rにしたにゃん♪(ฅ•ω•ฅ)");
			InventoryPriority.addInventory(player.getInventory(), result);
		}
	}

	private static void subItems(CraftingInventory inv, Player player, int subamount) {
		int c = 0;
		ItemStack matrix[] = inv.getMatrix().clone();
		CraftListener.setEventSkip(player.getUniqueId(), true);
		for(ItemStack item : matrix) {
			if(item == null) continue;
			int amount = item.getAmount() - subamount;
			if(amount <= 0)item = null;
			else item.setAmount(amount);
			if(++c == matrix.length)CraftListener.setEventSkip(player.getUniqueId(), false);
			inv.setItem(c, item);
		}
	}

	public static KFurnaceRecipe getFurnaceRecipe(FurnaceInventory inv, Set<String> includes) {
		RecipeOption.clear(inv.getLocation());
		ItemStack input = inv.getSmelting();
		if(input == null)return null;
		List<KFurnaceRecipe> recipes = KameRecipes.getRecipes(includes, RecipeType.Furnace);
		Main.cast("§d[鯖たん] §eかまどレシピにゃん♪(ฅ•ω•ฅ)" + includes + recipes.size());
		for(KFurnaceRecipe recipe : recipes) {
			ItemStack base = recipe.getInput();
			if(base.getType() == input.getType() && ItemProcessor.matchItem(base, input, recipe.getCheckMode(), null, inv.getLocation())) {
				Main.cast("§d[鯖たん] §eレシピが一致したにゃん♪(ฅ•ω•ฅ)");
				RecipeOption.build(null, recipe, inv.getLocation());
			}
		}
		return RecipeOption.prepare(null, inv.getLocation());
	}

	public static KFuelRecipe getFuelRecipe(FurnaceInventory inv, Set<String> includes) {
		RecipeOption.clear(inv.getLocation());
		ItemStack input = inv.getFuel();
		if(input == null)return null;
		Main.cast("§d[鯖たん] §e燃料レシピにゃん♪(ฅ•ω•ฅ)");
		List<KFuelRecipe> recipes = KameRecipes.getRecipes(includes, RecipeType.Fuel);
		for(KFuelRecipe recipe : recipes) {
			ItemStack base = recipe.getResult();
			if(base.getType() == input.getType() && ItemProcessor.matchItem(base, input, recipe.getCheckMode(), null, inv.getLocation())) {
				Main.cast("§d[鯖たん] §e燃料が一致したにゃん♪(ฅ•ω•ฅ)");
				RecipeOption.build(null, recipe, inv.getLocation());
			}
		}
		return RecipeOption.prepare(null, inv.getLocation());
	}

	public static KFusionRecipe getFusionRecipe(List<ItemStack> items, Location loc, Player player, Set<String> includes) {
		RecipeOption.clear(loc);
		List<Material> mates = RecipeSifter.itemForMaterial(items);
		List<KFusionRecipe> recipes = KameRecipes.getRecipes(includes, RecipeType.Fusion);
		recipes.removeIf(x -> !x.getInputMaterial().equals(mates));
		if(recipes.size() > 0)Main.cast("§d[鯖たん] §eにゅっ！一致の予感にゃん！！(ฅoωoฅ)");
		for(KFusionRecipe recipe : recipes) {
			if(ItemProcessor.matchItem(recipe.getInput(), items, recipe.getCheckMode(), player, loc)) {
				if(Main.debug)Main.cast("§d[鯖たん] §eレシピが一致したにゃん♪(ฅ•ω•ฅ)\n" + recipe.getResult());
				RecipeOption.build(player, recipe, loc);
			}
		}
		return RecipeOption.prepare(player, loc);
	}

	public static KSmashRecipe getSmashRecipe(List<ItemStack> items, ItemStack smash, String spell, Location loc, Player player, Set<String> includes) {
		RecipeOption.clear(loc);
		List<Material> mates = RecipeSifter.itemForMaterial(items);
		List<KSmashRecipe> recipes = KameRecipes.getRecipes(includes, RecipeType.Smash);
		recipes.removeIf(x -> !x.getInputMaterial().equals(mates));
		if(recipes.size() > 0)Main.cast("§d[鯖たん] §eにゅっ！一致の予感にゃん！！(ฅoωoฅ)");
		for(KSmashRecipe recipe : recipes) {
			if(Main.debug)Main.cast(spell + " " + recipe.getSpell());
			if(spell.matches(recipe.getSpell()) && (!recipe.isRequestItem() ||
					ItemProcessor.matchItem(recipe.getSmashItem(), smash, recipe.getCheckMode(), player, loc)) &&
					ItemProcessor.matchItem(recipe.getInput(), items, recipe.getCheckMode(), player, loc)) {
				if(Main.debug)Main.cast("§d[鯖たん] §eレシピが一致したにゃん♪(ฅ•ω•ฅ)\n" + recipe.getResult());
				RecipeOption.build(player, recipe, loc);
			}
		}
		return RecipeOption.prepare(player, loc);

	}

}
