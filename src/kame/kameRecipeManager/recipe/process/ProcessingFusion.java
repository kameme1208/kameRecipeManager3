package kame.kameRecipeManager.recipe.process;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import com.google.common.collect.Lists;

import kame.kameRecipeManager.Main;
import kame.kameRecipeManager.recipe.KFusionRecipe;
import kame.kameRecipeManager.recipe.KRecipe;
import kame.kameRecipeManager.recipe.option.CraftingEffect;
import kame.kameRecipeManager.recipe.option.CraftingSound;
import kame.kameRecipeManager.recipe.product.ProductRecipe;
import kame.kameRecipeManager.recipe.product.ProductReplace;
import kame.kameRecipeManager.utils.StandUtils;


public class ProcessingFusion extends BukkitRunnable {
	private static List<Block> fusion = Lists.newArrayList();
	private Player player;
	private World world;
	private Location loc;
	private Location droploc;
	private Block block;
	private ArmorStand stand;
	private List<ItemStack> items;
	private List<Item> drops = new ArrayList<>();
	private List<ItemStack> fusions = new ArrayList<>();
	int deg = 0;
	public ProcessingFusion(Player player, Block block, ArmorStand stand, List<ItemStack> items) {
		fusion.add(block);
		this.player = player;
		this.items = items;
		this.stand = stand;
		this.block = block;
		this.loc = block.getLocation().add(0.5, 0.5, 0.5);
		this.droploc = loc.add(0, 0.375, 0);
		this.world = block.getWorld();
	}

	public static boolean isBrewing(Block block) {
		return fusion.contains(block);
	}

	@Override
	public void run() {
		if(items.size() > 0 && deg < 3) {
			ItemStack base = items.remove(0);
			ItemStack input = base.clone();
			base.setAmount(base.getAmount()-1);
			input.setAmount(1);
			fusions.add(input.clone());
			ItemMeta im = input.getItemMeta();
			String name = "";
			if(im.hasDisplayName()) name = im.getDisplayName();
			im.setDisplayName(":kames." + base.hashCode() + ":" + name);
			input.setItemMeta(im);
			Location loc = block.getLocation().add(0.5, 0.15, 0.5);
			switch(deg++) {
			case 0:loc.add(0.25, 0, 0);
				break;
			case 1:loc.add(-0.18, 0, 0.25);
				break;
			case 2:loc.add(-0.18, 0, -0.25);
				break;
			}
			Item i = world.dropItem(loc, input);
			i.setPickupDelay(Integer.MAX_VALUE);
			i.setVelocity(new Vector(0, 0, 0));
			drops.add(i);
			loc.getWorld().spawnParticle(Particle.FLAME, loc,5, 0, 0, 0, 0.01);
			loc.getWorld().spawnParticle(Particle.CLOUD, loc,1, 0, 0, 0, 1);
			world.playSound(loc, Sound.ENTITY_ITEM_PICKUP, 1, 0.5f);
		}else {
			this.cancel();
			Main.cast(fusions);
			ArmorStand stand = StandUtils.getCraftStand(block);
			Set<String> includes = StandUtils.getIncludeName(stand);
			if(includes.size() == 0)includes.add("調合台");
			KRecipe recipe = RecipeMatcher.getFusionRecipe(fusions, block.getLocation(), player, includes);
			if(recipe != null)process((KFusionRecipe) recipe);
			else failFusion();
		}
	}

	private void process(KFusionRecipe results) {;
		world.playSound(loc, Sound.ENTITY_PLAYER_LEVELUP, 1, 2);
		new BukkitRunnable() {
			int time = 0;
			@Override
			public void run() {
				if(stand.isDead()) {
					fusion.remove(block);
					this.cancel();
					for(Item item : drops)item.remove();
					for(ItemStack items : fusions)world.dropItem(loc, items);
					return;
				}
				if(time == (int)(results.getFusionTime()*0.8)) {
					loc.getWorld().spawnParticle(Particle.CRIT_MAGIC, loc, 50, 0.2, 0.2, 0.2, 1);
					world.playSound(loc, Sound.ITEM_FLINTANDSTEEL_USE, 1, 0.7f);
				}
				if(time > results.getFusionTime()) {
					ItemStack result = results.getResult(player, loc);
					for(Item item : drops)item.remove();
					for(ProductRecipe product : results.getProducts()) {
						if(product instanceof ProductReplace) {
							result = product.buildItem();
							Main.cast(result);
						}else {
							ItemStack item = product.buildItem();
							if(item != null)drop(item);
						}
					}
					drop(result);
					fusion.remove(block);
					this.cancel();
				}else {
				if(time%20 == 0)world.playSound(loc, Sound.BLOCK_WATER_AMBIENT, 1, (float) (Math.random()+0.5));
					loc.getWorld().spawnParticle(Particle.ENCHANTMENT_TABLE, loc, 1, 0, 0, 0, 1);
					for(Item item : drops) {
						item.setTicksLived(1);
					}
					time++;
				}
			}
			private void drop(ItemStack item) {
				if(item != null)world.dropItem(droploc, item).setVelocity(new Vector(0, 0, 0));
				CraftingEffect effect = results.getEffect();
				if(!effect.isEnpty())effect.playEffect(null, droploc);
				loc.getWorld().spawnParticle(Particle.CLOUD, loc, 10, 0.1, 0.1, 0.1, 0);
				CraftingSound sound = results.getSound();
				if(!sound.isEnpty())sound.playSound(null, droploc);
				else world.playSound(droploc, Sound.BLOCK_REDSTONE_TORCH_BURNOUT, 1, 0.7f);
			}
		}.runTaskTimer(Main.getInstance(), 0, 1);

	}

	private void failFusion() {
		loc.getWorld().spawnParticle(Particle.REDSTONE, loc, 10, 0.1, 0.1, 0.1, 0);
		world.playSound(droploc, Sound.BLOCK_LAVA_POP, 1, 0.5f);
		new BukkitRunnable() {
			@Override
			public void run() {
				fusion.remove(block);
				world.playSound(stand.getLocation(), Sound.BLOCK_REDSTONE_TORCH_BURNOUT, 1, 0.5f);
				for(Item item : drops)item.remove();
				for(ItemStack items : fusions) {
					Item item = world.dropItem(droploc, items);
					item.setVelocity(new Vector(0, 0, 0));
					loc.getWorld().spawnParticle(Particle.CLOUD, loc, 10, 0, 0, 0, 0);
				}

			}

		}.runTaskLater(Main.getInstance(), 20);

	}
}
