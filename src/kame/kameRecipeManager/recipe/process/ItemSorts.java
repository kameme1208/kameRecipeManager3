package kame.kameRecipeManager.recipe.process;

import java.util.Comparator;

import org.bukkit.inventory.ItemStack;

/**
 * 使用方法 ItemSorts.getInstance();
 * アイテムのソートに使用するクラス
 */
public class ItemSorts implements Comparator<ItemStack> {
	private static ItemSorts instance;

	private ItemSorts() {
		instance = this;
	}

	public static ItemSorts getInstance() {
		return instance != null ? instance : new ItemSorts();
	}

	public int compare(ItemStack item1, ItemStack item2) {
		return item1.serialize().toString().compareTo(item2.serialize().toString());
	}
}
