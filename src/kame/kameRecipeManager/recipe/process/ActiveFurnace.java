package kame.kameRecipeManager.recipe.process;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Furnace;
import org.bukkit.entity.Item;
import org.bukkit.inventory.FurnaceInventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import kame.kameRecipeManager.Main;
import kame.kameRecipeManager.recipe.KFuelRecipe;
import kame.kameRecipeManager.recipe.KFurnaceRecipe;
import kame.kameRecipeManager.recipe.product.ProductRecipe;
import kame.kameRecipeManager.recipe.product.ProductReplace;
import kame.kameRecipeManager.utils.StandUtils;
import kame.kameRecipeManager.utils.Utils;

public class ActiveFurnace extends BukkitRunnable {

	private static ActiveFurnace instance;
	private static Set<ProcessingFurnace> activeList = new HashSet<>();
	public static Map<Location, Float> exp = new HashMap<>();

	@Override
	public void run() {
		activeList.removeIf(entry -> processing(entry));
		activeList.removeIf(this::canNotBurning);
	}

	public static ActiveFurnace getInstance() {
		if(instance == null)instance = new ActiveFurnace();
		return instance;
	}

	public static List<Block> getBurningFurnaces() {
		List<Block> list = new ArrayList<>();
		for(ProcessingFurnace furnace : activeList) {
			list.add(furnace.getBlock().getBlock());
		}
		return list;
	}

	public static Set<Chunk> getFurnaceChunk() {
		Set<Chunk> set = new HashSet<>();
		activeList.forEach(x -> set.add(x.getBlock().getChunk()));
		return set;
	}

	public void addBurning(Furnace furnace, KFurnaceRecipe recipe, KFuelRecipe fuel, Set<String> include) {
		activeList.removeIf(x -> x.getBlock().getLocation().equals(furnace.getLocation()));
		activeList.add(new ProcessingFurnace(furnace, recipe, fuel, include));
	}

	public static ProcessingFurnace getActive(Block block) {
		return activeList.stream().filter(x -> x.getBlock().getBlock().equals(block)).findFirst().orElse(null);
	}

	private boolean processing(ProcessingFurnace furnace) {
		if(!(furnace.getBlock().getBlock().getState() instanceof Furnace))return true;
		if(!furnace.getBlock().getChunk().isLoaded())furnace.getBlock().getChunk().load();
		if(furnace.getSmelting() == null || furnace.getSmelting().getType() == Material.AIR) {
			furnace.setCookTime(0);
			return true;
		}
		KFurnaceRecipe recipe = furnace.getRecipe();
		if(recipe == null || !Utils.equals(furnace.getSmelting(), furnace.getOldSmelting())) {
			furnace.updateRecipe(RecipeMatcher.getFurnaceRecipe(furnace.getInventory(), furnace.getInclude()));
			furnace.setCookTime(0);
			return false;
		}
		if(furnace.isFull() || furnace.isProductReady() || !furnace.isOutReady()) {
			furnace.setCookTime(0);
			return false;
		}
		playParticle(furnace.getBlock().getLocation().add(0.5, 0.5, 0.5), furnace.getRecipe());
		int cook = furnace.nextCook();
		if(cook >= 200) {
			Main.cast("§d[鯖たん] §eアイテムが焼けたにゃん♪(ฅ•ω•ฅ)");
			furnace.setCookTime(0);
			FurnaceInventory inv = furnace.getInventory();
			ItemStack smelt = inv.getSmelting();
			if(smelt.getAmount() == 1) {
				inv.setSmelting(null);
			}else {
				smelt.setAmount(smelt.getAmount() - 1);
			}
			ItemStack result = furnace.getResult();
			for(ProductRecipe product : recipe.getProducts()) {
				if(product instanceof ProductReplace) {
					result = Utils.replaceMeta(product.buildItem(), null, furnace.getBlock().getLocation());
				}else {
					ItemStack item = Utils.replaceMeta(product.buildItem(), null, furnace.getBlock().getLocation());
					if(item != null) {
						dropItem(furnace.getBlock(), item);
					}
				}
			}
			ItemStack item = furnace.getOutput();
			exp(furnace.getBlock(), recipe.getExperience());
			if(hasButton(furnace.getBlock())) {
				throwItem(furnace.getBlock(), result);
			}else if(item == null) {
				inv.setResult(result);
			}else {
				item.setAmount(item.getAmount() + result.getAmount());
			}
			furnace.updateRecipe(RecipeMatcher.getFurnaceRecipe(furnace.getInventory(), furnace.getInclude()));
		}
		return false;
	}

	private boolean canNotBurning(ProcessingFurnace furnace) {
		return isNotBurning(furnace) && hasNotFuel(furnace);
	}

	private boolean isNotBurning(ProcessingFurnace furnace) {
		return !furnace.isBurning() && furnace.getRecipe() != null;
	}

	private boolean hasNotFuel(ProcessingFurnace furnace) {
		if(furnace.getSmelting() == null || furnace.getSmelting().getType() == Material.AIR)return true;
		KFuelRecipe recipe = RecipeMatcher.getFuelRecipe(furnace.getInventory(), furnace.getInclude());
		Main.cast(recipe);
		if(recipe != null) {
			updateFuel(furnace, recipe);
			furnace.setBurnTime((int) (recipe.getBurnTime() * StandUtils.getBurnRate(furnace.getBlock().getBlock())));
			furnace.setCookRate(recipe);
			return false;
		}
		return true;
	}

	private void updateFuel(ProcessingFurnace furnace, KFuelRecipe recipe) {
		FurnaceInventory inv = furnace.getInventory();
		ItemStack fuel = inv.getFuel();
		if(fuel.getType() == Material.LAVA_BUCKET || fuel.getType() == Material.WATER_BUCKET) {
			if(hasAnyButton(furnace.getBlock())) {
				dropItem(furnace.getBlock(), new ItemStack(Material.BUCKET));
				if(fuel.getAmount() == 1)inv.setFuel(null);
				else fuel.setAmount(fuel.getAmount() - 1);
			}else {
				inv.setFuel(new ItemStack(Material.BUCKET));
			}
		}else {
			if(fuel.getAmount() == 1)inv.setFuel(null);
			else fuel.setAmount(fuel.getAmount() - 1);
		}
	}

	private ActiveFurnace() {
		this.runTaskTimer(Main.getInstance(), 0, 1);
	}

	private void playParticle(Location loc, KFurnaceRecipe recipe) {
		if(recipe.getEffect() != null)recipe.getEffect().playEffect(null, loc);
		if(recipe.getSound() != null)recipe.getSound().playSound(null, loc);
	}

	private BlockFace[] faces = {BlockFace.DOWN, BlockFace.UP, BlockFace.NORTH, BlockFace.SOUTH, BlockFace.WEST, BlockFace.EAST};

	private boolean hasButton(Furnace furnace) {
		for(BlockFace f : faces) {
			Material mate = furnace.getBlock().getRelative(f).getType();
			if(mate == Material.STONE_BUTTON || mate == Material.END_ROD)return true;
		}
		return false;
	}

	private boolean hasAnyButton(Furnace furnace) {
		for(BlockFace f : faces) {
			Material mate = furnace.getBlock().getRelative(f).getType();
			if(mate == Material.WOOD_BUTTON || mate == Material.STONE_BUTTON || mate == Material.END_ROD)return true;
		}
		return false;
	}

	private void exp(Furnace furnace, float experience) {
		Location loc = furnace.getLocation();
		if(!exp.containsKey(loc))exp.put(loc, 0F);
		exp.put(loc, exp.get(loc) + experience);
	}

	private boolean throwItem(Furnace furnace, ItemStack item) {
		for(BlockFace f : faces) {
			Block b = furnace.getBlock().getRelative(f);
			if(b.getType() == Material.END_ROD) {
				BlockFace vector = faces[furnace.getBlock().getRelative(f).getData()];
				Vector vec = new Vector(vector.getModX(), vector.getModY(), vector.getModZ());
				dropItem(b, item, vector, vec);
				return true;
			}
		}
		return dropResult(furnace, item);
	}

	private boolean dropResult(Furnace furnace, ItemStack item) {
		Block block = furnace.getBlock();
		for(BlockFace face : faces) {
			Material mate = furnace.getBlock().getRelative(face).getType();
			if(mate == Material.STONE_BUTTON) {
				dropItem(block, item, face, new Vector(0, 0, 0));
				return true;
			}
		}
		return false;
	}

	private void dropItem(Furnace furnace, ItemStack item) {
		BlockFace f = BlockFace.valueOf(furnace.getData().toString().split(" ")[2]);
		Block block = furnace.getBlock();
		for(BlockFace face : faces) {
			Material mate = furnace.getBlock().getRelative(face).getType();
			if(mate == Material.WOOD_BUTTON) {
				dropItem(block, item, face, new Vector(0, 0, 0));
				return;
			}
		}
		if(!throwItem(furnace, item))
			dropItem(block, item, f, new Vector(0, 0, 0));
	}


	private void dropItem(Block block, ItemStack item, BlockFace face, Vector vec) {
		Item drop = block.getWorld().dropItem(block.getRelative(face).getLocation().add(0.5, 0.1, 0.5), item);
		drop.setVelocity(vec);
		drop.setPickupDelay(0);
	}
}
