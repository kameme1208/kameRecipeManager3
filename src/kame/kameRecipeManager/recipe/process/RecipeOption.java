package kame.kameRecipeManager.recipe.process;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Biome;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.MaterialData;
import org.bukkit.scheduler.BukkitRunnable;

import kame.kameRecipeManager.Main;
import kame.kameRecipeManager.reader.exception.IllegalItemException;
import kame.kameRecipeManager.recipe.KRecipe;
import kame.kameRecipeManager.recipe.option.CraftingEffect;
import kame.kameRecipeManager.recipe.option.CraftingSound;
import kame.kameRecipeManager.utils.RecipeCommandSender;
import kame.kameRecipeManager.utils.Utils;
/**
 * オプションのリスト
 * pathsound		:成功時の音
 * failsound		:失敗時の音
 * patheffect		:成功時のエフェクト
 * faileffect		:失敗時のエフェクト
 * pathexpand		:成功時の爆発
 * failexpand		:失敗時の爆発
 * pathplcmd		:成功時のプレイヤーコマンド
 * failplcmd		:失敗時のプレイヤーコマンド
 * pathclcmd		:成功時のコンソールコマンド
 * failclcmd		:失敗時のコンソールコマンド
 * pathinfo			:成功時のメッセージ
 * failinfo			:失敗時のメッセージ
 * pathbroadcast	:成功時のサーバーメッセージ
 * failbroadcast	:失敗時のサーバーメッセージ
 * playsound		:クラフト準備時の音
 * playeffect		:クラフト準備時のエフェクト
 * broadcast		:クラフト準備時のサーバーメッセージ
 * info				:クラフト準備時のメッセージ
 * permission		:必要パーミッション
 * score			:必要スコア									(小-大:任意)
 * level			:必要レベル									(小-大:任意)
 * levelcost		:クラフト成功時のレベルコスト
 * healthcost		:クラフト成功時の体力コスト
 * foodcost			:クラフト成功時の空腹コスト
 * saturationcost	:クラフト成功時の腹持ちレベルコスト
 * faillevelcost	:クラフト失敗時のレベルコスト
 * failhealthcost	:クラフト失敗時の体力レベルコスト
 * failfoodcost		:クラフト失敗時の空腹レベルコスト
 * failsaturationcost:クラフト失敗時の腹持ちレベルコスト
 * world			:クラフトできるワールド
 * biome			:クラフトできるバイオーム
 * lightlevelsky	:クラフトできる明るさレベル(空から)			(小-大:任意)
 * lightlevelblock	:クラフトできる明るさレベル(ブロックから)	(小-大:任意)
 * lightlevel		:クラフトできる明るさレベル(どっちも)		(小-大:任意)
 * health			:クラフトできる体力							(小-大:任意)
 * hunger			:クラフトできる満腹度						(小-大:任意)
 * saturation		:クラフトできる隠れ満腹度					(小-大:任意)
 * time				:クラフトできる時間							(小-大:任意)
 * totaltime		;クラフトできるサーバートータル時間			(小-大:任意)
 * blockpower		:クラフトできる赤石入力						(小-大:任意)
 * blocks			:周りにあるブロック
 */
public class RecipeOption {

	private static final ItemStack fail = new ItemStack(Material.BARRIER);

	static {
		ItemMeta im = fail.getItemMeta();
		im.setDisplayName(ChatColor.AQUA + "resultFail");
		im.setLore(Arrays.asList(ChatColor.RED + "クラフトに失敗しました"));
		fail.setItemMeta(im);
	}

	private static HashMap<String, List<RecipeOption>> options = new HashMap<>();

	private KRecipe recipe;
	private Location loc;

	/*
	 * levels & permissions & costsaturationcost
	 */
	private Set<String> permissions = new HashSet<>();

	private int levelmax = Integer.MAX_VALUE;
	private int levelmin = 0;

	private int levelcost = 0;
	private int healthcost = 0;
	private int foodcost = 0;
	private int saturationcost = 0;

	private int faillevelcost = 0;
	private int failhealthcost = 0;
	private int failfoodcost = 0;
	private int failsaturationcost = 0;

	/*
	 * messages & commands
	 */
	private List<String> pathinfo = new ArrayList<>();
	private List<String> failinfo = new ArrayList<>();

	private List<String> info 		= new ArrayList<>();
	private List<String> broadcast	= new ArrayList<>();

	private List<String> pathbroadcast = new ArrayList<>();
	private List<String> failbroadcast = new ArrayList<>();

	private List<String> pathclcmd = new ArrayList<>();
	private List<String> failclcmd = new ArrayList<>();

	private List<String> pathplcmd = new ArrayList<>();
	private List<String> failplcmd = new ArrayList<>();

	private Map<Location, MaterialData> pathsetblock = new HashMap<>();
	private Map<Location, MaterialData> failsetblock = new HashMap<>();


	/*
	 * sounds & effects
	 */
	private List<CraftingSound> pathsounds = new ArrayList<>();
	private List<CraftingSound> failsounds = new ArrayList<>();
	private List<CraftingSound> playsounds = new ArrayList<>();

	private List<CraftingEffect> patheffects = new ArrayList<>();
	private List<CraftingEffect> faileffects = new ArrayList<>();
	private List<CraftingEffect> playeffects = new ArrayList<>();

	/*
	 * biomes & worlds & other
	 */
	private Set<Biome> biomes = new HashSet<>();
	private Set<World> worlds = new HashSet<>();
	private Set<CheckBlock> blocks = new HashSet<>();
	private Set<CheckNbtBlock> nbtblock = new HashSet<>();
	private Set<String> scores = new HashSet<>();

	private int timemax = 24000;
	private int timemin = 0;

	private long totaltimemax = Long.MAX_VALUE;
	private long totaltimemin = 0;

	private int heightmax = 256;
	private int heightmin = 0;

	private int blockbrightmax = 15;
	private int blockbrightmin = 0;

	private int skybrightmax = 15;
	private int skybrightmin = 0;

	private int brightmax = 15;
	private int brightmin = 0;

	private int blockpowermax = 15;
	private int blockpowermin = 0;

	private int healthmax = Integer.MAX_VALUE;
	private int healthmin = 0;

	private int hungermax = Integer.MAX_VALUE;
	private int hungermin = 0;

	private int saturamax = Integer.MAX_VALUE;
	private int saturamin = 0;

	private Integer pathexpand = null;
	private Integer failexpand = null;


	@SuppressWarnings("deprecation")
	private RecipeOption(Player player, KRecipe recipe, Location loc) {
		this.recipe = recipe;
		this.loc = loc.clone();
		if(Main.debug)Main.cast(recipe.getOption());
		for(String option : recipe.getOption()) {
			String[] args = option.split(":", 2);
			if(args.length == 1) {
				Bukkit.getLogger().severe("<kameRecipeManager> ERROR: OptionArgumentsError [" + option + "]");
				continue;
			}
			int[] minmax;
			switch (args[0]) {
			case "pathsound":
				pathsounds.add(createSound(args[1]));
				break;
			case "failsound":
				failsounds.add(createSound(args[1]));
				break;
			case "patheffect":
				patheffects.add(createEffect(args[1]));
				break;
			case "faileffect":
				faileffects.add(createEffect(args[1]));
				break;
			case "playsound":
				playsounds.add(createSound(args[1]));
				break;
			case "playeffect":
				playeffects.add(createEffect(args[1]));
				break;
			case "pathexpand":
				if(args[1].matches("-?[0-9]+"))pathexpand = Integer.parseInt(args[1]);
				break;
			case "failexpand":
				if(args[1].matches("-?[0-9]+"))failexpand = Integer.parseInt(args[1]);
				break;
			case "pathplcmd":
				pathplcmd.add(Utils.engine(args[1], player, loc));
				break;
			case "failplcmd":
				failplcmd.add(Utils.engine(args[1], player, loc));
				break;
			case "pathclcmd":
				pathclcmd.add(Utils.engine(args[1], player, loc));
				break;
			case "failclcmd":
				failclcmd.add(Utils.engine(args[1], player, loc));
				break;
			case "pathinfo":
				pathinfo.add(Utils.engine(args[1], player, loc));
				break;
			case "failinfo":
				failinfo.add(Utils.engine(args[1], player, loc));
				break;
			case "pathbroadcast":
				pathbroadcast.add(Utils.engine(args[1], player, loc));
				break;
			case "failbroadcast":
				failbroadcast.add(Utils.engine(args[1], player, loc));
				break;
			case "broadcast":
				broadcast.add(Utils.engine(args[1], player, loc));
				break;
			case "info":
				if(player != null)info.add(Utils.engine(args[1], player, loc));
				break;
			case "permission":
				permissions.add(Utils.engine(args[1], player, loc));
				break;
			case "score":
				score(args[1]);
				break;
			case "level":
				minmax = minmax(args[1], levelmax);
				levelmin = minmax[0];
				levelmax = minmax[1];
				break;
			case "levelcost":
				if(args[1].matches("-?[0-9]+"))levelcost = Integer.parseInt(args[1]);
				break;
			case "healthcost":
				if(args[1].matches("-?[0-9]+"))healthcost = Integer.parseInt(args[1]);
				break;
			case "foodcost":
				if(args[1].matches("-?[0-9]+"))foodcost = Integer.parseInt(args[1]);
				break;
			case "saturationcost":
				if(args[1].matches("-?[0-9]+"))saturationcost = Integer.parseInt(args[1]);
				break;
			case "faillevelcost":
				if(args[1].matches("-?[0-9]+"))levelcost = Integer.parseInt(args[1]);
				break;
			case "failhealthcost":
				if(args[1].matches("-?[0-9]+"))healthcost = Integer.parseInt(args[1]);
				break;
			case "failfoodcost":
				if(args[1].matches("-?[0-9]+"))foodcost = Integer.parseInt(args[1]);
				break;
			case "failsaturationcost":
				if(args[1].matches("-?[0-9]+"))saturationcost = Integer.parseInt(args[1]);
				break;
			case "world":
				World world = Bukkit.getWorld(args[1]);
				if(world != null)worlds.add(world);
				break;
			case "biome":
				Biome biome = getBiome(args[1]);
				if(biome != null)biomes.add(biome);
				break;
			case "lightlevelsky":
				minmax = minmax(args[1], skybrightmax);
				skybrightmin = minmax[0];
				skybrightmax = minmax[1];
			case "lightlevelblock":
				minmax = minmax(args[1], blockbrightmax);
				blockbrightmin = minmax[0];
				blockbrightmax = minmax[1];
				break;
			case "lightlevel":
				minmax = minmax(args[1], brightmax);
				brightmin = minmax[0];
				brightmax = minmax[1];
				break;
			case "health":
				minmax = minmax(args[1], healthmax);
				healthmin = minmax[0];
				healthmax = minmax[1];
				break;
			case "hunger":
				minmax = minmax(args[1], hungermax);
				hungermin = minmax[0];
				hungermax = minmax[1];
				break;
			case "saturation":
				minmax = minmax(args[1], saturamax);
				saturamin = minmax[0];
				saturamax = minmax[1];
				break;
			case "time":
				minmax = minmax(args[1], timemax);
				timemin = minmax[0];
				timemax = minmax[1];
				break;
			case "totaltime":
				minmax = minmax(args[1], timemax);
				totaltimemin = minmax[0];
				totaltimemax = minmax[1];
				break;
			case "blockpower":
				minmax = minmax(args[1], blockpowermax);
				blockpowermin = minmax[0];
				blockpowermax = minmax[1];
				break;
			case "blocks":
				String[] str = args[1].split(":", 2);
				if(str.length == 2) {
					CheckBlock block = null;
					if(str[0].matches("-?[0-9]+,-?[0-9]+,-?[0-9]+")) {
						String[] pos = str[0].split(",");
						block = new CheckBlock(Integer.parseInt(pos[0]), Integer.parseInt(pos[1]), Integer.parseInt(pos[2]));
					}else {
						BlockFace face = getBlockFace(str[0]);
						if(face != null) block = new CheckBlock(face);
						else break;
					}
					for(String matestr : str[1].split("\\|")) {
						if(matestr.startsWith("!")) {
							Material material = Utils.parseMaterial(matestr.substring(1));
							if(material != null)block.addBudMaterial(material);
						}else {
							Material material = Utils.parseMaterial(matestr);
							if(material != null)block.addMaterial(material);
						}
					}
					blocks.add(block);
				}
				break;
			case "nearitem":
				break;
			case "nearblock":
				str = args[1].split(":", 2);
				if(str.length == 2 && str[0].matches("-?[0-9]+,-?[0-9]+,-?[0-9]+")) {
					CheckNbtBlock block = null;
					String[] pos = str[0].split(",");
					try {
						block = new CheckNbtBlock(Utils.parseItemStack(str[1]), Integer.parseInt(pos[0]), Integer.parseInt(pos[1]), Integer.parseInt(pos[2]));
						nbtblock.add(block);
					} catch (Exception e) {
						Bukkit.getLogger().severe("<kameRecipeManager> ERROR: OptionIllegualError [" + str + "]");
						Bukkit.getLogger().severe("<kameRecipeManager> " + e.getMessage());
					}
				}else {
					Bukkit.getLogger().severe("<kameRecipeManager> ERROR: OptionArgumentsError [" + option + "]");
				}
				break;
			case "pathsetblock":
				str = args[1].split(":", 2);
				if(str.length == 2) {
					Location location = loc.clone();
					if(str[0].matches("-?[0-9]+,-?[0-9]+,-?[0-9]+")) {
						String[] pos = str[0].split(",");
						location.add(Integer.parseInt(pos[0]), Integer.parseInt(pos[1]), Integer.parseInt(pos[2]));
					}else {
						BlockFace face = getBlockFace(str[0]);
						if(face != null)location.add(face.getModX(), face.getModY(), face.getModZ());
						else break;
					}
					Material mate = Utils.parseMaterial(str[1]);
					MaterialData material = new MaterialData(mate == null ? Material.AIR : mate);
					str = str[1].split(":");
					if(str[str.length-1].matches("1?[0-9]")) {
						byte data = (byte) Integer.parseInt(str[str.length-1]);
						material.setData(data);
					}
					pathsetblock.put(location, material);
				}
				break;
			case "failsetblock":
				str = args[1].split(":", 2);
				if(str.length == 2) {
					Location location = loc.clone();
					if(str[0].matches("-?[0-9]+,-?[0-9]+,-?[0-9]+")) {
						String[] pos = str[0].split(",");
						location.add(Integer.parseInt(pos[0]), Integer.parseInt(pos[1]), Integer.parseInt(pos[2]));
					}else {
						BlockFace face = getBlockFace(str[0]);
						if(face != null)location.add(face.getModX(), face.getModY(), face.getModZ());
						else break;
					}
					Material mate = Utils.parseMaterial(str[1]);
					MaterialData material = new MaterialData(mate == null ? Material.AIR : mate);
					str = str[1].split(":");
					if(str[str.length-1].matches("1?[0-9]")) {
						byte data = (byte) Integer.parseInt(str[str.length-1]);
						material.setData(data);
					}
					failsetblock.put(location, material);
				}
				break;
			default:
				break;
			}

		}
	}

	private int[] minmax(String str, int def) {
		if(!str.matches("[0-9]+([-~][0-9]+)?"))return new int[]{0, def};
		String[] line = str.split("[-~]");
		int a = Integer.parseInt(line[0]);
		int b = line.length > 1 ? Integer.parseInt(line[1]) : def;
		return new int[]{Math.min(a, b), Math.max(a, b)};
	}

	private void score(String str) {
		if(str.matches("[^:]+:[^:]+:-?[0-9]+~-?[0-9]+"))return;
		scores.add(str);
	}

	private CraftingSound createSound(String line) {
		try {
			if(line.indexOf("minecraft:") == 0)line = line.substring(10);
			String[] str = line.split(":");
			return new CraftingSound(str[0], Arrays.copyOfRange(str, 1, str.length));
		} catch (IllegalItemException e) {
			Bukkit.getLogger().severe("<kameRecipeManager> ERROR: OptionIllegualError [" + line + "]");
			Bukkit.getLogger().severe("<kameRecipeManager> " + e.getMessages());
			return new CraftingSound();
		}
	}

	private CraftingEffect createEffect(String line) {
		try {
			if(line.indexOf("minecraft:") == 0)line = line.substring(10);
			String[] str = line.split(":");
			return new CraftingEffect(str[0], Arrays.copyOfRange(str, 1, str.length));
		} catch (IllegalItemException e) {
			Bukkit.getLogger().severe("<kameRecipeManager> ERROR: OptionIllegualError [" + line + "]");
			Bukkit.getLogger().severe("<kameRecipeManager> " + e.getMessages());
			return new CraftingEffect();
		}
	}

	private Biome getBiome(String line) {
		try {
			return Biome.valueOf(line);
		}catch(IllegalArgumentException e) {
			Bukkit.getLogger().severe("<kameRecipeManager> ERROR: OptionIllegualError [" + line + "]");
			return null;
		}
	}

	private BlockFace getBlockFace(String line) {
		try {
			return BlockFace.valueOf(line);
		}catch(IllegalArgumentException e) {
			Bukkit.getLogger().severe("<kameRecipeManager> ERROR: OptionIllegualError [" + line + "]");
			return null;
		}
	}

	private static final BlockFace[] FACE = {BlockFace.UP, BlockFace.DOWN, BlockFace.NORTH, BlockFace.EAST, BlockFace.SOUTH, BlockFace.WEST};
	private boolean playResult(Player player) {
		if(player != null) {
			if(!checkNumber(levelmin,  player.getLevel(),			levelmax ))	return false;
			if(!checkNumber(healthmin, player.getHealth(),			healthmax))	return false;
			if(!checkNumber(saturamin, player.getSaturation(),		saturamax))	return false;
			if(!checkNumber(hungermin, player.getFoodLevel(),		hungermax))	return false;
			for(String perm : permissions)if(perm.indexOf('!') == 0 ?
						 player.hasPermission(perm.substring(1)) :
						!player.hasPermission(perm							 ))	return false;
			if(!scoreMatch(player											 )) return false;
		}
		Block block = loc.getBlock();
		int blight = block.getLightFromBlocks();
		int slight = block.getRelative(BlockFace.UP).getLightFromSky();
		for(BlockFace face :FACE)blight = Math.max(blight, block.getRelative(face).getLightFromBlocks() - 1);
		for(BlockFace face :FACE)slight = Math.max(slight, block.getRelative(face).getLightFromSky() - 1);
		if(blight < 0)blight = 0;
		if(slight < 0)slight = 0;
		int light = Math.max(blight, blight);
		if(!checkNumber(blockbrightmin,	slight,						 blockbrightmax	))return false;
		if(!checkNumber(skybrightmin,	blight,						 skybrightmax	))return false;
		if(!checkNumber(brightmin,		light,						 brightmax		))return false;
		if(!checkNumber(heightmin,		loc.getY(),					 heightmax		))return false;
		if(!checkNumber(blockpowermin,	block.getBlockPower(),		 blockpowermax	))return false;
		if(!checkNumber(timemin,  		loc.getWorld().getTime(),	 timemax  		))return false;
		if(!checkNumber(totaltimemin,	loc.getWorld().getFullTime(),totaltimemax	))return false;
		if(!(blocks.stream().allMatch(x -> x.containtsMaterial(block, BlockFace.NORTH	))||
			 blocks.stream().allMatch(x -> x.containtsMaterial(block, BlockFace.SOUTH	))||
			 blocks.stream().allMatch(x -> x.containtsMaterial(block, BlockFace.EAST	))||
			 blocks.stream().allMatch(x -> x.containtsMaterial(block, BlockFace.WEST))	))return false;
		if(!(nbtblock.stream().allMatch(x -> x.containtsMaterial(block, BlockFace.NORTH	))||
			 nbtblock.stream().allMatch(x -> x.containtsMaterial(block, BlockFace.SOUTH	))||
			 nbtblock.stream().allMatch(x -> x.containtsMaterial(block, BlockFace.EAST		))||
			 nbtblock.stream().allMatch(x -> x.containtsMaterial(block, BlockFace.WEST))	))return false;
		if(biomes.size() > 0 && !biomes.contains(block.getBiome() ))return false;
		if(worlds.size() > 0 && !worlds.contains(block.getWorld() ))return false;
		return true;
	}

	private boolean checkNumber(long min, long check, long max) {
		return min <= check && check <= max;
	}

	private boolean checkNumber(long min, double check, long max) {
		return min <= check && check <= max;
	}

	private boolean scoreMatch(Player player) {
		for(String str : scores) {
			String[] sc = str.split(":");
			int score = Bukkit.getScoreboardManager().getMainScoreboard()
					.getObjective(Utils.engine(sc[0], player, loc))
					.getScore(Utils.engine(sc[1], player, loc)).getScore();
			sc = sc[2].split("~|-", 2);
			int a = Integer.parseInt(sc[0]);
			int b = Integer.parseInt(sc[1]);
			if(!checkNumber(Math.min(a, b), score, Math.max(a, b)))return false;
		}
		return true;
	}

	@SuppressWarnings("deprecation")
	private void playPath(Player player) {
		pathclcmd.forEach(x -> Bukkit.dispatchCommand(new RecipeCommandSender(loc.getBlock()), Utils.engine(x, player, loc)));
		if(pathexpand != null)loc.getWorld().createExplosion(loc.getX(), loc.getY(), loc.getZ(), Math.abs(pathexpand), pathexpand < 0, pathexpand < 0);
		pathsounds.forEach(x -> x.playSound(player, loc));
		patheffects.forEach(x -> x.playEffect(player, loc));
		pathsetblock.entrySet().forEach(x -> {x.getKey().getBlock().setType(x.getValue().getItemType());x.getKey().getBlock().setData(x.getValue().getData());});
		if(player != null) {
			pathinfo.forEach(x -> player.sendMessage(Utils.engine(x, player, loc)));
			pathbroadcast.forEach(x -> player.sendMessage(Utils.engine(x, player, loc)));
			if(!player.isOp())pathplcmd.forEach(x -> {player.setOp(true);player.performCommand(Utils.engine(x, player, loc));player.setOp(false);});
			else pathplcmd.forEach(x -> player.performCommand(Utils.engine(x, player, loc)));
			player.setLevel(player.getLevel() - levelcost);
			player.setHealth(player.getHealth() - healthcost);
			player.setFoodLevel(player.getFoodLevel() - foodcost);
			player.setSaturation(player.getSaturation() - saturationcost);
		}
	}

	@SuppressWarnings("deprecation")
	private void playFail(Player player) {
		failclcmd.forEach(x -> Bukkit.dispatchCommand(new RecipeCommandSender(loc.getBlock()), Utils.engine(x, player, loc)));
		if(failexpand != null)loc.getWorld().createExplosion(loc.getX(), loc.getY(), loc.getZ(), Math.abs(failexpand), failexpand < 0, failexpand < 0);
		failsounds.forEach(x -> x.playSound(player, loc));
		faileffects.forEach(x -> x.playEffect(player, loc));
		failsetblock.entrySet().forEach(x -> {x.getKey().getBlock().setType(x.getValue().getItemType());x.getKey().getBlock().setData(x.getValue().getData());});
		if(player != null) {
			failinfo.forEach(x -> player.sendMessage(Utils.engine(x, player, loc)));
			failbroadcast.forEach(x -> player.sendMessage(Utils.engine(x, player, loc)));
			if(!player.isOp())failplcmd.forEach(x -> {player.setOp(true);player.performCommand(Utils.engine(x, player, loc));player.setOp(false);});
			else failplcmd.forEach(x -> player.performCommand(Utils.engine(x, player, loc)));
			player.setLevel(player.getLevel() - faillevelcost);
			player.setHealth(player.getHealth() - failhealthcost);
			player.setFoodLevel(player.getFoodLevel() - failfoodcost);
			player.setSaturation(player.getSaturation() - failsaturationcost);
		}
	}

	public static void clear(HumanEntity player) {
		options.remove(player.getUniqueId().toString());
	}

	public static void clear(Location loc) {
		options.remove(loc.toString());
	}

	public static void buildCraft(Player player, KRecipe recipe, Location loc) {
		List<RecipeOption> buffer = options.containsKey(player.getUniqueId().toString()) ?
				options.get(player.getUniqueId().toString()) : new ArrayList<>();
		buffer.add(new RecipeOption(player, recipe, loc));
		options.put(player.getUniqueId().toString(), buffer);
	}

	public static void build(Player player, KRecipe recipe, Location loc) {
		List<RecipeOption> buffer = options.containsKey(loc.toString()) ?
				options.get(loc.toString()) : new ArrayList<>();
		buffer.add(new RecipeOption(player, recipe, loc));
		options.put(loc.toString(), buffer);
	}

	public static HashMap<UUID, Buffer> buffers = new HashMap<>();
	private static class Buffer extends BukkitRunnable {
		private final Player player;
		private RecipeOption option;
		public void run() {
			option.playeffects.forEach(x -> x.playEffect(player, option.loc));
			option.playsounds.forEach(x -> x.playSound(player, option.loc));
			option.broadcast.forEach(x -> Bukkit.broadcastMessage(x));
			option.info.forEach(x -> player.sendMessage(x));
			option.playeffects.clear();
			option.playsounds.clear();
			option.broadcast.clear();
			option.info.clear();
		}

		private Buffer(Player player) {
			this.player = player;
			this.runTask(Main.getInstance());
		}

		private void updateResult(RecipeOption option) {
			this.option = option;
			buffers.put(player.getUniqueId(), this);
		}

	}

	public static ItemStack prepareCraft(Player player) {
		List<RecipeOption> buffer = options.containsKey(player.getUniqueId().toString()) ?
				options.get(player.getUniqueId().toString()) : new ArrayList<>();
		if(buffer.size() == 0)return null;
		if(buffer.size() == 1) {
			Buffer buf = buffers.get(player.getUniqueId());
			if(buf == null)buf = new Buffer(player);
			buf.updateResult(buffer.get(0));
			return buffer.get(0).recipe.getResult();
		}
		for(RecipeOption option : buffer)if(option.playResult(player)) {
			Buffer buf = buffers.get(player.getUniqueId());
			if(buf == null)buf = new Buffer(player);
			buf.updateResult(option);
			return option.recipe.getResult();
		}
		return fail;
	}

	@SuppressWarnings("unchecked")
	public static <T> T prepare(Player player, Location loc) {
		List<RecipeOption> buffer = options.containsKey(loc.toString()) ?
				options.get(loc.toString()) : new ArrayList<>();
		for(RecipeOption option : buffer)if(option.playResult(player)) {
			option.playPath(player);
			return (T) option.getRecipe();
		}
		for(RecipeOption option : buffer) {
			option.playFail(player);
			break;
		}
		return null;
	}

	public static RecipeOption crafting(Player player) {
		List<RecipeOption> buffer = options.containsKey(player.getUniqueId().toString()) ?
				options.get(player.getUniqueId().toString()) : new ArrayList<>();
		for(RecipeOption option : buffer) {
			if(option.playResult(player))return option;
		}
		for(RecipeOption option : buffer) {
			option.playFail(player);
			return null;
		}
		return null;
	}

	public static void resultCraft(Player player, RecipeOption option) {
		option.playPath(player);
	}

	public static boolean isOneByOne(Player player) {
		List<RecipeOption> option = options.get(player.getUniqueId().toString());
		return option == null || !option.stream().noneMatch(x ->
				x.levelcost    !=0	||x.healthcost    !=0	||x.foodcost    !=0	||x.saturationcost    !=0||
				x.faillevelcost!=0 	||x.failhealthcost!=0	||x.failfoodcost!=0	||x.failsaturationcost!=0
		);
	}

	public KRecipe getRecipe() {
		return recipe;
	}

	public Location getLocation() {
		return loc;
	}
}
