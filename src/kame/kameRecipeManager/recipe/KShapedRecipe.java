package kame.kameRecipeManager.recipe;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;
import org.bukkit.inventory.ShapedRecipe;

import kame.kameRecipeManager.Main;
import kame.kameRecipeManager.recipe.option.CheckMode;
import kame.kameRecipeManager.recipe.process.RecipeSifter;
import kame.kameRecipeManager.recipe.product.ProductRecipe;
import kame.kameRecipeManager.utils.Utils;

public class KShapedRecipe implements KRecipe {
	private final Map<Character, ItemStack> map;
	private final Map<Character, Material> materialmap;
	private final ShapedRecipe recipe;
	private final List<String> option;
	private final Set<CheckMode> check;
	private final List<ProductRecipe> products;

	public KShapedRecipe(ShapedRecipe recipe, Map<Character, ItemStack> map, List<String> option, List<ProductRecipe> products, Set<CheckMode> check) {
		this.recipe = recipe;
		this.map = map;
		this.materialmap = RecipeSifter.itemForMaterial(map);
		this.option = option;
		this.products = products;
		this.check = check;
	}

	public KShapedRecipe(ShapedRecipe recipe) {
		this.recipe = recipe;
		Map<Character, ItemStack> map = recipe.getIngredientMap();
		map.entrySet().removeIf(x -> x.getValue() == null || x.getValue().getType() == Material.AIR);
		this.map = map;
		this.materialmap = RecipeSifter.itemForMaterial(map);
		this.option = Collections.emptyList();
		this.products = Collections.emptyList();
		this.check = Main.getDefaultCheckMode();
	}

	public String[] getShape() {
		return this.recipe.getShape();
	}

	public Map<Character, ItemStack> getIngredientMap() {
		return this.map;
	}

	public Map<Character, Material> getMaterialMap() {
		return this.materialmap;
	}

	public Recipe getBukkitRecipe() {
		return this.recipe;
	}

	public ItemStack getResult() {
		return this.recipe.getResult();
	}

	public ItemStack getResult(Player player, Location loc) {
		return Utils.replaceMeta(this.recipe.getResult(), player, loc);
	}

	public List<String> getOption() {
		return this.option;
	}

	public Set<CheckMode> getCheckMode() {
		return check;
	}

	public List<ProductRecipe> getProducts() {
		return products;
	}
	
	public String toString() {
		return "KShapedRecipe: input=" + map + "\nresult=" + this.recipe.getResult();
	}

}
