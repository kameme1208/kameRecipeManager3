package kame.kameRecipeManager.recipe;

import java.util.Collections;

import org.bukkit.inventory.FurnaceRecipe;

import kame.kameRecipeManager.Main;
import kame.kameRecipeManager.recipe.option.CraftingEffect;
import kame.kameRecipeManager.recipe.option.CraftingSound;

public class VanillaFurnaceRecipe extends KFurnaceRecipe implements VanillaRecipe {
	private static final CraftingEffect EFFECT = new CraftingEffect();
	private static final CraftingSound SOUND = new CraftingSound();

	public VanillaFurnaceRecipe(FurnaceRecipe recipe) {

		super(recipe, recipe.getInput(), 200, 0, EFFECT, SOUND, Collections.emptyList(), Collections.emptyList(), Main.getDefaultCheckMode());

		switch(recipe.getInput().getType()) {
		case SAND:
		case COBBLESTONE:
		case NETHERRACK:
		case STAINED_CLAY:
		case CHORUS_FRUIT:
			super.exp = 0.1f;
			break;
		case SPONGE:
		case LOG:
		case LOG_2:
			super.exp = 0.15f;
			break;
		case CACTUS:
		case NETHER_STALK:
		case LAPIS_ORE:
			super.exp = 0.2f;
			break;
		case CLAY_BALL:
			super.exp = 0.3f;
			break;
		case CLAY:
		case MUTTON:
		case PORK:
		case POTATO:
		case RAW_BEEF:
		case RAW_CHICKEN:
		case RAW_FISH:
			super.exp = 0.35f;
			break;
		case IRON_ORE:
		case REDSTONE_ORE:
			super.exp = 0.7f;
			break;
		case GOLD_ORE:
		case DIAMOND_ORE:
		case EMERALD_ORE:
			super.exp = 1.0f;
		default:
			break;

		}
	}

	public String toString() {
		return "VanillaFurnaceRecipe: input=" + super.getInput() +  "\ntime=" + super.getCookTime() + "\nresult=" + super.getResult();
	}

}
