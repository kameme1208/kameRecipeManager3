package kame.kameRecipeManager.recipe;

import java.util.List;
import java.util.Set;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;
import org.bukkit.inventory.ShapelessRecipe;

import kame.kameRecipeManager.recipe.option.CheckMode;
import kame.kameRecipeManager.recipe.process.RecipeSifter;
import kame.kameRecipeManager.recipe.product.ProductRecipe;
import kame.kameRecipeManager.utils.Utils;

public class KShapelessRecipe implements KRecipe {
	private final List<ItemStack> items;
	private final List<Material> materialitems;
	private final ShapelessRecipe recipe;
	private final List<String> option;
	private final List<ProductRecipe> products;
	private final Set<CheckMode> check;

	public KShapelessRecipe(ShapelessRecipe recipe, List<ItemStack> items, List<String> option, List<ProductRecipe> products, Set<CheckMode> check) {
		this.recipe = recipe;
		this.materialitems = RecipeSifter.itemForMaterial(items);
		this.items = items;
		this.option = option;
		this.products = products;
		this.check = check;
	}

	public List<ItemStack> getIngredientList() {
		return this.items;
	}

	public List<Material> getMaterialList() {
		return materialitems;
	}

	public int getSize() {
		return items.size();
	}

	public Recipe getBukkitRecipe() {
		return this.recipe;
	}

	public ItemStack getResult() {
		return this.recipe.getResult();
	}

	public ItemStack getResult(Player player, Location loc) {
		return Utils.replaceMeta(this.recipe.getResult(), player, loc);
	}

	public List<String> getOption() {
		return this.option;
	}

	public Set<CheckMode> getCheckMode() {
		return check;
	}

	public List<ProductRecipe> getProducts() {
		return products;
	}
	
	public String toString() {
		return "KShapelessRecipe: input=" + items + "\nresult=" + this.recipe.getResult();
	}

}
