package kame.kameRecipeManager.recipe;

import java.util.Collections;

import org.bukkit.inventory.ShapelessRecipe;

import kame.kameRecipeManager.Main;

public class VanillaShapelessRecipe extends KShapelessRecipe implements VanillaRecipe {
	
	public VanillaShapelessRecipe(ShapelessRecipe recipe) {
		super(recipe, recipe.getIngredientList(), Collections.emptyList(), Collections.emptyList(), Main.getDefaultCheckMode());
	}
	
	public String toString() {
		return "VanillaShapelessRecipe: input=" + super.getIngredientList() + "\nresult=" + super.getResult();
	}
	
}
