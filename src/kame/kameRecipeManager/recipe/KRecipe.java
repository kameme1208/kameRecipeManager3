package kame.kameRecipeManager.recipe;

import java.util.List;
import java.util.Set;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;

import kame.kameRecipeManager.recipe.option.CheckMode;
import kame.kameRecipeManager.recipe.product.ProductRecipe;

public interface KRecipe {
	public Recipe getBukkitRecipe();

	public ItemStack getResult();

	public ItemStack getResult(Player player, Location loc);

	public List<String> getOption();

	public Set<CheckMode> getCheckMode();

	public List<ProductRecipe> getProducts();
}
