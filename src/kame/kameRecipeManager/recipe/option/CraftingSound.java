package kame.kameRecipeManager.recipe.option;

import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import kame.kameRecipeManager.reader.exception.IllegalItemException;
import kame.kameRecipeManager.reader.exception.IllegalItemException.ErrorType;

public class CraftingSound {
	private Sound sound;
	private String soundforname = "";
	private float volume = 1;
	private float pitch = 1;
	private int tickper = 1;

	public CraftingSound() {}

	public CraftingSound(String sound, String... options) throws IllegalItemException {
		try {
			this.sound = Sound.valueOf(sound);
		}catch(IllegalArgumentException e) {
			if(sound.matches("[0-9]+")) {
				int value = Integer.parseInt(sound);
				this.sound = Sound.values().length <= value ? Sound.values()[value] : null;
			}else {
				soundforname = sound;
			}
		}
		int i = 0;
		for(String str : options) {
			String line[] = str.split("=", 2);
			switch(line[0]) {
			case "v":
			case "volume"://volume=0.1
				try {
					this.volume = Float.parseFloat(line[1]);
				}catch(IllegalArgumentException e) {
					throw new IllegalItemException(ErrorType.WARN, "", "SoundNumberFormatError ", "(", str, ") is Invalid character");
				}
				break;
			case "p":
			case "pitch"://pitch=0.1
				try {
					this.pitch = Float.parseFloat(line[1]);
				}catch(IllegalArgumentException e) {
					throw new IllegalItemException(ErrorType.WARN, "", "SoundNumberFormatError ", "(", str, ") is Invalid character");
				}
				break;
			case "per":
			case "tickper"://pitch=0.1
				try {
					this.tickper = Integer.parseInt(line[1]);
				}catch(IllegalArgumentException e) {
					throw new IllegalItemException(ErrorType.WARN, "", "SoundNumberFormatError ", "(", str, ") is Invalid character");
				}
				break;
			default:
				switch(i) {
				case 0:
					try {
						this.volume = Float.parseFloat(str);
					}catch(NumberFormatException e) {
						throw new IllegalItemException(ErrorType.WARN, "", "SoundNumberFormatError ", "(", str, ") is Invalid character");
					}
					break;
				case 1:
					try {
						this.pitch = Float.parseFloat(str);
					}catch(NumberFormatException e) {
						throw new IllegalItemException(ErrorType.WARN, "", "SoundNumberFormatError ", "(", str, ") is Invalid character");
					}
					break;
				case 2:
					try {
						this.tickper = Integer.parseInt(str);
					}catch(NumberFormatException e) {
						throw new IllegalItemException(ErrorType.WARN, "", "SoundNumberFormatError ", "(", str, ") is Invalid character");
					}
					break;
				default:
						throw new IllegalItemException(ErrorType.WARN, "", "SoundTypeError ", "(", str, ") is Invalid character");
				}
			}
			i++;
		}
	}

	public void playSound(Player player, Location loc) {
		if(player == null) {
			playBlockSound(loc);
		}else {
			playPlayerSound(player, loc);
		}
	}

	private void playPlayerSound(Player player, Location loc) {
		if((sound == null && soundforname.equals("")) || (System.currentTimeMillis() / 50 % tickper) != 0)return;
		if(sound != null) {
			player.playSound(loc, sound, volume, pitch);
		}else {
			player.playSound(loc, soundforname, volume, pitch);
		}
	}

	private void playBlockSound(Location loc) {
		if(sound != null) {
			loc.getWorld().playSound(loc, sound, volume, pitch);
		}else {
			loc.getWorld().playSound(loc, soundforname, volume, pitch);
		}
	}

	public boolean isEnpty() {
		return sound == null && soundforname.equals("");
	}
}
