package kame.kameRecipeManager.recipe.option;

public enum CheckMode {
	uncheckname,
	unchecklore,
	uncheckenchant,
	uncheckitemflags,
	uncheckother,
	patternname,
	patternlore,
	;

	public static CheckMode fromName(String ckm) {
		try {
			return CheckMode.valueOf(ckm.toLowerCase());
		}catch(IllegalArgumentException ew) {
			return null;
		}
	}
}
