package kame.kameRecipeManager.recipe.option;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import kame.kameRecipeManager.reader.exception.IllegalItemException;
import kame.kameRecipeManager.reader.exception.IllegalItemException.ErrorType;

public class CraftingEffect {
	private Particle particle;
	private ItemStack mate = new ItemStack(Material.STONE);
	private float speed = 0.05f;
	private float offset = 0;
	private int count = 1;
	private float offsetX = 0.2f;
	private float offsetY = 0.2f;
	private float offsetZ = 0.2f;
	private int tickper = 1;

	public CraftingEffect() {}

	public CraftingEffect(String effect, String... options) throws IllegalItemException {
		try {
			this.particle = Particle.valueOf(effect);
		}catch(IllegalArgumentException e) {
			if(effect.matches("[0-9]+")) {
				int value = Integer.parseInt(effect);
				this.particle = Particle.values().length <= value ? Particle.values()[value] : null;
			}
		}
		Material material = null;
		short durability = 0;
		for(String str : options) {
			String data[];
			String line[] = str.split("=", 2);
			if(line.length != 2) {
				throw new IllegalItemException(ErrorType.WARN, "", "EffectAugumentError ", "(", str, ")");
			}
			switch(line[0].toLowerCase()) {
			case "type":
			case "id"://id=0
				try {
					material = Material.matchMaterial(line[1]);
				}catch(Exception e) {
					throw new IllegalItemException(ErrorType.WARN, "", "MaterialTypeError ", "(", str, ")");
				}
				break;
			case "d":
			case "data"://data=0
				try {
					durability = Short.parseShort(line[1]);
				}catch(Exception e) {
					throw new IllegalItemException(ErrorType.WARN, "", "EffectNumberFormatError ", "(", str, ")");
				}
				break;
			case "s":
			case "speed"://speed=0.1
				if(!line[1].matches("[0-9]+(.[0-9]+)?")) {
					throw new IllegalItemException(ErrorType.WARN, "", "EffectNumberFormatError ", "(", str, ")");
				}
				this.speed = Float.parseFloat(line[1]);
				break;
			case "c":
			case "count"://count=10
				if(!line[1].matches("[0-9]+")) {
					throw new IllegalItemException(ErrorType.WARN, "", "EffectNumberFormatError ", "(", str, ")");
				}
				this.count = Integer.parseInt(line[1]);
				break;
			case "rgb":
			case "color"://color=r,g,b
				if(!line[1].matches("[0-9]+(.[0-9]+)?,[0-9]+(.[0-9]+)?,[0-9]+(.[0-9]+)?")) {
					throw new IllegalItemException(ErrorType.WARN, "", "EffectNumberFormatError ", "(", str, ")");
				}
				data = line[1].split(",");
				this.offsetX = Float.parseFloat(data[0]);
				this.offsetY = Float.parseFloat(data[1]);
				this.offsetZ = Float.parseFloat(data[2]);
				break;
			case "v":
			case "vector"://vector=x,y,z
				if(!line[1].matches("[0-9]+(.[0-9]+)?,[0-9]+(.[0-9]+)?,[0-9]+(.[0-9]+)?")) {
					throw new IllegalItemException(ErrorType.WARN, "", "EffectNumberFormatError ", "(", str, ")");
				}
				data = line[1].split(",");
				this.offsetX = Float.parseFloat(data[0]);
				this.offsetY = Float.parseFloat(data[1]);
				this.offsetZ = Float.parseFloat(data[2]);
				break;
			case "r":
			case "range"://range=r
				if(!line[1].matches("[0-9]+(.[0-9]+)?")) {
					throw new IllegalItemException(ErrorType.WARN, "", "EffectNumberFormatError ", "(", str, ")");
				}
				float r = Float.parseFloat(line[1]);
				this.offsetX = r;
				this.offsetY = r;
				this.offsetZ = r;
				break;
			case "y":
			case "offsety"://offset=y
				if(!line[1].matches("[0-9]+(.[0-9]+)?")) {
					throw new IllegalItemException(ErrorType.WARN, "", "EffectNumberFormatError ", "(", str, ")");
				}
				this.offset = Float.parseFloat(line[1]);
				break;
			case "per":
			case "tickper"://pitch=0.1
				if(!line[1].matches("[0-9]+")) {
					throw new IllegalItemException(ErrorType.WARN, "", "EffectNumberFormatError ", "(", str, ")");
				}
				this.tickper  = Integer.parseInt(line[1]);
				break;
			default:

				if(!str.matches("[0-9]+(,[0-9]+)?")) {
					throw new IllegalItemException(ErrorType.WARN, "", "EffectTypeError ", "(", str, ")");

				}
				data = str.split(",");
				try {
					mate = new ItemStack(Material.values()[Integer.parseInt(data[0])], 1, Short.parseShort(data[1]));
				}catch(Exception e) {
					throw new IllegalItemException(ErrorType.WARN, "", "MaterialTypeError ", "(", str, ")");
				}
			}
		}
		if(material != null) {
			mate = new ItemStack(material, 1, durability);
		}

	}

	public void playEffect(Player player, Location loc) {
		if(particle == null || (System.currentTimeMillis() / 50 % tickper) != 0)return;
		Object obj = null;
		if(particle.equals(Particle.BLOCK_CRACK) || particle.equals(Particle.BLOCK_DUST)) {
			obj = mate.getData();
		}
		if(particle.equals(Particle.ITEM_CRACK)) {
			obj = mate;
		}

		if(player != null)player.spawnParticle(particle, loc.clone().add(0, offset, 0), count, offsetX, offsetY, offsetZ, speed, obj);
		else loc.getWorld().spawnParticle(particle, loc.clone().add(0, offset, 0), count, offsetX, offsetY, offsetZ, speed, obj);

	}

	public boolean isEnpty() {
		return particle == null;
	}
}
