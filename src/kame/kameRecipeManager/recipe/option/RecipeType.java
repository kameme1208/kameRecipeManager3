package kame.kameRecipeManager.recipe.option;

public enum RecipeType {
	Shaped("CRAFT"),
	Shapeless("COMBINE"),
	Furnace("SMELT"),
	Fusion("FUSION"),
	Smash("SMASH"),

	Fuel("FUEL"),

	Droped("NONE");

	private final String type;
	private RecipeType(String type) {
		this.type = type;
	}

	public String getName() {
		return this.type;
	}

}
