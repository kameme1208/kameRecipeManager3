package kame.kameRecipeManager.recipe;

import org.bukkit.inventory.ShapedRecipe;

public class VanillaShapedRecipe extends KShapedRecipe implements VanillaRecipe {

	public VanillaShapedRecipe(ShapedRecipe recipe) {
		super(recipe);
	}

	public String toString() {
		return "VanillaShapedRecipe: input=" + super.getIngredientMap() + "\nresult=" + super.getResult();
	}

}
