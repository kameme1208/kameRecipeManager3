package kame.kameRecipeManager.recipe.product;

import org.bukkit.inventory.ItemStack;

public class ProductAdd implements ProductRecipe {

	private final int per;
	private final ItemStack item;

	public ProductAdd(int per, ItemStack item) {
		this.per = per;
		this.item = item;
	}

	public ItemStack buildItem() {
		double random = Math.random() * 100000D;
		return per > random ? item : null;
	}
}
