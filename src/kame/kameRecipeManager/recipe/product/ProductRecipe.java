package kame.kameRecipeManager.recipe.product;

import org.bukkit.inventory.ItemStack;

public interface ProductRecipe {

	public ItemStack buildItem();
}
