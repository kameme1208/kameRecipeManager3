package kame.kameRecipeManager.recipe.product;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.inventory.ItemStack;

import kame.kameRecipeManager.reader.exception.IllegalItemException;
import kame.kameRecipeManager.reader.exception.IllegalItemException.ErrorType;

public class ProductReplace implements ProductRecipe{
	//1 = 0.001%(x1000)
	private ArrayList<Integer> perList = new ArrayList<>();
	private ArrayList<ItemStack> itemList = new ArrayList<>();

	public void addPerItem(int per, ItemStack item) throws IllegalItemException {
		if(perList.size() > 0)per += perList.get(perList.size()-1);
		if(per > 100000)throw new IllegalItemException(ErrorType.OTHER, "" , "ProductItemError. total value must not exceed 100! (" + per/1000f + ")");
		perList.add(per);
		itemList.add(item);
	}

	public ItemStack buildItem() {
		int random = (int) (Math.random() * 100000D);
		for(int i = 0; i < perList.size(); i++) {
			if(random < perList.get(i))return itemList.get(i);
		}
		return null;
	}

	public List<String> getList() {
		List<String> list = new ArrayList<>();
		int per = 100000;
		for(int i = 0; i < perList.size(); i++) {
			ItemStack item = itemList.get(i);
			int percent = perList.get(i);
			per -= percent;
			String name = item.getItemMeta().getDisplayName();
			if(name == null)name = item.getType().toString().toLowerCase();
			list.add("§6§l>>§c" + (percent / 1000.0) + "§e% §b" + name);
		}
		if(per > 0)list.add("§6§l>>§c" + (per / 1000.0) + "§e% §bnone");
		return list;
	}
}
