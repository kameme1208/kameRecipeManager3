package kame.kameRecipeManager.recipe;

import java.util.List;
import java.util.Set;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;

import kame.kameRecipeManager.recipe.option.CheckMode;
import kame.kameRecipeManager.recipe.option.CraftingEffect;
import kame.kameRecipeManager.recipe.option.CraftingSound;
import kame.kameRecipeManager.recipe.process.RecipeSifter;
import kame.kameRecipeManager.recipe.product.ProductRecipe;
import kame.kameRecipeManager.utils.Utils;

public class KSmashRecipe implements KRecipe {

	private final List<ItemStack> input;
	private final List<Material> materials;
	private final String spell;
	private final ItemStack result;
	private final List<String> option;
	private final CraftingEffect effect;
	private final CraftingSound sound;
	private final List<ProductRecipe> products;
	private final Set<CheckMode> check;
	private final ItemStack smashitem;
	private final short consume;

	public KSmashRecipe(Recipe recipe, ItemStack result, ItemStack smashitem, short consume, List<ItemStack> items, String spell, CraftingEffect effect, CraftingSound sound, List<String> option, List<ProductRecipe> products, Set<CheckMode> check) {
		this.materials = RecipeSifter.itemForMaterial(items);
		this.input = items;
		this.result = result;
		this.spell = spell;
		this.effect = effect;
		this.sound = sound;
		this.option = option;
		this.products = products;
		this.check = check;
		this.smashitem = smashitem;
		this.consume = consume;
	}

	public List<ItemStack> getInput() {
		return this.input;
	}

	public List<Material> getInputMaterial() {
		return this.materials;
	}

	public Recipe getBukkitRecipe() {
		return null;
	}

	public String getSpell() {
		return spell;
	}

	public ItemStack getResult() {
		return result;
	}

	public ItemStack getResult(Player player, Location loc) {
		return Utils.replaceMeta(result, player, loc);
	}

	public CraftingEffect getEffect() {
		return effect;
	}

	public CraftingSound getSound() {
		return sound;
	}

	public List<String> getOption() {
		return this.option;
	}

	public Set<CheckMode> getCheckMode() {
		return check;
	}

	public List<ProductRecipe> getProducts() {
		return products;
	}

	public boolean isRequestItem() {
		return smashitem != null;
	}
	
	public ItemStack getSmashItem() {
		return smashitem;
	}

	public short getConsumeDurability() {
		return consume;
	}
	
	public String toString() {
		return "KShapedRecipe: input=" + input + "\nspell=" + spell + "\nresult=" + result;
	}

}
