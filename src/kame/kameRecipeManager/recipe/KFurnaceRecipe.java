package kame.kameRecipeManager.recipe;

import java.util.List;
import java.util.Set;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.inventory.FurnaceRecipe;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;

import kame.kameRecipeManager.recipe.option.CheckMode;
import kame.kameRecipeManager.recipe.option.CraftingEffect;
import kame.kameRecipeManager.recipe.option.CraftingSound;
import kame.kameRecipeManager.recipe.product.ProductRecipe;
import kame.kameRecipeManager.utils.Utils;

public class KFurnaceRecipe implements KRecipe {
	private final ItemStack input;
	private final FurnaceRecipe recipe;
	private final List<String> option;
	protected float exp;
	private final int time;
	private final CraftingEffect effect;
	private final CraftingSound sound;
	private final Set<CheckMode> check;
	private final List<ProductRecipe> products;

	public KFurnaceRecipe(FurnaceRecipe recipe, ItemStack item, int time, float exp, CraftingEffect effect, CraftingSound sound, List<String> option, List<ProductRecipe> products, Set<CheckMode> check) {
		this.recipe = recipe;
		this.input = item;
		this.time = time;
		this.exp = exp;
		this.effect = effect;
		this.sound = sound;
		this.option = option;
		this.products = products;
		this.check = check;
	}

	public ItemStack getInput() {
		return this.input;
	}

	public int getCookTime() {
		return this.time;
	}

	public Recipe getBukkitRecipe() {
		return this.recipe;
	}

	public ItemStack getResult() {
		return this.recipe.getResult();
	}

	public ItemStack getResult(Player player, Location loc) {
		return Utils.replaceMeta(this.recipe.getResult(), player, loc);
	}

	public CraftingEffect getEffect() {
		return effect;
	}

	public CraftingSound getSound() {
		return sound;
	}

	public List<String> getOption() {
		return this.option;
	}

	public float getExperience() {
		return exp;
	}

	public Set<CheckMode> getCheckMode() {
		return check;
	}

	public List<ProductRecipe> getProducts() {
		return products;
	}

	public String toString() {
		return "KFurnaceRecipe: input=" + input + "\ntime=" + time + "\nresult=" + this.recipe.getResult();
	}

}
