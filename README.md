Spigot用レシピプラグイン<br>
Bukkitでは動作しないので注意（Java8以降）<br>

<h5>ダウンロード → [issues](https://gitlab.com/kameme1208/kameRecipeManager3/issues)</h5>

<br>
--主なバニラからの仕様変更<br>
・Lore文のついたクラフト、金床での名前変更が出来なくなる<br>
・ツールはいつの間にか作れるようにしました<br>
・アイテムのNBTが変更されているブロックは設置時に保持されるように<br>
・エンティティがブロックを変更するとき上の処理が行われたブロックは変更をキャンセルするように<br>
<br>
--プラグインの仕様時の注意<br>
・クラフトの処理を上書きしているため、デフォルトのレシピを追加したりするプラグインとはほとんど競合すると思われます<br>
・1.9以降のみ対応予定です（サウンドファイル等）<br>
・予期せぬ不具合、謎い仕様がその他多々あります<br>
<br>
--レシピの作り方<br>
#通常の作業台にはあまり手を加えない仕様にしております<br>
#なので通常の作業台では特殊アイテムを普通のアイテムとしてのクラフトも許してしまうこともあります（Lore文で解決）<br>
#アイテムの説明文の部分に@craftとついているブロックが特殊レシピ対象です<br>
#また@hideとつけると置いたときにネームタグを表示しなかったり@inHandで手持ち作業台になります<br><br>
<br>

<h1>使い方・説明</h1>
<h3>クラフト台本体のLore文のオプション</h3>
|オプション |対象クラフト       |使用・挿入例           |説明                                   |
|-----------|-------------------|-----------------------|---------------------------------------|
|@craft  	|すべて             |@craft                 |この文字を入れるとクラフト台になります |
|@hide	    |すべて             |@hide                  |クラフト台のタグ表示を隠します         |
|@inhand	|**CRAFT,COMBINE**  |@inhand                |手持ちのクラフト台にします             |
|@include  	|すべて             |@include:クラフト台名  |そのレシピを含むクラフト台             |
|@burnrate  |**SMELT,FUSION**   |@burnrate:1.5          |かまど個別の燃焼時間(倍率)を指定します |
|@cookrate  |**SMELT,FUSION**   |@cookrate:2.0          |かまど個別の精錬速度(倍率)を指定します |


<h3>オプションのリスト</h3>
|オプション |説明               |
|-----------|-------------------|
|@name  	|表示名             |
|@lore	    |説明文             |
|@ench	    |エンチャント 強さ  |
|@tag   	|GiveのNBTタグ      |
|@flag  	|フラグ名 / ALL     |
* 例:=== STONE @name:§b伝説の石 @lore:§aこれを持って友達に見せびらかそう @ench:1:1 @flag:ALL

<h3>チェックタイプのオプション</h3>
|オプション      |説明                  |
|----------------|----------------------|
|uncheckname     |アイテム名を無視      |
|unchecklore     |説明文を無視          |
|uncheckenchant  |エンチャントを無視    |
|uncheckitemflags|アイテムフラグを無視  |
|uncheckother    |その他を無視          |
|patternname     |名前を正規表現で比較  |
|patternlore     |説明文を正規表現で比較|
* 使用例 $checkmode:uncheckname  <br>

<br>
<h4>レシピ特有のオプション</h4>
|オプション                 |説明                   |使用できるレシピ       |
|---------------------------|-----------------------|-----------------------|
|$times:\<tick>	            |時間を指定		    	|かまど、醸造台         |
|$exp:\<exp>		    	|経験値				    |かまど                 |
|$effect:\<effecttype>:...  |クラフト時エフェクト   |かまど、醸造台、金床   |
|$sound:\<soundtype>:...    |クラフト時サウンド	    |かまど、醸造台、金床   |
|$spell:\<spell>		    |指定スペル			    |金床                   |
|$cost:\<cost>		    	|消費耐久値			    |金床                   |
|$needitem:\<item>	    	|指定アイテム		    |金床                   |

<br>
<h4>実行時に組み立てられるオプション</h4>
* クラフト準備時の周囲の環境、プレイヤーの状態で判別する事が出来るオプションです<br>
  1行に複数のオプションを指定することも出来ます。<br>
  見にくい場合は複数行に分けて指定しても構いません。<br>
  オプション行の始まりは必ず[+$options: +](要空白)を付けてください。<br>
  + 記述例: $options: @playsound:ENTITY_WOLF_AMBIENT:v=1:p=1<br>
  + 例2(旧互換): $option @playsound:ENTITY_WOLF_AMBIENT:v=1:p=1<br>

|<h5>[+クラフトサウンド+]</h5>||
|---------------|-------------------|
|pathsound		|成功時の音         |
|failsound		|失敗時の音         |
|playsound		|クラフト準備時の音 |
* 例:@playsound:<SOUND_NAME>:...  <br>
* 例:@playsound:playsound:ENTITY_WOLF_AMBIENT:v=1:p=1 (v - Volume, p - Pitch)  <br>

|<h5>[+クラフトエフェクト+]</h5>||
|---------------|---------------------------|
|patheffect		|成功時のエフェクト         |
|faileffect		|失敗時のエフェクト         |
|playeffect		|クラフト準備時のエフェクト |
* 例:@playeffect:<EFFECT_NAME>:...  <br>
例:@playeffect:BLOCK_DUST:id=1,d=0:s=1:c=10:\v:0.2,0.5,0.1:per:10  <br>
* 例:(d - Data(id,meta), s - Speed, c - Count, rgb - Color, v - Vector(Offset), r - range, y - offdrtLocation(Y), per - tickper)  <br>

<h5>エフェクトの種類 いっぱいあります /kr effects [TAB]で一覧と実行することでパーティクルを表示させて確認する事ができます</h5>

EXPLOSION\_NORMAL, EXPLOSION\_LARGE, EXPLOSION\_HUGE, FIREWORKS\_SPARK, WATER\_BUBBLE, WATER\_SPLASH, WATER\_WAKE, SUSPENDED, SUSPENDED\_DEPTH, CRIT, CRIT\_MAGIC, SMOKE\_NORMAL, SMOKE\_LARGE, SPELL, SPELL\_INSTANT, SPELL\_MOB, SPELL\_MOB\_AMBIENT, SPELL\_WITCH, DRIP\_WATER, DRIP\_LAVA, VILLAGER\_ANGRY, VILLAGER\_HAPPY, TOWN\_AURA, NOTE, PORTAL, ENCHANTMENT\_TABLE, FLAME, LAVA, FOOTSTEP, CLOUD, REDSTONE, SNOWBALL, SNOW\_SHOVEL, SLIME, HEART, BARRIER, ITEM\_CRACK, BLOCK\_CRACK, BLOCK\_DUST, WATER\_DROP, ITEM\_TAKE, MOB\_APPEARANCE, DRAGON\_BREATH, END\_ROD, DAMAGE\_INDICATOR, SWEEP\_ATTACK, FALLING\_DUST;


<h5>[+エフェクト設定項目+] エフェクトの設定がややこしいので分けて書いておきます</h5>

|型|省略型(別の型)|使い方|意味|
|---------------|---------------|-------|------------------------------|
|id	        	|type           |type=STONE or type=1|アイテムのID(大文字文字、数字)|
|data	    	|d              |data=0 |アイテムの耐久値|
|speed	    	|s              |speed=0.1|エフェクトの速さ|
|count	    	|c              |count=16|エフェクトの数|
|color	    	|rgb, vector, v |color=0.1,0.25,0.3, v=0,0.1,0|エフェクトの色 or 進行方向 or バラつき※|
|range	    	|r              |range=0.25(v=0.25,0.25,0.25と同等)|エフェクトのバラつき(上と同じだが一括設定)|
|offsetY		|y              |y=1.5(1.5ブロック上に発生)|エフェクトの上下方向のズレ(x,zは変更不可)|
|tickper		|per            |per=10(10tickに1回発生)|エフェクトの頻度|
|数字,数字		|無し           |49,0(黒曜石のメタ値0)|エフェクトのIDと耐久値|
※ : エフェクトの色と進行方向は一部パーティクル・パーティクル数が0の時のみ適応されます

|<h5>[+爆発+]</h5>||
|---------------|---------------------|
|pathexpand		|成功時の爆発         |
|failexpand		|失敗時の爆発         |
* 例:@failexpand:\<amount>  <br>

|<h5>[+コマンド・メッセージ+]</h5>||
|---------------|-----------------------------------|
|pathplcmd		|成功時のプレイヤーコマンド         |
|failplcmd		|失敗時のプレイヤーコマンド         |
|pathclcmd      |成功時のコンソールコマンド         |
|failclcmd		|失敗時のコンソールコマンド         |
|pathinfo		|成功時のメッセージ                 |
|failinfo		|失敗時のメッセージ                 |
|pathbroadcast	|成功時のサーバーメッセージ         |
|failbroadcast	|失敗時のサーバーメッセージ         |
|broadcast		|クラフト準備時のサーバーメッセージ |
|info		    |クラフト準備時のメッセージ         |
|permission		|必要パーミッション                 |
* @info:\<message.../command...>  <br>

|<h5>[+クラフトコスト+]</h5>||
|-------------------|-----------------------------------|
|levelcost		    |クラフト成功時のレベルコスト       |
|healthcost		    |クラフト成功時の体力コスト         |
|foodcost           |クラフト成功時の空腹コスト         |
|saturationcost		|クラフト成功時の腹持ちレベルコスト |
|faillevelcost		|クラフト失敗時のレベルコスト       |
|failhealthcost		|クラフト失敗時の体力レベルコスト   |
|failfoodcost	    |クラフト失敗時の空腹レベルコスト   |
|failsaturationcost	|クラフト失敗時の腹持ちレベルコスト |
* @cost:\<amount>  <br>

|<h5>[+地形・ワールド+]</h5>||
|-----------|---------------------------|
|world		|クラフトできるワールド     |
|biome		|クラフトできるバイオーム   |
* @world:\<name>  <br>

<br>

|<h5>[+スコアボード+]</h5>||
|-----------|-----------------------|
|score		|必要スコア (小-大:任意)|
* @score:Objective:Name:10-20 (or 10~20)  <br>

|<h5>[+プレイヤー、ブロックの状態+]</h5>||
|-------------------|-----------------------------------|
|level		        |必要レベル                                                 |
|lightlevelsky	    |クラフトできる明るさレベル(空から)   (小-大:任意)          |
|lightlevelblock    |クラフトできる明るさレベル(ブロックから)   (小-大:任意)    |
|lightlevel		    |クラフトできる明るさレベル(どっちも)   (小-大:任意)        |
|health		        |クラフトできる体力   (小-大:任意)                          |
|hunger		        |クラフトできる満腹度   (小-大:任意)                        |
|saturation	        |クラフトできる隠れ満腹度   (小-大:任意)                    |
|time	            |クラフトできる時間   (小-大:任意)                          |
|totaltime	        |クラフトできるサーバートータル時間   (小-大:任意)          |
|blockpower	        |クラフトできる赤石入力   (小-大:任意)                      |
* @level:\<levelmin>  <br>
* @level:\<min>~\<max>  <br>
* @level:10-20 (or 10~20)  <br>

|<h5>[+ブロック+]</h5>||
|------------|--------------------------|
|blocks      |周りにあるブロック        |
|pathsetblock|相対座標からのブロック変更|
|failsetblock|相対座標からのブロック変更|
* @blocks:\<FACE>:\<TYPE>  <br>
* @blocks:0,-1,0:STONE|SAND  <br>
* @blocks:UP:DOAMOND_BLOCK  <br>
* @pathsetblock:DOWN:AIR  <br>

<br>
今のところこれくらいだよ追加していく予定だからその時はよろしくね  <br>
↓ここから実際に少しだけレシピを追加してみる  <br><br>
<h2>作業台レシピの追加</h2>

* **圧縮台という作業台に不定形レシピでレシピを作成する**<br>
`8個の丸石 <=> 1個の圧縮丸石`

>>>
**COMBINE** 圧縮台  <br>
I8= COBBLESTONE  <br>
=== COBBLESTONE @**name**:圧縮ブロック @**lore**:丸石x8 @**ench**:0:1 @**flag**:ALL  <br>
  <br>
**COMBINE** 圧縮台  <br>
I = COBBLESTONE @**name**:圧縮ブロック @**lore**:丸石x8 @**ench**:0:1 @**flag**:ALL  <br>
=== COBBLESTONE:0:8  <br>
>>>

* **さらに8倍の超圧縮丸石レシピを作成**<br>
`8個の圧縮丸石 <=> 1個の超圧縮丸石`

>>>
**COMBINE** 圧縮台  <br>
I8= COBBLESTONE @**name**:圧縮ブロック @**lore**:丸石x8 @**ench**:0:1 @**flag**:ALL  <br>
=== COBBLESTONE @**name**:超圧縮ブロック @**lore**:丸石x64 @**ench**:0:1 @**flag**:ALL  <br>
  <br>
**COMBINE** 圧縮台  <br>
I = COBBLESTONE @**name**:超圧縮ブロック @**lore**:丸石x64 @**ench**:0:1 @**flag**:ALL  <br>
=== COBBLESTONE:0:8 @**name**:圧縮ブロック @**lore**:丸石x8 @**ench**:0:1 @**flag**:ALL  <br>
>>>

* **さらに8倍の超圧縮丸石をを1個の激圧縮丸石にするレシピを作成**<br>
 * この圧縮には`レベル1`を要求するオプションを付加てみます<br>
 * また作成時に[+音+]、[+メッセージ+]、[+エフェクト+]をつけてみます<br>
 * カラーコードは[+「§」+]セクションでできます<br>

>>>

**COMBINE** 圧縮台  <br>
I8= COBBLESTONE @**name**:超圧縮ブロック @**lore**:丸石x64 @**ench**:0:1 @**flag**:ALL  <br>
=== COBBLESTONE @**name**:激圧縮ブロック @**lore**:丸石x512 @**ench**:0:1 @**flag**:ALL  <br>
$option @**level**:1 @**levelcost**:1 @**playsound**:WOLF_BARK:p=2  <br>
$option @**info**:§b[クラフト] §aこのクラフトはレベルが1以上で可能です  <br>
$option @**pathinfo**:§b[クラフト] §a激圧縮ブロックを作成しました  <br>
 * [+※横に長くなりすぎるオプションは分けて書くことも可能です +] <br>

**COMBINE** 圧縮台  <br>
I = COBBLESTONE @**name**:激圧縮ブロック @**lore**:丸石x512 @**ench**:0:1 @**flag**:ALL  <br>
=== COBBLESTONE:0:8 @**name**:超圧縮ブロック @**lore**:丸石x64 @**ench**:0:1 @**flag**:ALL  <br>
>>>

  <br>
  <br>
<h2>製錬レシピの追加</h2>
* **粉砕機というかまどに黒曜石を製錬(粉砕)するレシピを追加**<br>
`黒曜石 => 黒曜石の粉`

>>>
**SMELT** 粉砕機  <br>
※アイテム名には`Bukkit:ID`以外にも`minecraft:id`,`数字ID`も使用可能です  <br>
I = minecraft:obsidian  <br>
=== FIREWORK_CHARGE @**name**:§b黒曜石の粉 @**lore**:§4吸引注意 @**ench**:0:1 @**flag**:all  <br>
>>>

* **製錬レシピには[+燃焼時間+]、[+製錬時の音+]、[+製錬時のエフェクト+]なども指定が可能**<br>
`b黒曜石の粉 => 溶けた黒曜石`

>>>
**SMELT** 高温かまど  <br>
I = FIREWORK_CHARGE @**name**:§b黒曜石の粉 @**lore**:§4吸引注意 @**ench**:0:1 @**flag**:all  <br>
=== MAGMA_CREAM @**name**:§b溶けた黒曜石 @**lore**:§cドロドロ @**ench**:0:1 @**flag**:all  <br>
**$times**:1600  <br>
※80秒  <br>
**$effect**:EXPLOSION  <br>
**$sound**:BLOCK_WATER_AMBIENT:\v=0.5:p=0.5  <br>
>>>

<h2>醸造台レシピの追加</h2>
* **調合台という醸造台に鶏肉から錬金するレシピを追加**<br>
 * 醸造台レシピはかまどと同様[+燃焼時間+]、[+製錬時の音+]、[+製錬時のエフェクト+]など指定が可能です  <br>
 * このクラフトでは[+同時に3つ+]のアイテムまで要求することがでます  <br>
 * 鶏肉から錬金するレシピ　  <br>
 * またクラフトには[+確率分岐+]と+[確率追加+]があります(調合以外でも使用可能  <br>

>>>
**FUSION** 調合台  <br>
I = STONE  <br>
I = COOKED_CHICKEN  <br>
I = STONE  <br>
=== GOLD_INGOT  <br>
[--10%-] = GOLD_INGOT  <br>
[--70%-] = STONE  <br>
//70%が石をドロップ、10%で金をドロップ、残り20%はクラフト失敗(アイテムが出ない)  <br>
**$times**:200  <br>
>>>

* **金が増えるか増えないかわからないレシピ**<br>
 * 最低1個　最高5個  <br>

>>>
**FUSION** 調合台  <br>
I3= GOLD_INGOT  <br>
=== GOLD_INGOT  <br>
[++50%+] = GOLD_INGOT  <br>
[++50%+] = GOLD_INGOT  <br>
[++50%+] = GOLD_INGOT  <br>
[++50%+] = GOLD_INGOT  <br>
**$times**:200  <br>
>>>

<h2>金床レシピの追加</h2>
 * 叩きレシピでは**斧**と**金床**を使ったクラフト方法  <br>
 * クラフトで要求できる[+アイテムの数には制限はありません+]  <br>
 * また、**スペル**を指定することで、金床の文字入力欄に書いた[+文字が一致した場合のみ+]クラフトが成功します  <br>
 * クラフトに使用する斧とクラフト時の[+耐久の消費量+]設定できます  <br>
 * 耐久力のエンチャントをすることで消費を減らすこともできます  <br>
 * **計算式「消費量 = 指定数 - (0~1ランダム * 耐久レベル)　0以下になった場合は無視 耐久が10で最大10の消費が抑えられます**  <br>

※一度の指定では最大9個までです、それ以上要求する場合は圧縮レシピもしくは別の行に指定してください  <br>
 * **TNT,丸石x64,ダイヤモンド => 匠スポーン**  <br>

>>>
**SMASH** 匠たん  <br>
I = TNT  <br>
I8= COBBLESTONE  <br>
I8= COBBLESTONE  <br>
I8= COBBLESTONE  <br>
I8= COBBLESTONE  <br>
I8= COBBLESTONE  <br>
I8= COBBLESTONE  <br>
I8= COBBLESTONE  <br>
I8= COBBLESTONE  <br>
I = DIAMOND  <br>
=== 383 @**tag**:{EntityTag:{id:"Creeper"}}  <br>
**$spell**:匠よ来い  <br>
**$cost**:100  <br>
>>>

<h2>パースエラー</h2>
 * 間違えているとその間違っている行と間違っている場所が表示されます  <br>
 * 行番号の見る事の出来るテキストエディタを使うことをお勧めします  <br>

>>>
**SMELT** これ間違い  <br>
I = aaa:こんなID存在しないよん  <br>
I = STONE @**name**:かまどに2つの入力はありません  <br>
=== てすてす ここも間違えています  <br>
>>>

<h2>動的な文字・数字</h2>

 * オプションでの文字や、アイテムの名前、説明文は  <br>
 * 一部の文字を使うことでサーバー内の動的情報に置き換えることができます  <br>

<h3>かまどレシピ以外で使用可能 </h3>
|値                     |置換文字                           |
|------                 |------                             |
|\<player>				|プレイヤー名  				        |
|\<name>				|プレイヤー表示名			        |
|\<level>				|プレイヤーレベル  			        |
|\<UUID>				|プレイヤーのUUID  			        |
|\<health>				|体力  				    	        |
|\<foodlevel>			|満腹度  				            |
|\<saturation>			|隠れ満腹度(腹持ちみたいな数字)	    |
|\<playerX>				|プレイヤー座標 X			        |
|\<playerY>				|プレイヤー座標 Y			        |
|\<playerZ>				|プレイヤー座標 Z			        |
|\<score_文字>			|プレイヤーのスコアの値			    |

<h3>全てのレシピで使用可能</h3>

|値                     |置換文字                           |
|------                 |------                             |
|\<locationX>			|ブロックの座標 X			        |
|\<locationY>			|ブロックの座標 Y			        |
|\<locationZ>			|ブロックの座標 Z			        |
|\<time>			    |ワールド時間 (0-24000)		    	|
|\<fulltime>			|サーバー総時間				        |
|\<skylevel>			|ブロックの空からの明るさレベル		|
|\<blocklevel>			|ブロックからの明るさレベル         |
|\<lightlevel>			|明るさレベル(上2つの大きい方)		|
|\<World>			    |ワールド名				            |
|\<biome>			    |バイオーム(小文字)		        	|
|\<BIOME>			    |バイオーム(大文字)		        	|
<br>
<br>
<br>
<br>
 * またオプションでの文字、アイテムの名前、説明文内で  <br>
 * 特定の文字で囲うことで四則演算をさせることも可能です　上で使用した数字も代入可能です  <br>
 * ==NUM(str)[]内に式を書くことで文字として返される  <br>
 ///例 ==NUM(str)[(10/2) == 5]  -> trueと帰ってくる  <br>
<br>
 * ==NUM(int)[]内に式を書くことで整数型として返される(整数にならない場合はエラーを出力)  <br>
 ///例 ==NUM(int)[10/2]  -> 5と帰ってくる  <br>
<br>
 * ==NUM(dec)[]内に式を書くことで小数型として返される(数字にならない場合はエラーを出力)  <br>
 ///例 ==NUM(dec)[10/2]  -> 5.0と帰ってくる  <br>
<br>
 * 内部でJavaScriptを使用しているので変数など別の事も出来ると思います(やり方知りません  <br>
 * +、?、*、/、% が主に計算で使え == <= >= != < > が比較で使用できます  <br>
 * 他にもいろいろできると思うので「JavaScript 演算子」等で自力で調べてください(バグ未確認です)  <br>

<h1>開発者用</h1>
kameRecipeManager ver 3.3.0から外部からオプションを追加出来る機能が付きました。

レシピの処理中に動的に変化する周囲の環境やプレイヤーの状態で判別するオリジナルの機能を追加したいときに使ってみてください。

----
* おてほん クラフトが成功したときに指定ダメージを受けるオプションの作成。

<h6>1. CraftOptionをextendsしたクラスを作成する</h6>
ここではオプションの定義のみで実際には中身の処理は追加しません。
```java
public class Damage extends CraftOption {
	@Override
	public boolean isOnce() {
	    // シフトクリックで実行した場合その回数分のオプションが一気に実行されます。
	    // シフトクリックでも1回の実行にしたい場合はここでtrueを返します。
	    return true;
	}
}
```

<h6>2. 上で作ったクラスに[CraftCheckOption, CraftWorldCheckOption, CraftFailOption, CraftFineOption, CraftRunOption]のいずれかをimplementsする</h6>
複数のインターフェースを継承するとそれぞれの実行時に実行されます。
```java
public class Damage extends CraftOption implements CraftFineOption { //今回は成功時に実行してほしいのでCraftRunOptionを継承します
    
    private double damage;
    
    //実行時に何度も処理されてほしくないものは前もって計算させておくことも出来ます(レシピのチェックが走る前に1度だけ呼ばれる)
    @Override
    public void init(String str) {
        damage = Double.parseDouble(str);
        //エラーはすべて拾ってくれるので放置でいいと思います。
    }
    
    //CraftFineOptionの処理ではクラフトでアイテムが作られた回数分実行されます。
    @Override
    public void result(Player player, Block block, String str) {
        player.damage(damage);
    }

	@Override
	public boolean isOnce() {
	    return true;
	}
}
```

<h6>3. 最後に作成したクラスをCraftOptionに登録する。</h6>
```java
public class ExampleOption extends JavaPlugin {
    @Override
    public void onEnable() {
       Spell.register("pathdamage", Damage.class);
    }
}
```

これでクラフトが成功したときにダメージを与えるオプションが作成できました。

<h5>オプションのインターフェースの種類</h5>
|インターフェース名     |使い方・意味                                       |引数               |
|------                 |------                                             |------             |
|CraftRunOption			|クラフト台でレシピが確定したときに実行             |result(Player, Block, String)|
|CraftCheckOption		|クラフト台でレシピを検索しているときに実行         |result(Player, Block, String)|
|CraftWorldCheckOption	|クラフト台でレシピを検索しているときに実行(4回実行)|result(Player, Block, String, BlockFace※)|
|CraftFineOption        |クラフトに成功したときに実行                       |result(Player, Block, String)|
|CraftFailOption		|クラフトに失敗したときに実行                       |result(Player, Block, String)|

※: ここの値には NORTH, SOUTH, EAST, WESTが順番に入ります、地形の4方向をチェックしたい場合に使用します。

<h5>ちょっと便利かもしれないCraftOptionのメソッド</h5>
CraftOptionを継承したクラスでのみ使用できる内部API`OptionUtils`を使うことが出来ます。<br>
* 使用例: OptionUtils.in(min, check, max);
* 戻り値boolean:  `check`の値が`min`以上`max`以下(未満ではない)の時にtrueを返します。

|メソッド名     |使い方・意味                                       |引数               |
|------                 |------                                             |------             |
|OptionUtils.in			|checkの値がmin <= check <= max となるか確認        |in(Number, Number, Number)|
|OptionUtils.engine		|内部で文字列のboolean式の計算を行います(※)        |engine(String)|

※"true || false && (false && true)";みたいな文字列式をtrueかfalseで返してくれる。「!」対応してないです。